﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.mappings;
using NHibernate;

namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class CategoryDAO : BaseDAORepository<bdmirrorDB.categories>
    {
        public bdmirrorDB.categories getcatbyName(string name_en)
        {
            bdmirrorDB.categories res = new bdmirrorDB.categories();
            IList<bdmirrorDB.categories> tmp =FindAll("name_bn", name_en);
            if(tmp.Count!=0)
                 res = tmp.First();
           
            
            return res;
        }
        public IList<bdmirrorDB.categories> GetAllCat()
        {
            IList<bdmirrorDB.categories> res = new List<bdmirrorDB.categories>();
            try
            {
                using (ISession session = new Connection().OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.categories))
                        
                        .List<bdmirrorDB.categories>();
                }
                new Connection().CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
