﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Criterion;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
using NHibernate;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class ContentDAO : BaseDAORepository<bdmirrorDB.contents>
    {
        public bdmirrorDB.contents GetContentbyNewsID(bdmirrorDB.news newsid)
        {

            Connection nb = new Connection();
            bdmirrorDB.contents list = new bdmirrorDB.contents();
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.contents))
                                      .Add(Restrictions.Eq("news_id", newsid))
                                      .List<bdmirrorDB.contents>().Single();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            return list;


        }
    }
}
