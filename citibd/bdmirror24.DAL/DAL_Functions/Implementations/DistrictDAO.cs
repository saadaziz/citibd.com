﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;

namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class DistrictDAO : BaseDAORepository<bdmirrorDB.admdistrict>
    {
        public IList<bdmirrorDB.admdistrict> GetAll()
        {
            return All();
        }
        public IList<bdmirrorDB.admdistrict> GetAllDistrictByDivision(int districtID)
        {
            Connection nb = new Connection();
            IList<bdmirrorDB.admdistrict> list = null;
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.admdistrict))
                                      .Add(Restrictions.Eq("DivisionID", new DivisionDAO().pickbyID(districtID)))
                                      .List<bdmirrorDB.admdistrict>();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            return list;
        }
    }
}
