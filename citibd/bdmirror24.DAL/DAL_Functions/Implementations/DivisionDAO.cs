﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.DAL_Functions.Interfaces;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class DivisionDAO : BaseDAORepository<bdmirrorDB.admdivision>, IRepo<bdmirrorDB.admdivision>
    {
        public IList<bdmirrorDB.admdivision> GetAll()
        {
            return All();
        }
        
    }
}
