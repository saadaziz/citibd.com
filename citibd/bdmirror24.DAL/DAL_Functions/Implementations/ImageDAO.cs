﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.mappings;

namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class ImageDAO : BaseDAORepository<bdmirrorDB.images>
    {
        static Connection nb = new Connection();
        public IList<bdmirrorDB.images> GetImagesbyNewsID(bdmirrorDB.news newsid)
        {
            //Connection nb = new Connection();
            IList< bdmirrorDB.images> list = new List<bdmirrorDB.images>();
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.images))
                                      .Add(Restrictions.Eq("news_id", newsid))
                                      .List<bdmirrorDB.images>();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            return list;
        }
        public bdmirrorDB.images GetImagebyNewsID(bdmirrorDB.news newsid)
        {
            
            IList< bdmirrorDB.images> list = new List< bdmirrorDB.images>();
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.images))
                                      .Add(Restrictions.Eq("news_id", newsid))
                                      .AddOrder(Order.Desc("width"))
                                      .List<bdmirrorDB.images>();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            
            return (list.Count>0)?list.First():new bdmirrorDB.images();
        }
       public IList<bdmirrorDB.images> getNewsImage()
        {
            IList<bdmirrorDB.images> list = new List<bdmirrorDB.images>();
            try
            {
                //Connection nb = new Connection();
                using (ISession session = nb.OpenSession())
                {
                    session.BeginTransaction();

                    // list = FindByQuery("from images i where i.news_id !=null ");
                    list = session.CreateCriteria(typeof(bdmirrorDB.images))
                                          .Add(Restrictions.IsNotNull("news_id"))
                                           .AddOrder(Order.Desc("news_id"))
                                          //.SetProjection(Projections.Distinct(Projections.Property("news_id")))
                                           .SetMaxResults(80)
                                          .List<bdmirrorDB.images>();

                    session.Transaction.Commit();
                }
                nb.CloseSession();
                return list;
            }
            catch (Exception ex)
            {
                return list;
            }
        }
    }
}
