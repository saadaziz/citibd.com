﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
using NHibernate;
using NHibernate.Criterion;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class KeywrdsDAO:BaseDAORepository<bdmirrorDB.keywords>
    {
        public IList<bdmirrorDB.keywords> GetKeywordsbyNewsID(bdmirrorDB.news newsid)
        {
            Connection nb = new Connection();
            IList<bdmirrorDB.keywords> list = null;
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.keywords))
                                      .Add(Restrictions.Eq("news_id", newsid))
                                      .List<bdmirrorDB.keywords>();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            return list;
        }
    }
}
