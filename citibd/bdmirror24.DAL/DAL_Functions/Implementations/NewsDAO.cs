﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using NHibernate;
using NHibernate.Criterion;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
using System.Data;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class NewsDAO : BaseDAORepository<bdmirrorDB.news>
    {
        static Connection conn = new Connection();
        public bdmirrorDB.news getbyID(int id)
        {
            return pickbyID(id);
        }
        public void updatelatest(string myExecuteQuery)
        {
            string MyConString = conn.connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);
            MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, connection);
            myCommand.Connection.Open();
            //myConnection.Open();
            myCommand.ExecuteNonQuery();
            connection.Close();
        }
        public void increaseread(Int64 newsid)
        {
            string MyConString = conn.connectionString;
            MySqlCommand cmd = new MySqlCommand("readincrease", new MySqlConnection(MyConString));
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("newsid1", newsid));
            cmd.Connection.Open();
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }
        public void setspotlight()
        {
            string MyConString = conn.connectionString;
            MySqlCommand cmd = new MySqlCommand("update_spotlight", new MySqlConnection(MyConString));
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }
        public IList<bdmirrorDB.news> GetLatestNews(string which)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = conn.OpenSession())
                {

                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq(which, 1))
                        .AddOrder(Order.Desc("insert_time"))
                        .List<bdmirrorDB.news>();
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<bdmirrorDB.news> SearchNews(string word)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = conn.OpenSession())
                {

                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Like("title_bn", word))
                        .AddOrder(Order.Desc("insert_time"))
                        .List<bdmirrorDB.news>();
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return res;
            }
        }
        public bdmirrorDB.news getLastNewsbyCat(bdmirrorDB.categories cat,string language)
        {
            bdmirrorDB.news res = null;
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res =(language=="en")? session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("category_id", cat))
                        .Add(Restrictions.Not(Restrictions.Eq("title_en", "")))
                        .AddOrder(Order.Desc("insert_time"))
                        .SetMaxResults(1)
                        .List<bdmirrorDB.news>()[0]
                        : session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("category_id", cat))
                        .Add(Restrictions.Not(Restrictions.Eq("title_bn", "")))
                        .AddOrder(Order.Desc("insert_time"))
                        .SetMaxResults(1)
                        .List<bdmirrorDB.news>()[0];
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bdmirrorDB.news getspotlight()
        {
            bdmirrorDB.news res = new bdmirrorDB.news();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("top_news", "1"))
                        .List<bdmirrorDB.news>()[0];
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return res;
            }
        }
        public IList<bdmirrorDB.news> getAllNewsbyCatName(bdmirrorDB.categories cat)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("category_id", cat))
                        .Add(Restrictions.Eq("published", "1"))
                        .AddOrder(Order.Desc("insert_time"))
                        .List<bdmirrorDB.news>();
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<bdmirrorDB.news> GetAllNewsByUser(bdmirrorDB.user cat)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("writer", cat))
                        .AddOrder(Order.Desc("insert_time"))
                        .List<bdmirrorDB.news>();
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<bdmirrorDB.news> getAllNewsbyNewsTitle(string title)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(string))
                        .Add(Restrictions.Eq("title_en",title))
                        .List<bdmirrorDB.news>();
                }
                conn.CloseSession();
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<bdmirrorDB.news> getMostread()
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .AddOrder(Order.Desc("mostread"))
                         .Add(Restrictions.Eq("published", "1"))
                        .List<bdmirrorDB.news>().Take(5).ToList();
                }
                conn.CloseSession();
                return res;
            }
            catch (Exception ex)
            {
                return res;
            }
        }
        public IList<bdmirrorDB.news> getAllNewsbyCountry(bdmirrorDB.admcountry country)
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .Add(Restrictions.Eq("country_id", country))
                         .Add(Restrictions.Eq("published", "1"))
                        .List<bdmirrorDB.news>();
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<bdmirrorDB.news> GetLatestNews()
        {
            IList<bdmirrorDB.news> res = new List<bdmirrorDB.news>();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.news))
                        .AddOrder(Order.Desc("insert_time"))
                         .Add(Restrictions.Eq("published", "1"))
                        .List<bdmirrorDB.news>().Take(18).ToList();
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<int> getHomeNews(string language)
        {
            string sql = string.Empty;
            if (language != "bn")
            {
                sql =
                    @"(SELECT id from news where category_id=1 and published='1' and title_en!='' order by  news.id desc limit 0,5) 
UNION (SELECT id from news where category_id=2  and published='1' and title_en!='' order by  news.id desc limit 0,5) 
UNION (SELECT id from news where category_id=3  and published='1' and title_en!='' order by  news.id desc limit 0,5) 
UNION (SELECT id from news where category_id=4  and published='1' and title_en!='' order by  news.id desc limit 0,5)
UNION (SELECT id from news where category_id=5  and published='1' and title_en!='' order by  news.id desc limit 0,5)
UNION (SELECT id from news where category_id=6  and published='1' and title_en!='' order by  news.id desc limit 0,5) 
UNION (SELECT id from news where category_id=7  and published='1' and title_en!='' order by  news.id desc limit 0,5) 
UNION (SELECT id from news where category_id=8  and published='1' and title_en!='' order by  news.id desc limit 0,5) 
UNION (SELECT id from news where category_id=9  and published='1' and title_en!='' order by  news.id desc limit 0,5) 
UNION (SELECT id from news where category_id=10  and published='1' and title_en!='' order by  news.id desc limit 0,5)
UNION (SELECT id from news where category_id=11  and published='1' and title_en!='' order by  news.id desc limit 0,5)
UNION (SELECT id from news where category_id=13  and published='1' and title_en!='' order by  news.id desc limit 0,5)
UNION (SELECT id from news where category_id=14  and published='1' and title_en!='' order by  news.id desc limit 0,5)
UNION (SELECT id from news where category_id=15  and published='1' and title_en!='' order by  news.id desc limit 0,5)
UNION (SELECT id from news where category_id=16  and published='1' and title_en!='' order by  news.id desc limit 0,5)
UNION (SELECT id from news where category_id=17  and published='1' and title_en!='' order by  news.id desc limit 0,5);";
            }
            else
            {
                sql = @"(SELECT id from news where category_id=1 and published='1' and title_bn!='' order by   news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=2  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=3  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=4  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=5  and published='1' and title_bn!='' order by  news.id  desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=6  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=7  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=8  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=9  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=10  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=11  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=13  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=14  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=15  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=16  and published='1' and title_bn!='' order by  news.id desc limit 0,5)
                    UNION
                    (SELECT id from news where category_id=17  and published='1' and title_bn!='' order by  news.id desc limit 0,5)";
            }
            string MyConString = conn.connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            IList<int> res = new List<int>();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = sql;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res.Add(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;

        }
        public IList<int> getMainNews(string language)
        {
            string sql = string.Empty;
            if (language != "bn")
            {
                sql =
                    @"SELECT id from news where title_en!='' and top_news='1' ORDER BY date desc limit 0,8;";
            }
            else
            {
                sql = @"SELECT id from news where title_bn!='' and top_news='1' ORDER BY date desc limit 0,8";
            }
            string MyConString = conn.connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            IList<int> res = new List<int>();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = sql;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res.Add(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;

        }
        public bdmirrorDB.news getNewsByPic(string query)
        {
            string MyConString = conn.connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            bdmirrorDB.news res = new bdmirrorDB.news();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = query;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res = pickbyID(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;
        }
        public IList<int> GetCategoryNews(string query)
        {
            
            string MyConString = conn.connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            IList<int> res = new List<int>();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = query;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res.Add(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;

        }
        public IList<int> getRightNews(string language)
        {
            string sql = string.Empty;

            sql = (language == "en") ?
@"(select id from news where category_id=3 and published='1' and title_en!='' order by date desc limit 0,5)
                    UNION
                    (select id from news where category_id=1 and published='1' and title_en!='' order by date desc limit 0,5)
                    UNION
                    (select id from news where category_id=6 and published='1' and title_en!='' order by date desc limit 0,5)
                    UNION
                    (select id from news where category_id=2 and published='1' and title_en!='' order by date desc limit 0,5)" :
@"(select id from news where category_id=3 and published='1' and title_bn!='' order by date desc limit 0,5)
                    UNION
                    (select id from news where category_id=1 and published='1' and title_bn!='' order by date desc limit 0,5)
                    UNION
                    (select id from news where category_id=6 and published='1' and title_bn!='' order by date desc limit 0,5)
                    UNION
                    (select id from news where category_id=2 and published='1' and title_bn!='' order by date desc limit 0,5)";
            string MyConString = conn.connectionString;
            MySqlConnection connection = new MySqlConnection(MyConString);

            IList<int> res = new List<int>();
            //Int32 log = 0;
            MySqlCommand command = connection.CreateCommand();
            MySqlDataReader Reader;
            //DBNull gg = null;
            command.CommandText = sql;
            connection.Open();
            Reader = command.ExecuteReader();
            while (Reader.Read())
            {
                if ((Reader["id"]) != DBNull.Value)
                {

                    res.Add(int.Parse((Reader["id"]).ToString()));
                }
                //break;

            }
            Reader.Close();
            connection.Close();

            //res = FindByQuery(sql);
            return res;
        }
    }
}
