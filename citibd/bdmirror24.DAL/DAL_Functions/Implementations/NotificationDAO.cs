﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
using NHibernate;
using NHibernate.Criterion;
using MySql.Data.MySqlClient;
using System.Data;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class NotificationDAO : BaseDAORepository<bdmirrorDB.notification>
    {
        static Connection conn = new Connection();
        public void updatenoti(Int64 userid)
        {
            string MyConString = conn.connectionString;
            MySqlCommand cmd = new MySqlCommand("update_noti", new MySqlConnection(MyConString));
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("userid", userid));
            cmd.Connection.Open();
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }
        public IList<bdmirrorDB.notification> getAllNotification(bdmirrorDB.user user)
        {
            IList<bdmirrorDB.notification> res = new List<bdmirrorDB.notification>();
            try
            {
                Connection conn= new Connection();
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.notification))
                        .Add(Restrictions.Eq("user_id", user))
                        .Add(Restrictions.Eq("status", "unread"))
                        .AddOrder(Order.Desc("insert_time"))
                        .List<bdmirrorDB.notification>();
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
