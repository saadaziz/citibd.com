﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Criterion;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
using NHibernate;
namespace bdmirror24.DAL.DAL_Functions.Implementations
{
    public class userDAO : BaseDAORepository<bdmirrorDB.user>
    {
        static Connection conn = new Connection();
        
        public bdmirrorDB.user getuserbyemail(string user_name)
        {
            bdmirrorDB.user res = new bdmirrorDB.user();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.user))
                        .Add(Restrictions.Eq("email", user_name))
                        .List<bdmirrorDB.user>()[0];
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bdmirrorDB.user getByFName(string fname)
        {
            bdmirrorDB.user res = new bdmirrorDB.user();
            try
            {
                using (ISession session = conn.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(bdmirrorDB.user))
                        .Add(Restrictions.Eq("first_name", fname))
                        .List<bdmirrorDB.user>()[0];
                }
                conn.CloseSession();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
