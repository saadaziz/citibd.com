﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.DALFunctions.Interfaces;
using bdmirror24.DAL.mappings;

namespace bdmirror24.DAL.DAL_Functions.Interfaces
{
    public interface ICategory:IRepo<bdmirrorDB.categories>
    {
        bdmirrorDB.categories getcatbyName(string name_en);
        IList<bdmirrorDB.categories> GetAllCat();
    }
}
