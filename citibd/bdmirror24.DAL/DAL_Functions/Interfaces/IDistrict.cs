﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Interfaces
{
    public interface IDistrict:IRepo<bdmirrorDB.admdistrict>
    {
        IList<bdmirrorDB.admdivision> GetAllDistrictByDivision(int districtID);
        IList<bdmirrorDB.admdivision> GetAll();
    }
}
