﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Interfaces
{
    public interface IImage : IRepo<bdmirrorDB.images>
    {
        IList<bdmirrorDB.images> GetImagesbyNewsID(bdmirrorDB.news newsid);
        bdmirrorDB.images GetImagebyNewsID(bdmirrorDB.news newsid);
        IList<bdmirrorDB.images> getNewsImage();
    }
}
