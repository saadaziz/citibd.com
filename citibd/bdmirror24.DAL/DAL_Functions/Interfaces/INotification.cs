﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Interfaces;
namespace bdmirror24.DAL.DAL_Functions.Interfaces
{
    public interface INotification : IRepo<bdmirrorDB.notification>
    {
        IList<bdmirrorDB.notification> getAllNotification(bdmirrorDB.user user);
        void updatenoti(Int64 userid);
    }
}
