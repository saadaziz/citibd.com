﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
namespace bdmirror24.DAL.Manager
{
    public class CountryManager:BaseDAORepository<bdmirrorDB.admcountry>
    {
        public bdmirrorDB.admcountry GetCountry(int counryid)
        {
            return (counryid > 0) ? pickbyID(counryid) : null;
        }
        public IList<bdmirrorDB.admcountry> GetAllCountry()
        {
            return All();
        }
    }
}
