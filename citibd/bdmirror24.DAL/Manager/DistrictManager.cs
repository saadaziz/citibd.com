﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;

using NHibernate;
using NHibernate.Criterion;
namespace bdmirror24.DAL.Manager
{
    public class DistrictManager 
    {
        public IList<bdmirrorDB.admdistrict> GetAllDistrictByDivision(int divisionid)
        {
            //bdmirrorDB.admdivision division = new DivisionManager().pickbyID(divisionid);

            return (divisionid > 0) ? new DistrictDAO().GetAllDistrictByDivision(divisionid) : null;
        }
        public IList<bdmirrorDB.admdistrict> GetAllDistrict()
        {
            return new DistrictDAO().GetAll();
        }
    }
}
