﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DALFunctions.Implementations;
using bdmirror24.DAL.DALFunctions.Interfaces;
using bdmirror24.DAL.DAL_Functions.Implementations;
using NHibernate.Criterion;
using NHibernate;

namespace bdmirror24.DAL.Manager
{
    public class UpzillaManager:BaseDAORepository<bdmirrorDB.admupazilaps>
    {
        public IList<bdmirrorDB.admupazilaps> GetAllUpzillaByDistrict(int Districtid)
        {
            Connection nb = new Connection();
            IList<bdmirrorDB.admupazilaps> list = null;
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(bdmirrorDB.admupazilaps))
                                      .Add(Restrictions.Eq("DistrictID", new DistrictDAO().pickbyID(Districtid)))
                                      .List<bdmirrorDB.admupazilaps>();

                session.Transaction.Commit();
            }
            nb.CloseSession();
            return list;
        }
        
        public IList<bdmirrorDB.admupazilaps> GetAllUpzilla()
        {
            return All();
        }
    }
}
