﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bdmirror24.DAL.mappings
{
    public class bdmirrorDB
    {
         [Serializable]
        public class categories
        {
            public virtual Int64 cat_id { get; set; }
            public virtual string name_bn { get; set; }

        }
        [Serializable]
        public class news
        {
            public virtual Int64 news_id { get; set; }
            public virtual string title_bn { get; set; }
            public virtual string top_news { get; set; }
            public virtual int latest { get; set; }
            public virtual string published { get; set; }
            public virtual string news_link { get; set; }
            public virtual DateTime insert_time { get; set; }
            public virtual categories category_id { get; set; }
            public virtual admcountry country_id { get; set; }
            public virtual admdistrict district_id { get; set; }
            public virtual admupazilaps upzillaID { get; set; }
            public virtual user writer { get; set; }
            public virtual string content_bn { get; set; }
            public virtual contents contents { get; set; }
            public virtual int top_slider { get; set; }
            public virtual string writer_name { get; set; }
            public virtual DateTime modified_time { get; set; }
            public virtual int mostread { get; set; }

        }
         [Serializable]
        public class contents
        {
            public virtual Int64 content_id { get; set; }
            public virtual string english_content { get; set; }

        }
         [Serializable]
         public class notification
         {
             public virtual Int64 noti_id { get; set; }
             public virtual string notifications { get; set; }
             public virtual string status { get; set; }
             public virtual DateTime insert_time { get; set; }
             public virtual user user_id { get; set; }
         }
         [Serializable]
        public class images
        {
            public virtual Int64 image_id { get; set; }
            public virtual news news_id { get; set; }
            public virtual string image_name { get; set; }
            public virtual string caption_en { get; set; }
            public virtual string link { get; set; }
            public virtual string image_watermarked { get; set; }
            public virtual DateTime time { get; set; }
            public virtual double height { get; set; }
            public virtual double width { get; set; }
        }
        
         [Serializable]
        public class user
        {
             
            public virtual Int64 user_id { get; set; }
            public virtual string first_name { get; set; }
            public virtual string last_name { get; set; }
            public virtual string email { get; set; }
            public virtual string password { get; set; }
            public virtual string contact_no { get; set; }
            public virtual string gender { get; set; }
            public virtual string national_id_no { get; set; }
            public virtual admdivision Division { get; set; }
            public virtual admcountry Country { get; set; }
            public virtual admupazilaps Upzilla { get; set; }
            public virtual admdistrict District { get; set; }
            public virtual string priviledge { get; set; }
            public virtual string Address { get; set; }
            public virtual int profile_complete { get; set; }
            public virtual DateTime joined_date { get; set; }
            public virtual DateTime DOB { get; set; }
            public virtual images profile_pic { get; set; }
            public virtual int PostalCode { get; set; }
            public virtual string Reference_Person { get; set; }
            public virtual string Reference_Person_addr { get; set; }
            public virtual string Reference_Person_contact { get; set; }
            public virtual string short_bio { get; set; }
            //public IList<notification> notifications { get; protected set; }
        }
         [Serializable]
        public class admcountry
        {
            public virtual Int64 country_id { get; set; }
            public virtual string country_name { get; set; }
            public virtual string country_code { get; set; }
            public virtual string ISDcode { get; set; }

        }
         [Serializable]
        public class admdivision
        {
            public virtual Int64 division_id { get; set; }
            public virtual string divCode { get; set; }
            public virtual string divName { get; set; }
            public virtual string divCodeName { get; set; }
            public virtual admcountry countryid { get; set; }
        }
         [Serializable]
        public class admdistrict
        {
            
            public virtual Int64 DistrictID { get; set; }
            public virtual string DistrictCode { get; set; }
            public virtual string DistrictName { get; set; }
            public virtual string DistrictCodeName { get; set; }
            public virtual admdivision DivisionID { get; set; }
        }
         [Serializable]
        public class admupazilaps
        {
            public virtual Int64 UpazilaPSID { get; set; }
            public virtual string UpazilaPSCode { get; set; }
            public virtual string UpazilaPSName { get; set; }
            public virtual admdistrict DistrictID { get; set; }
        }
         [Serializable]
        public class keywords
        {
            public virtual Int64 keyword_id { get; set; }
            public virtual string keyword { get; set; }
            public virtual news news_id { get; set; }
        }
         [Serializable]
        
         public class all_add
         {
             public virtual Int64 add_id { get; set; }
             public virtual String title { get; set; }
             public virtual String image_link { get; set; }
             public virtual String image_url { get; set; }
             public virtual Int64 addorder { get; set; }

         }
         [Serializable]
         public class videos
         {
             public virtual Int64 video_id { get; set; }
             public virtual string caption_en { get; set; }
             public virtual string caption_bn { get; set; }
             public virtual string link { get; set; }
             public virtual DateTime time { get; set; }
         }
       
    }
}
