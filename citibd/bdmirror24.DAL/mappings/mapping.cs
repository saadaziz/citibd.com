﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace bdmirror24.DAL.mappings
{
    public class categoriesmap : ClassMap<bdmirrorDB.categories>
    {
        public categoriesmap()
        {
            Id(x => x.cat_id).Column("id");
            Map(x => x.name_bn).Column("name_bn").Nullable();
            Table("categories");
        }
        public class newsmap : ClassMap<bdmirrorDB.news>
        {
            public newsmap()
            {
                Id(x => x.news_id).Column("id");
                Map(x => x.title_bn).Column("title_bn").Nullable();
                Map(x => x.top_news).Column("top_news").Nullable();
                Map(x => x.latest).Column("latest").Nullable();
                Map(x => x.published).Column("published").Nullable();
                Map(x => x.news_link).Column("news_link").Nullable();
                Map(x => x.insert_time).Column("insert_time").Not.Nullable();
                Map(x => x.top_slider).Column("top_slider").Not.Nullable();
                Map(x => x.writer_name).Column("writer_name").Nullable();
                Map(x => x.content_bn).Column("content_bn").Nullable();
                Map(x => x.modified_time).Column("modified_time").Nullable();
                Map(x => x.mostread).Column("mostread").Not.Nullable();
                References(x => x.contents).Column("content_id").Nullable().Not.LazyLoad();
                References(x => x.category_id).Column("category_id").Not.Nullable().Not.LazyLoad();
                References(x => x.country_id).Column("country_id").Nullable().Not.LazyLoad();
                References(x => x.district_id).Column("district_id").Nullable().Not.LazyLoad();
                References(x => x.upzillaID).Column("upzillaID").Nullable();
                References(x => x.writer).Column("writer").Not.Nullable().Not.LazyLoad();
                Table("news");
            }

        }
        public class contentsmap : ClassMap<bdmirrorDB.contents>
        {
            public contentsmap()
            {
                Id(x => x.content_id).Column("content_id");
                Map(x => x.english_content).Column("english_content").Nullable();
                Table("contents");
            }

        }
        public class imagesmap : ClassMap<bdmirrorDB.images>
        {
            public imagesmap()
            {
                Id(x => x.image_id).Column("image_id");
                Map(x => x.image_name).Column("image_name").Nullable();
                Map(x => x.caption_en).Column("caption_en").Nullable();
                Map(x => x.link).Column("link").Nullable();
                Map(x => x.image_watermarked).Column("image_watermarked").Nullable();
                Map(x => x.time).Column("time").Not.Nullable();
                Map(x => x.height).Column("height").Nullable();
                Map(x => x.width).Column("width").Nullable();
                References(x => x.news_id).Column("news_id").Nullable().Not.LazyLoad();
                Table("images");
            }

        }
        public class usermap : ClassMap<bdmirrorDB.user>
        {
            public usermap()
            {
                Id(x => x.user_id).Column("user_id");
                Map(x => x.first_name).Column("first_name").Not.Nullable();
                Map(x => x.last_name).Column("last_name").Nullable();
                Map(x => x.email).Column("email").Not.Nullable();
                Map(x => x.password).Column("password").Not.Nullable();
                Map(x => x.contact_no).Column("contact_no").Not.Nullable();
                Map(x => x.gender).Column("gender").Not.Nullable();
                Map(x => x.national_id_no).Column("national_id_no").Nullable();
                Map(x => x.priviledge).Column("priviledge").Not.Nullable();
                References(x => x.Division).Column("Division").Nullable().Not.LazyLoad();
                References(x => x.Upzilla).Column("Upzilla").Nullable().Not.LazyLoad();
                References(x => x.District).Column("District").Nullable().Not.LazyLoad();
                References(x => x.Country).Column("Country").Not.LazyLoad();
                References(x => x.profile_pic).Column("profile_pic").Nullable().Not.LazyLoad();
                Map(x => x.profile_complete).Column("profile_complete").Not.Nullable();
                Map(x => x.Address).Column("Address").Nullable();
                Map(x => x.joined_date).Column("joined_date").Not.Nullable();
                Map(x => x.DOB).Column("DOB").Nullable();
                Map(x => x.PostalCode).Column("Postal_Code").Nullable();
                Map(x => x.Reference_Person).Column("Reference_Person").Nullable();
                Map(x => x.Reference_Person_addr).Column("Reference_Person_addr").Nullable();
                Map(x => x.Reference_Person_contact).Column("Reference_Person_contact").Nullable();
                Map(x => x.short_bio).Column("short_bio").Nullable();
                //HasMany(x => x.notifications).KeyColumn("").Inverse().Cascade.All().AsBag();
                Table("user");
            }

        }
        public class admcountrymap : ClassMap<bdmirrorDB.admcountry>
        {
            public admcountrymap()
            {
                Id(x => x.country_id).Column("country_id");
                Map(x => x.country_name).Column("country_name").Not.Nullable();
                Map(x => x.country_code).Column("country_code").Not.Nullable();
                Map(x => x.ISDcode).Column("ISDcode").Nullable();
                Table("admcountry");
            }
        }
        public class admdivisionmap : ClassMap<bdmirrorDB.admdivision>
        {
            public admdivisionmap()
            {
                Id(x => x.division_id).Column("DivisionID");
                Map(x => x.divCode).Column("DivisionCode").Nullable();
                Map(x => x.divName).Column("DivisionName").Not.Nullable();
                Map(x => x.divCodeName).Column("DivisionCodeName").Nullable();
                References(x => x.countryid).Column("CountryID").Nullable();
                Table("admdivision");
            }

        }
        public class admdistrictmap : ClassMap<bdmirrorDB.admdistrict>
        {
            public admdistrictmap()
            {
                Id(x => x.DistrictID).Column("DistrictID");
                Map(x => x.DistrictCode).Column("DistrictCode").Nullable();
                Map(x => x.DistrictName).Column("DistrictName").Nullable();
                Map(x => x.DistrictCodeName).Column("DistrictCodeName").Nullable();
                References(x => x.DivisionID).Column("DivisionID").Nullable();
                Table("admdistrict");
            }

        }
        public class admupazilapsmap : ClassMap<bdmirrorDB.admupazilaps>
        {
            public admupazilapsmap()
            {
                Id(x => x.UpazilaPSID).Column("UpazilaPSID");
                Map(x => x.UpazilaPSCode).Column("UpazilaPSCode").Nullable();
                Map(x => x.UpazilaPSName).Column("UpazilaPSName").Nullable();
                References(x => x.DistrictID).Column("DistrictID").Nullable();
                Table("admupazilaps");
            }

        }
        
        public class keywordsmap : ClassMap<bdmirrorDB.keywords>
        {
            public keywordsmap()
            {
                Id(x => x.keyword_id).Column("keyword_id");
                Map(x => x.keyword).Column("keyword").Nullable();
                References(x => x.news_id).Column("news_id").Nullable();
                Table("keywords");
            }

        }
        
        public class all_addmap : ClassMap<bdmirrorDB.all_add>
        {
            public all_addmap()
            {
                Id(x => x.add_id).Column("add_id");
                Map(x => x.title).Column("title").Not.Nullable();
                Map(x => x.image_link).Column("image_link").Not.Nullable();
                Map(x => x.image_url).Column("image_url").Nullable();
                Map(x => x.addorder).Column("addorder").Not.Nullable();
                Table("all_add");
            }

        }
        public class videosmap : ClassMap<bdmirrorDB.videos>
        {
            public videosmap()
            {
                Id(x => x.video_id).Column("video_id");
                Map(x => x.caption_en).Column("caption_en").Nullable();
                Map(x => x.caption_bn).Column("caption_bn").Nullable();
                Map(x => x.link).Column("link").Not.Nullable();
                Map(x => x.time).Column("time").Not.Nullable();
                Table("videos");
            }

        }
        public class notificationmap : ClassMap<bdmirrorDB.notification>
        {
            public notificationmap()
            {
                Id(x => x.noti_id).Column("noti_id");
                Map(x => x.insert_time).Column("insert_time").Not.Nullable();
                Map(x => x.notifications).Column("notification").Not.Nullable();
                References(x => x.user_id).Column("user_id").Not.Nullable().Not.LazyLoad();
                Map(x => x.status).Column("status").Not.Nullable();
                Table("notification");
            }
        }
    }
}
