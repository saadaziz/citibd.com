﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Couchbase;
using log4net;
using bdmirror24.DAL.mappings;
using Enyim.Caching.Memcached;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace bdmirror24.couchbase
{
    public class couchbasemain
    {
        public bdmirrorDB.news getNews(int id)
        {
            string res = "";
            bdmirrorDB.news news =null;
            using (var client = new CouchbaseClient())
            {
                var result = client.Get<bdmirrorDB.news>("citibd-"+id.ToString());
                if (result!=null)
                {
                    news = client.Get<bdmirrorDB.news>("citibd-" + id.ToString());
                    //Response.Write(news.title_en);
                    //news = (bdmirrorDB.news)client.Get<bdmirrorDB.news>(id.ToString());
                    client.Touch(id.ToString(), TimeSpan.FromHours(3));
                }
            }
            return news;
        }
        //protected static log4net.ILog _Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void setnews(bdmirrorDB.news news)
        {
            try
            {
                using (var client = new CouchbaseClient())
                {
                    // Response.Write("There is no news with id :"+news.news_id);

                    MemoryStream memorystream = new MemoryStream();
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(memorystream, news);

                    var store = client.ExecuteStore(StoreMode.Set,
                                                    "citibd-" + news.news_id.ToString(),
                                                    news,
                                                    TimeSpan.FromHours(2));
                    //res = news.title_en;
                    //if (!store.Success)
                    //    news.title_en = store.Message + "......----" + store.Exception;
                    if (!store.Success)
                    {
                        //_Logger.Error(typeof(bdmirrorDB.news).Name + " coudl not get -- " + store.Exception);
                    }
                }
            }
            catch (Exception ex)
            { 
            
            }
        }
    }
}
