﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="citibd.Category"
    MasterPageFile="~/main.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<%-- <script type="text/javascript">
     function RetrievePicture(imgCtrl, picid) {
         alert("asadasda");
         imgCtrl.onload = null;
         imgCtrl.src = 'showimg.ashx?path=' + picid;
         
     }
    </script>--%>
     <script type="text/javascript">
         $(document).ready(function () {

             function lastPostFunc() {
                 $('#divPostsLoader').html('<img src="img/bigLoader.gif">');
                 var data = { category: $("#content_hdnval").val() };
                 //send a query to server side to present new content
                 $.ajax({
                     type: "POST",
                     url: "category.aspx/Foo",
                     data: JSON.stringify( data ),
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (data) {

                         if (data != "") {
                             $('#pnlslider').append(data.d);
                         }
                         $('#divPostsLoader').empty();
                     }

                 })
             };

             //When scroll down, the scroller is at the bottom with the function below and fire the lastPostFunc function
             $(window).scroll(function () {
                 if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                     //alert($(window).scrollTop());
                     lastPostFunc();
                     //metro_slider();
                 }
             });

         });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
 <form id="form1" runat="server">
    <asp:HiddenField ID="hdnval" runat="server" />
    <div class="n_content clearfix">
        <div class="row-fluid">
         
            <div class="viewport">
                <div class="overview"> 
                    <div class="metro-slider" id="pnlslider" style="width:100%">
                    
                        <asp:Repeater ID="rptrow" runat="server">
                            <ItemTemplate>
                                <asp:Panel ID="pnlrow" runat="server" class="metro-row" style="width:100%">

                                <%# Eval("text") %>
                                
                               
                                </asp:Panel>

                            </ItemTemplate>
                        </asp:Repeater>

                    </div>
                </div>
            </div>
             <div id="divPostsLoader">
        </div>
        </div>
    </div>
    </form>
</asp:Content>
