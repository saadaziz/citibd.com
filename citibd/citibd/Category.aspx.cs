﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Data;
using System.Web.Services;
using System.Text;
namespace citibd
{
    public partial class Category : System.Web.UI.Page
    {
        #region private properties
        getImages getimg = new getImages();
        IList<bdmirrorDB.images> imagesall = new List<bdmirrorDB.images>();
        static Utility uit = new Utility();
        bdmirrorDB.categories cat = new bdmirrorDB.categories();
        static int noviews=0;
        private int catid
        {
            get
            {
                if (ViewState["catid"] == null)
                    ViewState["catid"] = -1;
                return (int)ViewState["catid"];
            }
            set
            {
                ViewState["catid"] = value;
            }
        }
        IList<bdmirrorDB.news> newz = new List<bdmirrorDB.news>();
        IList<bdmirrorDB.news> alreadyinserted = new List<bdmirrorDB.news>();
        ImageDAO imgdao = new ImageDAO();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cat = new bdmirrorDB.categories();
                if (Request.QueryString["id"] != null)
                {
                    cat = new CategoryDAO().pickbyID(int.Parse(Request.QueryString["id"].ToString()));
                    catid = Convert.ToInt32(cat.cat_id);
                }
                else
                    Response.Redirect("default.aspx");
                hdnval.Value = catid.ToString();
                DataTable dt= getimg.loadrows(cat,0);
                rptrow.DataSource = dt;
                rptrow.DataBind();
                Page.Title = cat.name_bn + " .::. Your home to civil journalism.";
                Page.MetaDescription = cat.name_bn + " news from dhaka,rajshahi,barishal,chittagong,rangpur,sylhet,khulna." + cat.name_bn + " Civil journalism of bangladesh";
                Page.MetaKeywords="civil journalism bangladesh,civil journalism dhaka,photo journalism dhaka,"+cat.name_bn;
            }
        }
        [WebMethod]
        public static string Foo(string category)
        {
            StringBuilder getPostsText = new StringBuilder();

            Category catthis= new Category();
            bdmirrorDB.categories cat1 = new CategoryDAO().pickbyID(int.Parse(category));
            int countnews = new NewsDAO().getAllNewsbyCatName(cat1).Count - 18;
            //for (int i = 1; i < (countnews / 6 - 3);i++ )
           if(noviews*6<countnews)
               getPostsText.AppendFormat(@"<div class=""metro-row"" style=""width:100%"">" + new getImages().loadrowslate(cat1, noviews++) + "</div>");
            return getPostsText.ToString();
        }
        //[WebMethod]
        //public static string updatenoti()
        //{
        //    bdmirrorDB.user usr = Utility.get_ormuserid();
        //    new NotificationDAO().updatenoti(usr.user_id);
        //    return "done";
        //}
    }
}