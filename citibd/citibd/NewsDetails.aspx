﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsDetails.aspx.cs" Inherits="citibd.NewsDetails"
    MasterPageFile="~/main.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        addthis.layers({
            'theme': 'gray',
            'share': {
                'position': 'left',
                'numPreferredServices': 5
            },
            'recommended': {
                'title': 'Recommended for citibd users:'
            }
        });
    </script>
    <style type="text/css">
        .imgdetails
        {
            margin-left:auto;
            margin-right:auto;
            }
    </style>
    <!-- AddThis Smart Layers END -->
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <div class="n_content clearfix">
        <div class="row-fluid">
            <div class="span8">
                <div class="row-fluid">
                    <h1>
                        <asp:HyperLink ID="hpltitle" runat="server"></asp:HyperLink></h1>
                    <%--<aside class="n_post_share_trigger n_color">+ Share</aside>
						<aside class="n_post_tags_trigger">+ Tags</aside>--%>
                        <table width="100%">
                        <tr >
                        <td style="width:60%">
                        <table><tr><td> <span class="n_little_date"><asp:HyperLink ID="hpldate" runat="server"></asp:HyperLink></span> 
                        <span class="n_stars"><asp:HyperLink ID="hplcat" runat="server"></asp:HyperLink></span> 
                        <span class="n_stars"> <asp:HyperLink ID="hplcountry"  runat="server"></asp:HyperLink></span></td></tr>
                        <tr><td>
                    <asp:Panel runat="server" ID="pnlothers" Visible="false">
                        <span class="n_stars">
                            <asp:HyperLink ID="hpldivision" runat="server"></asp:HyperLink></span> <span class="n_stars">
                                <asp:HyperLink ID="hpldistrict" runat="server"></asp:HyperLink></span>
                    </asp:Panel></td></tr></table>
                   
                        </td>
                     <td style="width:40%" class="pull-right">
                         <asp:Image ID="imgpro" runat="server" Height="75px" Width="75px"/><br />
                         <asp:HyperLink ID="hplprofile" runat="server" CssClass="margin-left:auto;margin-right:auto;"></asp:HyperLink>
                     </td>
                    </tr>
                    
                    </table>
                </div>
                <div class="row-fluid">
                    <div class="span12 n_blog_list n_post">
                        <asp:Panel ID="myCarousel" runat="server">
                            <div class="n_slider">
                                <div id="n_slider_wrapper">
                                    <div id="n_slider_inner">
                                        <div id="n_slider_carousel-wrapper">
                                            <div id="n_slider_carousel">
                                                <asp:Repeater runat="server" ID="rptimg" OnItemDataBound="rptimg_bound">
                                                    <ItemTemplate>
                                                        <asp:Image CssClass="imgdetails" ID="img" runat="server" ImageUrl='<%# Eval("watermark") %>' AlternateText='<%# Eval("caption") %>' />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <%--<img src="photos/categoriesSlider/large/15.png" width="750" alt="" data-href="categories_2.php" />--%>
                                            </div>
                                            <!-- News Slider Big Images Ends -->
                                        </div>
                                        <!-- News Slider Big Images Wrapper Ends -->
                                        <div id="n_slider_pager-wrapper">
                                            <div id="n_slider_pager">
                                                <asp:Repeater runat="server" ID="rptimgsmall">
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("watermark") %>' Width="80"
                                                            Height="80" AlternateText='<%# Eval("caption") %>' />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                            <!-- News Slider Pager Ends -->
                                        </div>
                                        <!-- News Slider Pager Wrapper Ends -->
                                    </div>
                                    <!-- News Slider Inner Ends -->
                                </div>
                                <!-- News Slider Wrapper Ends -->
                            </div>
                            <!-- News Slider Ends -->
                        </asp:Panel>
                    </div>
                </div>
                <div class="row-fluid">
                    <asp:Label CssClass="padding-top:10px;" ID="lblcontent" runat="server" Text=""></asp:Label>
                </div>
                <!-- Post Details Ends -->
                <div class="row-fluid">
                    <div class="span12">
                    </div>
                </div>
                <!-- Post Comments Ends -->
            </div>
            <!-- Big Content Area -->
            <div class="span2">
                <div class="row-fluid">
                    <div class="span12">
                        <h4 class="n_news_cat_list_title">
                            সর্বশেষ সংবাদ</h4>
                        <asp:Repeater ID="rptlatest" runat="server">
                            <ItemTemplate>
                                <div class="n_latest_post_container clearfix latest-post text-center">
                                    <a href='<%# Eval("newslink") %>' class="n_latest_post_picture">
                                        <img src='<%# Eval("imagelink") %>' height="75px" width="75px" alt='<%# Eval("newstitle") %>' /></a>
                                    <div class="n_latest_post_details">
                                        <a href='<%# Eval("newslink") %>' class="n_title_link">
                                            <h5 class="n_little_title">
                                                <%# Eval("newstitle") %></h5>
                                        </a><span class="n_little_date">
                                            <%# Eval("newsdate") %></span>
                                    </div>
                                </div>
                                <div class="n_splitter_2">
                                    <span class="n_bgcolor"></span>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <!-- Latest News Ends -->
                <div class="row-fluid n_hide">
                    <div class="span12">
                        <a href="#">
                            <img src="" alt="advertise" /></a>
                    </div>
                </div>
                <!-- AD Block Ends -->
            </div>
            <!-- Mid Side Bar Area -->
            <div class="span2">
                <div class="row-fluid n_small_block n_text_align_center">
                    <div class="span12">
                        <a href="#">
                            <img src="" alt="advertise" /></a>
                    </div>
                </div>
                <div class="row-fluid n_small_block n_text_align_center">
                    <div class="span12">
                        <h4 class="n_news_cat_list_title">
                            সর্বাধিক পঠিত সংবাদ</h4>
                    </div>
                </div>
                <asp:Repeater ID="rptmostread" runat="server">
                    <ItemTemplate>
                        <div class="row-fluid n_small_block n_text_align_center n_hide">
                            <div class="span12">
                                <div class="pull-left text-center centering-saad">
                                    <a href='<%# Eval("newslink") %>'>
                                        <img src='<%# Eval("imagelink") %>' width="170px" height="170px" alt='<%# Eval("newstitle") %>' /></a>
                                    <a href='<%# Eval("newslink") %>' class="n_title_link">
                                        <h5 class="n_little_title">
                                            <%# Eval("newstitle") %></h5>
                                    </a><span class="n_little_date">
                                        <%# Eval("newsdate") %></span>
                                    <p class="n_short_descr">
                                        '<%# Eval("newscontent") %>'</p>
                                </div>
                            </div>
                        </div>
                        <div class="n_splitter_2">
                            <span class="n_bgcolor"></span>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <!-- Right Side Bar Area -->
            <!-- Mid Side Bar Area -->
        </div>
        <div class="n_splitter">
            <span class="n_bgcolor"></span>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <img alt="advertise" />
            </div>
        </div>
    </div>
    <!-- Middle Content Ends -->
</asp:Content>
