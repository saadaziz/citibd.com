﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Data;

namespace citibd
{
    public partial class NewsDetails : System.Web.UI.Page
    {
        ImageDAO imgdao = new ImageDAO();
        NewsDAO ndao = new NewsDAO();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["newsid"] != null)
                    loadcontrolvalues(int.Parse(Request.QueryString["newsid"].ToString()));
                else
                    Response.Redirect("~/default.aspx");

            }

        }
        protected void loadcontrolvalues(int newsid)
        {
            bdmirrorDB.user curruser = Utility.get_ormuserid();
            bdmirrorDB.news nz= ndao.pickbyID(newsid);
            if (curruser == null)
            {
                curruser = new bdmirrorDB.user();
                curruser.priviledge = "0000";
            }
            if (nz.published == "1" || curruser.priviledge.ToCharArray()[3] == '1')
            {
                Page.Title = nz.title_bn;
                Page.MetaDescription = nz.content_bn;
                Page.MetaKeywords = "citibd.com,civil journalism," + nz.category_id.name_bn + "," + nz.title_bn;
                ndao.increaseread(nz.news_id);
                hplcat.Text = nz.category_id.name_bn;
                hplcat.NavigateUrl = "~/Category.aspx?catname=" + hplcat.Text;
                hpltitle.Text = nz.title_bn;
                hpltitle.NavigateUrl = "~/newsdetails.aspx?newsid=" + nz.news_id;
                hpldate.Text = Utility.retDate(nz.insert_time);
                hpldate.NavigateUrl = "Search.aspx?date=" + Utility.retDate(nz.insert_time);
                bdmirrorDB.contents cont = new ContentDAO().pickbyID(nz.contents.content_id);
                lblcontent.Text = cont.english_content;
                hplcountry.Text = nz.country_id.country_name;
                if (nz.country_id.country_id == 19)
                {
                    pnlothers.Visible = true;
                    hpldistrict.Text = (nz.district_id!=null)?nz.district_id.DistrictName:"Not Given";
                }
                DataTable dtImg = new DataTable("dtimg");
                dtImg.Columns.Add("id", typeof(string));
                dtImg.Columns.Add("watermark", typeof(string));
                dtImg.Columns.Add("caption", typeof(string));
                dtImg.Columns.Add("height", typeof(string));
                dtImg.Columns.Add("width", typeof(string));
                IList<bdmirrorDB.images> imgs = new ImageDAO().GetImagesbyNewsID(nz);
                if (imgs.Count != 0)
                {
                    foreach (bdmirrorDB.images img in imgs)
                    {
                        dtImg.Rows.Add(img.image_id.ToString(), img.image_watermarked, img.caption_en, img.height, img.width);
                    }
                    rptimg.DataSource = dtImg;
                    rptimg.DataBind();
                    rptimgsmall.DataSource = dtImg;
                    rptimgsmall.DataBind();
                }
                else
                    myCarousel.Visible = false;

            }
            else
            {
                
                //if (curruser.priviledge.ToCharArray()[3] != '1')
                    Response.Redirect("default.aspx");
            }
            loadlatest();
            loadpopular();
            loadprofile(nz);
        }
        protected void loadlatest()
        {
            DataTable dtlatest = new DataTable("dtlatest");
            dtlatest.Columns.Add("newslink", typeof(string));
            dtlatest.Columns.Add("imagelink", typeof(string));
            dtlatest.Columns.Add("newstitle", typeof(string));
            dtlatest.Columns.Add("newsdate", typeof(string));
            IList<bdmirrorDB.news> latestnews = ndao.GetLatestNews().Take(11).ToList();
            foreach (bdmirrorDB.news nz in latestnews)
            {
                bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
                if (img.news_id == null)
                    img = imgdao.pickbyID(233);
                dtlatest.Rows.Add("/NewsDetails.aspx?newsid=" + nz.news_id + "&title=" + nz.title_bn, img.image_watermarked, nz.title_bn, Utility.retDate(nz.insert_time));
            }
            rptlatest.DataSource = dtlatest;
            rptlatest.DataBind();
        }
        protected void loadpopular()
        {
            DataTable dtlatest = new DataTable("dtpopular");
            dtlatest.Columns.Add("newslink", typeof(string));
            dtlatest.Columns.Add("imagelink", typeof(string));
            dtlatest.Columns.Add("newstitle", typeof(string));
            dtlatest.Columns.Add("newsdate", typeof(string));
            dtlatest.Columns.Add("newscontent", typeof(string));
            IList<bdmirrorDB.news> latestnews = ndao.getMostread().Take(4).ToList();
            foreach (bdmirrorDB.news nz in latestnews)
            {

                bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
                if (img.news_id == null)
                    img = imgdao.pickbyID(233);
                dtlatest.Rows.Add("/NewsDetails.aspx?newsid=" + nz.news_id + "&title=" + nz.title_bn, img.image_watermarked, nz.title_bn, Utility.retDate(nz.insert_time), nz.content_bn);
            }
            rptmostread.DataSource = dtlatest;
            rptmostread.DataBind();
        }
        protected void loadprofile(bdmirrorDB.news nz)
        {

            hplprofile.Text = nz.writer.first_name;
            hplprofile.NavigateUrl ="Profile.aspx?id="+nz.writer.user_id+"&name="+ Server.UrlEncode( nz.writer.first_name);
            imgpro.ImageUrl = nz.writer.profile_pic.link;
        }
         #region repeater
        protected void rptimg_bound(object sender, RepeaterItemEventArgs e)
        {
            Image img = (Image)e.Item.FindControl("img");
            img.Height = new Unit( DataBinder.Eval(e.Item.DataItem, "height").ToString());
            img.Width =new Unit( DataBinder.Eval(e.Item.DataItem, "width").ToString());
        }
        #endregion
    }
}