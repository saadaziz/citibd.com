﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="citibd.Profile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
      
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Dashboard Login to Citibd.com - Your home to journalism">
<meta name="author" content="">

<link rel="icon" 
      type="image/png" 
      href="http://example.com/myicon.png">
<!-- styles -->
<link href="user/css/bootstrap.css" rel="stylesheet">
<link href="user/css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" href="user/css/font-awesome.css">
<!--[if IE 7]>
            <link rel="stylesheet" href="user/css/font-awesome-ie7.min.css">
        <![endif]-->
<link href="user/css/styles.css" rel="stylesheet">
<link href="user/css/theme-wooden.css" rel="stylesheet">

<!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="user/css/ie/ie7.css" />
        <![endif]-->
<!--[if IE 8]>
            <link rel="stylesheet" type="text/css" href="user/css/ie/ie8.css" />
        <![endif]-->
<!--[if IE 9]>
            <link rel="stylesheet" type="text/css" href="user/css/ie/ie9.css" />
        <![endif]-->
<link href="user/css/aristo-ui.css" rel="stylesheet">
<link href="user/css/elfinder.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
<!--fav and touch icons -->
<link rel="shortcut icon" href="ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
<!--============j avascript===========-->
<script src="user/js/jquery.js" type="text/javascript"></script>
<script src="user/js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="user/js/bootstrap.js" type="text/javascript"></script>
</head>
<body>
 
   <form id="form1" runat="server">
   
   <div class="layout">
	<!-- Navbar================================================== -->
	<div class="navbar navbar-inverse top-nav">
		<div class="navbar-inner">
			<div class="container">
				<span class="home-link"><a href="default.aspx"" class="icon-home"></a></span><a class="brand" href="default.aspx"><img src="http://www.citibd.com/Uploads/citibd.jpg" width="103" height="50" alt="সিটিবিডি.কম"></a>
				<div class="btn-toolbar pull-right notification-nav">
					<div class="btn-group">
						<div class="dropdown">
							<a class="btn btn-notification"><i class="icon-reply"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
            <button class="close" data-dismiss="alert" type="button">
                ×</button>
            <i class="icon-exclamation-sign"></i><strong>ভুল হয়েছে !</strong>
            <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
        </asp:Panel>
	<div class="container">
        <div class="form-signin1">
            <h3 class="form-signin-heading">
                Citbd.com User:</h3>
            <table >
                <tr>
                    <td style="width: 55%;">
                        <table>
                            <tr>
                                <td><asp:HyperLink ID="hplname" runat="server" Text="Label"></asp:HyperLink></td></tr>
                                <tr> <td>Joined &nbsp;&nbsp;<asp:Label ID="lbljoined" runat="server" Text="Label"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 45%;" class="pull-right">
                        <asp:Image ID="imgpropic" Height="125px" Width="125px" runat="server" />
                    </td>
                </tr>
                <tr><td></td></tr>
                                <tr><td></td></tr>
                <tr><td></td></tr>
                <tr><td></td></tr>
                <tr><td></td></tr>

                <tr>
                <td colspan="2">
                <ul class="clearfix switch-item">
                   
                    <li><span class="notify-tip">
                        <asp:Label ID="lblAllCount" runat="server" Text="20"></asp:Label></span>
                        <a href="#" class="light-blue"><i class="icon-cogs"></i><span>All News</span></a></li>
                    <li><span class="notify-tip">
                        <asp:Label ID="lblapprovedCount" runat="server" Text="20"></asp:Label></span><a href="#" class=" blue-violate"><i class="icon-lightbulb"></i><span>Approved</span></a></li>
                    <li><span class="notify-tip">
                        <asp:Label ID="lblPendingCount" runat="server" Text="20"></asp:Label></span><a href="#" class="dark-yellow"><i class="icon-bar-chart"></i><span>Pending</span></a></li>
                    <li><span class="notify-tip">
                        <asp:Label ID="lblRejectedCount" runat="server" Text="20"></asp:Label></span><a href="#" class="green"><i class="icon-shopping-cart"></i><span>Rejected</span></a></li>
                   
                </ul>
                </td></tr>
            </table>
		</div>
	</div>
</div>
   </form>
</body>
</html>


