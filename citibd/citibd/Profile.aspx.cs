﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
namespace citibd
{
    public partial class Profile : System.Web.UI.Page
    {
        userDAO udao = new userDAO(); 
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["name"]!=null)
            {
                bdmirrorDB.user usr = udao.pickbyID(int.Parse(Request.QueryString["id"].ToString()));
                if (usr == null)
                    Response.Redirect("default.aspx");
                else
                    loadprofile(usr);
            }
            else
                Response.Redirect("default.aspx");
        }
        protected void loadprofile(bdmirrorDB.user usr)
        {
            lbljoined.Text = Utility.retDate(usr.joined_date);
            hplname.Text = usr.first_name + " " + usr.last_name;
            hplname.NavigateUrl = "Profile.aspx?id=" + usr.user_id + "&name=" + Server.UrlEncode(usr.first_name);
            imgpropic.ImageUrl = usr.profile_pic.link;
            imgpropic.AlternateText = hplname.Text;
            IList<bdmirrorDB.news> news = new NewsDAO().GetAllNewsByUser(usr);
            int approved = 0, pending = 0, rejected = 0;
            foreach (bdmirrorDB.news nz in news)
            {
                if (nz.published == "0") pending++;
                else if (nz.published == "1") approved++;
                else rejected++;
            }
            lblAllCount.Text = news.Count.ToString();
            lblapprovedCount.Text = approved.ToString();
            lblRejectedCount.Text = rejected.ToString();
            lblPendingCount.Text = pending.ToString();
            Page.Title = "Citibd.com Civil Journalist: " + usr.first_name;
            Page.MetaKeywords = "citibd.com,civil jounalism bangladesh,civil jounalism dhaka,photo jounalism bangladesh,photo jounalism dhaka," + usr.first_name;
            Page.MetaDescription = "An excellent civil journalist named" + usr.first_name+ "joined in "+usr.joined_date.ToLongDateString()+". And contiributed like "+lblapprovedCount.Text+" news as  a civil jounalist";
        }
    }
}