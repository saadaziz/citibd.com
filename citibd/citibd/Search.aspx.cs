﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;
using System.Data;

namespace citibd
{
    public partial class Search : System.Web.UI.Page
    {
        #region private properties
        getImages getimg = new getImages();
        
        static Utility uit = new Utility();
        bdmirrorDB.admdivision admdivision = new bdmirrorDB.admdivision();
        static int noviews = 0;
        private int divisionid
        {
            get
            {
                if (ViewState["divisionid"] == null)
                    ViewState["divisionid"] = -1;
                return (int)ViewState["divisionid"];
            }
            set
            {
                ViewState["divisionid"] = value;
            }
        }
        IList<bdmirrorDB.news> newz = new List<bdmirrorDB.news>();

        NewsDAO ndao = new NewsDAO();
        ImageDAO imgdao = new ImageDAO();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["division"] != null)
                {
                    loaddivision();
                }
                else if (Request.QueryString["Searchtag"] != null)
                {
                    loadtext();
                }
                else
                    Response.Redirect("default.aspx");
                
            }
        }
        protected void loadtext()
        {
            mulsearch.ActiveViewIndex = 1;

            IList<bdmirrorDB.news> news = ndao.All().Where(x => x.title_bn.Contains(Request.QueryString["Searchtag"].ToString())).ToList();
            if (news.Count != 0)
            {
                DataTable dt = getimg.loadrows(news);
                rptrow.DataSource = dt;
                rptrow.DataBind();
                Page.Title = Request.QueryString["Searchtag"].ToString() + " .::. Your home to civil journalism.";
                Page.MetaDescription = Request.QueryString["Searchtag"].ToString() + " news from dhaka,rajshahi,barishal,chittagong,rangpur,sylhet,khulna." + Request.QueryString["Searchtag"].ToString() + " Civil journalism of bangladesh";
                Page.MetaKeywords = "civil journalism bangladesh,civil journalism dhaka,photo journalism dhaka," + Request.QueryString["Searchtag"].ToString();

            }
            else
                lblmsg.Text = "No results found.";
        }
        protected void loaddivision()
        {
            mulsearch.ActiveViewIndex = 0;
            admdivision = new DivisionDAO().pickbyID(int.Parse(Request.QueryString["id"].ToString()));
            divisionid = Convert.ToInt32(admdivision.division_id);
            bdmirrorDB.admcountry countryn = new CountryDAO().pickbyID(19);
            IList<bdmirrorDB.admdistrict> districts = new DistrictDAO().GetAllDistrictByDivision(divisionid);
            IList<bdmirrorDB.news> newztemp = ndao.getAllNewsbyCountry(countryn);
            foreach (bdmirrorDB.admdistrict dis in districts)
            {

                IList<bdmirrorDB.news> tnews = newztemp.Where(x => x.district_id != null).Where(x => x.district_id.DistrictID == dis.DistrictID).ToList();
                foreach (bdmirrorDB.news disr in tnews)
                    newz.Add(disr);
            }
            DataTable dt = getimg.loadrows(newz);
            rptrow.DataSource = dt;
            rptrow.DataBind();
            Page.Title = admdivision.divName + " .::. Your home to civil journalism.";
            Page.MetaDescription = admdivision.divName + " news from dhaka,rajshahi,barishal,chittagong,rangpur,sylhet,khulna." + admdivision.divName + " Civil journalism of bangladesh";
            Page.MetaKeywords = "civil journalism bangladesh,civil journalism dhaka,photo journalism dhaka," + admdivision.divName;

            hdnval.Value = admdivision.ToString();        
        }
    }
}