﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountSettings.aspx.cs"
    Inherits="citibd.User.AccountSettings" MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
    <title>Account Settings : Citibd.com - Your way to journalism</title>
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <asp:ScriptManager ID="scrpt" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updtpnl">
        <ContentTemplate>
            <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
                <button class="close" data-dismiss="alert" type="button">
                    ×</button>
                <i class="icon-exclamation-sign"></i><strong>Message!</strong>
                <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
            </asp:Panel>
            <div class="row-fluid ">
                <div class="span12">
                    <div class="primary-head">
                        <h3 class="page-header">
                            Account Settings</h3>
                       
                    </div>
                  
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="content-widgets light-gray">
                        <div class="widget-head blue">
                            <h3>
                                Account Settings</h3>
                        </div>
                        <div class="widget-container">
                            <div class="div-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Email Address</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtemail" runat="server" placeholder="Email address" class="span8"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtemail"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SerialValidationGroup"
                                            ErrorMessage="Please insert a valid email" ToolTip="Please insert a valid email"
                                            runat="server"><img src="../img/Left_Arrow.png" alt="*" /></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Old Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtopass" runat="server" placeholder="Text Input" class="span8"
                                            TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtopass"
                                            CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                            ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtpass" runat="server" placeholder="Text Input" class="span8"
                                            TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpass"
                                            CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                            ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Confirm Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtcpass" runat="server" placeholder="Text Input" class="span8"
                                            TextMode="Password"></asp:TextBox>
                                        <asp:CompareValidator ID="compval" ControlToValidate="txtpass" ControlToCompare="txtcpass"
                                            ErrorMessage="Password didnt match" ToolTip="Password didnt match" runat="server"
                                            ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:CompareValidator>
                                    </div>
                                </div>
                                <div class="div-actions">
                                    <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-success"
                                        OnClick="btnsave_click" />
                                    <button type="button" class="btn">
                                        Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
