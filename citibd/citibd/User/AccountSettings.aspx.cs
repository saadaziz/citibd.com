﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;

namespace citibd.User
{
    public partial class AccountSettings : System.Web.UI.Page
    {
        static bdmirrorDB.user user = new bdmirrorDB.user();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bdmirrorDB.user userid = Utility.get_ormuserid();
                if (userid != null)
                {
                    //if (user != null)
                        LoadAllValues();
                }
            }
        }
        protected void btnsave_click(object sender, EventArgs e)
        {
            pnlalert.Visible = true;
            if (user.password == txtopass.Text)
            {
                if (txtpass.Text == txtcpass.Text)
                {
                    user.email = txtemail.Text;
                    user.password = (txtpass.Text != "") ? txtpass.Text : user.password;
                    if (new userDAO().update(user))
                        lblmsg.Text = "Your Account Information has been successfully changed. Please check your email to confirm.";
                }
            }
            else
            {
                lblmsg.Text = "Your old password didnt match!";
            }
        }
        protected void LoadAllValues()
        {
           user = Utility.get_ormuserid();
            txtemail.Text = user.email;
        }
    }
}