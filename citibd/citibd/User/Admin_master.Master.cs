﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using citibd;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Data;
using System.Web.UI.HtmlControls;

namespace bdmirror24.User
{
    public partial class Admin_master : System.Web.UI.MasterPage
    {
        private int log = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string username_c = (string)null;
                string username_s = (string)HttpContext.Current.Session["UserName_citibd"];
                //Session["UserName_citibd"];
                HttpCookie cookie = Request.Cookies["UserInfo_citibd"];

                if (cookie != null)
                {
                    username_c = cookie["UserName_citibd"];
                    if ((username_c != null) && (username_s == username_c))
                    {
                        log = 1;
                    }
                    

                }

                if (log != 1)
                    Response.Redirect("~/user/login.aspx");
                bdmirrorDB.user user = Utility.get_ormuserid();
                if(user!=null)
                    loadcontrolvalues(user);
                else
                    Response.Redirect("~/user/login.aspx");
            }

        }
        protected void loadcontrolvalues(bdmirrorDB.user user)
        {
            
            
            IList<bdmirrorDB.news> news = new NewsDAO().GetAllNewsByUser(user);
            lblusername.Text = user.first_name;
            lblprocomple.Text = user.profile_complete + "%";
            int approved = 0, pending = 0, rejected = 0;
            foreach (bdmirrorDB.news nz in news)
            {
                if (nz.published == "0") pending++;
                else if (nz.published == "1") approved++;
                else rejected++;
            }
            lblAllCount.Text = news.Count.ToString();
            lblapprovedCount.Text = approved.ToString();
            lblRejectedCount.Text = rejected.ToString();
            lblPendingCount.Text = pending.ToString();
            List<string> link= Request.Url.AbsoluteUri.ToString().Split('/').ToList();
            lblpagename.Text = ret_name(link.Last());
            imgpropic.ImageUrl = user.profile_pic.image_watermarked;
            DataTable dtNews = new DataTable("dtNoti");
            dtNews.Columns.Add("id", typeof(string));
            dtNews.Columns.Add("noti", typeof(string));
            dtNews.Columns.Add("time", typeof(string));
            dtNews.Columns.Add("status", typeof(string));
            IList<bdmirrorDB.notification> notis = new NotificationDAO().getAllNotification(user); ;
            lblnotificationCount.Text = notis.Count.ToString();
            lblnotificationCount2.Text = notis.Count.ToString();
            foreach (bdmirrorDB.notification noti in notis)
            {
                string tm = ((DateTime.Now-noti.insert_time).Hours==0)?(DateTime.Now-noti.insert_time).Minutes+" minutes":(DateTime.Now-noti.insert_time).Hours+" hours";
                //string timev = ((DateTime.Now-noti.insert_time).Hours==0)?"minutes":"hours";
                dtNews.Rows.Add(noti.ToString(),noti.notifications,tm.ToString(),noti.status);
            }
            rptNoti.DataSource = dtNews;
            rptNoti.DataBind();
            if (user.priviledge.ToArray()[4] == '1')
                liuser.Attributes.Add("style", "display:block;"); 
            else
                liuser.Attributes.Add("style", "display:none;"); 

        }
        protected string ret_name(string input)
        {
            
            switch (input)
            {
                case "AllNews.aspx?status=published":
                    return "Published News";
                case "AllNews.aspx?status=rejected":
                    return "Rejected News";
                case "AllNews.aspx?status=pending":
                    return "Pending News";
                case "AllNews.aspx":
                    return "All News";
                case "PostNews.aspx":
                    return "Post News";
                case "EditProfile.aspx":
                    return "Edit Profile";
                case "AccountSettings.aspx":
                    return "Account Settings";
                case "Dashboard.aspx":
                    return "Dashboard";
                default:
                    return "Citibd.com";
            }
        }
        //#region repeater
        //protected void rptCat_bound(object sender, RepeaterItemEventArgs e)
        //{
            
        //        HyperLink hplLink = (HyperLink)e.Item.FindControl("hplLink");
        //        if (hplLink != null)
        //        {
        //            hplLink.Text = DataBinder.Eval(e.Item.DataItem, "name_en").ToString();
        //            hplLink.NavigateUrl = "~/User/Home/" + Server.UrlEncode(DataBinder.Eval(e.Item.DataItem, "name_en").ToString()) + "/";
        //        }
            

        //}
        //#endregion
    }
}