﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllNews.aspx.cs" Inherits="citibd.User.AllNews"
    MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
    <title>All of your News : Citibd.com - Your way to journalism</title>
   
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <asp:ScriptManager ID="scrpt" runat="server">
    </asp:ScriptManager>
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
        <button class="close" data-dismiss="alert" type="button">
            ×</button>
        <i class="icon-exclamation-sign"></i><strong>Message!</strong>
        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
    </asp:Panel>
    <div class="row-fluid">
        <div class="span12 switch-board gray">
            <div class="span4">
                Search by Title :
                <asp:TextBox ID="txtSearch" runat="server" Width="60%"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgtextsearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                    ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch2_Click" />
            </div>
            <div class="span4">
                Category :
                <asp:DropDownList ID="ddlCat" runat="server">
                </asp:DropDownList>
                &nbsp;
                <asp:ImageButton ID="imgcatsearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                    ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch2_Click" />
            </div>
            <div class="span4 pull-right">
                Date From
                <asp:TextBox ID="txtFromDate" runat="server" Width="80px"></asp:TextBox>
                <cc1:CalendarExtender ID="ceFromDate" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate" />
                &nbsp;To
                <asp:TextBox ID="txtToDate" runat="server" Width="80px"></asp:TextBox>
                <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate" Format="dd/MM/yyyy" />
                <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                    ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/img/Refresh.png"
                    ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
            </div>
        </div>
        <div class="span12 switch-board ">
            <asp:Panel ID="pnlaccept" runat="server" Visible="false">
                <asp:Label ID="lblids" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Button ID="Button2" runat="server" Text="Publish" CssClass="btn btn-success"
                    OnClick="btnpublish_click" />
                <asp:Button ID="Button1" runat="server" Text="Reject" CssClass="btn btn-danger" OnClick="btnpublish_click" />
                <asp:Button ID="btnlat" runat="server" Text="Latest" CssClass="btn btn-warning" OnClick="btnLattop_click" />
                <asp:Button ID="btntopslider" runat="server" Text="Top Slider" CssClass="btn btn-inverse"
                    OnClick="btnLattop_click" />
                <asp:Button ID="btntopnews" runat="server" Text="Spotlight" CssClass="btn btn-info"
                    OnClick="btnLattop_click" />
            </asp:Panel>
        </div>
    </div>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="grvNews" DataKeyNames="id" runat="server" Width="100%" AutoGenerateColumns="False"
                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvNews_PageIndexChanging"
                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvNews_RowDataBound"  AllowSorting="true"
                    EmptyDataText="No Records Found" CssClass="dark-table table table-dark table-striped table-sortable table-bordered"
                    AllowPaging="true" PageSize="30">
                    <Columns>
                     <asp:TemplateField  HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="1%">
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" runat="server" />
                            </ItemTemplate>
                            <HeaderTemplate>  
                                <asp:CheckBox ID="chkMainheadr"  
                                OnCheckedChanged="chkMainheadr_CheckedChanged"  
                                AutoPostBack="true" runat="server" />  
                            </HeaderTemplate>  
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="1%">
                            <ItemTemplate>
                                <asp:Label ID="lblnewsid" Text='<%# Eval("id") %>' Visible="false" runat="server" HorizontalAlign="Left" />
                                <asp:Label ID="lblid" Text='<%# Eval("serial") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Title" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblentitle" Text='<%# (Eval("title")) %>' runat="server" HorizontalAlign="Left"
                                    ToolTip='<%# (Eval("content")) %>' NavigateUrl='<%# (Eval("link")) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Published" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <div class="btn-group">
                                    <asp:Label ID="lblpub" CssClass="btn btn-success" Visible="false" runat="server"
                                        Text="Published" />
                                    <asp:Label ID="lblrej" runat="server" Text="Rejected" Visible="false" CssClass="btn btn-danger" />
                                    <asp:Label ID="lblrev" runat="server" Text="Reviewing" Visible="false" CssClass="btn btn-info" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Tags" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <div class="btn-group">
                                    <asp:Label ID="lbltopslider" CssClass="btn-mini btn-inverse" Visible="false" runat="server"
                                        Text="Top Slider" />
                                    <asp:Label ID="lblLatest" runat="server" Text="Latest" Visible="false" CssClass="btn-mini btn-warning" />
                                    <asp:Label ID="lbltopnews" runat="server" Text="Spotlight" Visible="false" CssClass="btn-mini btn-info" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="4%">
                            <ItemTemplate>
                                <asp:Label ID="lblcat" Text='<%# Eval("cat") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         
                        <asp:TemplateField HeaderText="Date" SortExpression="dates" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lbldate" Text='<%# Eval("dates") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="3%">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplimage" NavigateUrl='<%# Eval("image") %>' Text='<%# Eval("imagedim") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="hpledit" runat="server" Target="_blank">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
               <%-- <asp:SqlDataSource ID="dsNews" runat="server" ProviderName="System.Data.Obdc"
                    OnSelecting="dsNews_Selecting" SelectCommand="SELECT nz.id,nz.title_bn as title,nz.insert_time,nz.published,categories.name_bn as cat,content_bn
from news as nz
INNER JOIN categories on categories.id=nz.category_id
INNER JOIN contents on contents.content_id=nz.content_id
WHERE ((nz.insert_time >= @DateFrom) AND (nz.insert_time <= @DateTo)) ORDER BY nz.insert_time desc"
                    FilterExpression="PartyName LIKE '%{0}%' ">
                    <FilterParameters>
                        <asp:ControlParameter Name="title" ControlID="txtSearch" PropertyName="Text" />
                    </FilterParameters>
                    <SelectParameters>
                        <asp:Parameter Name="DateFrom" Type="DateTime" />
                        <asp:Parameter Name="DateTo" Type="DateTime" />
                    </SelectParameters>
                </asp:SqlDataSource>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
