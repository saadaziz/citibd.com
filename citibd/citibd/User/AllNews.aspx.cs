﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using bdmirror24.DAL;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Data;
namespace citibd.User
{
    public partial class AllNews : System.Web.UI.Page
    {
        #region Private Properties
        //public static string cstring =@"Driver={MySQL ODBC 3.51 Driver};server=localhost;Port=3306;User Id=root;database=boseit2_citibd;password=;Persist Security Info=True;CharSet=UTF8;pooling=true;tablecache=true;DefaultTableCacheAge=30;";
        private string strSearch = string.Empty;
        private string status
        {
            get
            {
                if (ViewState["status"] == null)
                    ViewState["status"] = "";
                return (string)ViewState["status"];
            }
            set
            {
                ViewState["status"] = value;
            }
        }
        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }
        private bool priviledge
        {
            get
            {
                if (ViewState["priviledge"] == null)
                    ViewState["priviledge"] = true;
                return (bool)ViewState["priviledge"];
            }
            set
            {
                ViewState["priviledge"] = value;
            }
        }
        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }
        static DataTable dtNews = new DataTable("dtNews");
        NewsDAO ndao = new NewsDAO();

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bdmirrorDB.user user = Utility.get_ormuserid();
                if (user != null)
                {
                    Utility.BindCategoryDDL(ddlCat);
                    DateFrom = DateTime.Today;
                    txtFromDate.Text = DateFrom.ToString("dd/MM/yyyy");
                    DateTo = DateTime.Today;
                    txtToDate.Text = DateTo.ToString("dd/MM/yyyy");
                    char[] priv = user.priviledge.ToCharArray();
                    priviledge = (priv[3] != '1') ? true : false;
                    pnlaccept.Visible = (priv[3] != '1') ? false : true;
                    status = (Request.QueryString["status"] != null) ? Request.QueryString["status"].ToString() : "";
                    initDT();
                    grvNews.DataSource = dtNews;
                    grvNews.DataBind();
                }
            }
        }
        protected void chkMainheadr_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkMainheadr = (CheckBox)grvNews.HeaderRow.FindControl("chkMainheadr");
            if (chkMainheadr.Checked == true)
            {
                foreach (GridViewRow gvOrdr in grvNews.Rows)
                {
                    CheckBox chkIndividual = (CheckBox)gvOrdr.FindControl("chk");
                    chkIndividual.Checked = true;
                }
            }
            else
            {
                foreach (GridViewRow gvOrdr in grvNews.Rows)
                {
                    CheckBox chkIndividual = (CheckBox)gvOrdr.FindControl("chk");
                    chkIndividual.Checked = false;
                }
            }
        }  

        #region publish/latest/top
        protected void btnpublish_click(object sender, EventArgs e)
        { 
            bool pub =false;
            if (((Button)sender).ID == "Button2")
                pub = true;
            foreach (GridViewRow rw in grvNews.Rows)
            {
                CheckBox chk = (CheckBox)rw.FindControl("chk");
                if (chk.Checked == true)
                {
                    bdmirrorDB.news nz = ndao.pickbyID(int.Parse(((Label)rw.FindControl("lblnewsid")).Text));
                    nz.published = (pub) ? nz.published=="1"?"0":"1" : "2";//gali dile kisu i korar nai
                    ndao.update(nz);
                    chk.Checked = false;
                    bdmirrorDB.notification noti = new bdmirrorDB.notification();
                    noti.user_id = nz.writer;
                    string ress = (pub) ? "published" : "rejected";
                    noti.notifications = "Your news " + nz.title_bn + " is " + ress + ".";
                    noti.insert_time = DateTime.Now;
                    noti.status = "unread";
                    new NotificationDAO().Add(noti);
                }
            }
            initDT();
            grvNews.DataSource = dtNews;
            grvNews.DataBind();
        }
        protected void btnLattop_click(object sender, EventArgs e)
        {
            
            bdmirrorDB.notification noti = new bdmirrorDB.notification();
            foreach (GridViewRow rw in grvNews.Rows)
            {
                CheckBox chk = (CheckBox)rw.FindControl("chk");
                if (chk.Checked == true)
                {
                    bdmirrorDB.news nz = ndao.pickbyID(int.Parse(((Label)rw.FindControl("lblnewsid")).Text));
                    nz.published =  "1";
                    if (((Button)sender).ID == "btnlat") nz.latest = (nz.latest!=1)?1:0;
                    else if (((Button)sender).ID == "btntopslider")  nz.top_slider = (nz.top_slider != 1) ? 1 : nz.top_slider = 0;
                    else if (((Button)sender).ID == "btntopnews") {
                        ndao.setspotlight();
                        nz.top_news = (nz.top_news != "1") ? "1" : "0";
                    }
                    ndao.update(nz);
                    chk.Checked = false;
                    noti.user_id = nz.writer;
                    noti.notifications = "Congratulations, your news " + nz.title_bn + " is selected as a Spotlight news and can be found in the home page.";
                    noti.insert_time = DateTime.Now;
                    noti.status = "unread";
                    new NotificationDAO().Add(noti);
                    send_email.s_email_sub(nz.writer.email, noti.notifications, "Notification from Citibd.com","notification");
                }
            }
            initDT();
            grvNews.DataSource = dtNews;
            grvNews.DataBind();
        }
        #endregion
        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion
        #region Methods For Grid
        protected void grvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvNews.PageIndex = e.NewPageIndex;
            IList<bdmirrorDB.news> news = (priviledge) ? ndao.GetAllNewsByUser(Utility.get_ormuserid()) : ndao.All();
            news.Skip(grvNews.PageSize * (e.NewPageIndex)).Take(grvNews.PageSize);
            initDT(news);

            grvNews.DataSource = dtNews;
            grvNews.DataBind();
        }

        protected void grvNews_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[0].Controls.Count > 0))
            {
                if (DataBinder.Eval(e.Row.DataItem, "pub").ToString() == "0")
                {
                    Label lblrev = (Label)e.Row.FindControl("lblrev");
                    lblrev.Visible = true;
                }
                else if (DataBinder.Eval(e.Row.DataItem, "pub").ToString() == "1")
                {
                    ((Label)e.Row.FindControl("lblpub")).Visible = true;
                    
                }
                else
                {
                    ((Label)e.Row.FindControl("lblrej")).Visible = true;
                }
                HyperLink btnEdit = (HyperLink)e.Row.FindControl("hpledit");
                btnEdit.NavigateUrl="~/user/PostNews.aspx?newsid="+(DataBinder.Eval(e.Row.DataItem, "id").ToString());
                HyperLink hplimage = (HyperLink)e.Row.FindControl("hplimage");
                if (DataBinder.Eval(e.Row.DataItem, "image").ToString() == "")
                    hplimage.Visible = false;
                string[] tag = DataBinder.Eval(e.Row.DataItem, "tags").ToString().Split(',');
                if (tag[0] == "1")
                    ((Label)e.Row.FindControl("lblLatest")).Visible = true;
                if (tag[1] == "1")
                    ((Label)e.Row.FindControl("lbltopnews")).Visible = true;
                if (tag[2] == "1")
                    ((Label)e.Row.FindControl("lbltopslider")).Visible = true;
            }
        }
        #endregion

        #region init
        protected void initDT()
        {
            IList<bdmirrorDB.news> news = (priviledge) ? ndao.GetAllNewsByUser(Utility.get_ormuserid()): ndao.All();
            if(status=="published")
                news = news.Where(x =>x.published=="1").ToList();
            else if (status == "rejected")
                news = news.Where(x => x.published == "2").ToList();
            else if (status == "pending")
                news = news.Where(x => x.published == "0").ToList();
            dtNews = new DataTable("dtNews");
            dtNews.Columns.Add("id", typeof(string));
            dtNews.Columns.Add("serial", typeof(string));
            dtNews.Columns.Add("title", typeof(string));
            dtNews.Columns.Add("link", typeof(string));
            dtNews.Columns.Add("content", typeof(string));
            dtNews.Columns.Add("cat", typeof(string));
            dtNews.Columns.Add("dates", typeof(string));
            dtNews.Columns.Add("pub", typeof(string));
            dtNews.Columns.Add("image", typeof(string));
            dtNews.Columns.Add("imagedim", typeof(string));
            dtNews.Columns.Add("tags", typeof(string));//latest,topnews,topslider
            int i = 0;
            ImageDAO imgdao = new ImageDAO();
            bdmirrorDB.images img = new bdmirrorDB.images();
            foreach (bdmirrorDB.news nz in news.OrderByDescending(x=>x.insert_time).ToList())
            {
                img =imgdao.GetImagebyNewsID(nz);
                dtNews.Rows.Add(nz.news_id.ToString(), (++i).ToString(), nz.title_bn, "~/NewsDetails.aspx?newsid=" + nz.news_id, nz.content_bn, nz.category_id.name_bn,
                    nz.insert_time.ToString("dd/MM/yyyy"),nz.published.ToString(),
                    img.image_watermarked, img.width + "x" + img.height, nz.latest + "," + nz.top_news + "," + nz.top_slider.ToString());
            }
        }
        protected void initDT(IList<bdmirrorDB.news> news)
        {
            if (status == "published")
                news = news.Where(x => x.published == "1").ToList();
            else if (status == "rejected")
                news = news.Where(x => x.published == "2").ToList();
            else if (status == "pending")
                news = news.Where(x => x.published == "0").ToList();
            dtNews = new DataTable("dtNews");
            dtNews.Columns.Add("id", typeof(string));
            dtNews.Columns.Add("serial", typeof(string));
            dtNews.Columns.Add("title", typeof(string));
            dtNews.Columns.Add("link", typeof(string));
            dtNews.Columns.Add("content", typeof(string));
            dtNews.Columns.Add("cat", typeof(string));
            dtNews.Columns.Add("dates", typeof(string));
            dtNews.Columns.Add("pub", typeof(string));
            dtNews.Columns.Add("image", typeof(string));
            dtNews.Columns.Add("imagedim", typeof(string));
            dtNews.Columns.Add("tags", typeof(string));//latest,topnews,topslider
            int i = 0;
            ImageDAO imgdao = new ImageDAO();
            bdmirrorDB.images img = new bdmirrorDB.images();
            foreach (bdmirrorDB.news nz in news.OrderByDescending(x => x.insert_time).ToList())
            {
                img = imgdao.GetImagebyNewsID(nz);
                dtNews.Rows.Add(nz.news_id.ToString(), (++i).ToString(), nz.title_bn,"~/NewsDetails.aspx?newsid="+nz.news_id, nz.content_bn, nz.category_id.name_bn,
                    nz.insert_time.ToString("dd/MM/yyyy"), nz.published.ToString(),
                    img.image_watermarked, img.width + "x" + img.height, nz.latest + "," + nz.top_news + "," + nz.top_slider.ToString());
            }
        }
        #endregion
        #region Methods For Button
        protected void imgRefresh_Click(object sender, EventArgs e)
        {
            initDT();

            grvNews.DataSource = dtNews;
            grvNews.DataBind();
        }
        protected void imgSearch2_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text.Trim();
            initDT();

            if (strSearch != string.Empty && ((ImageButton)sender).ID == "imgtextsearch")
            {
                DataRow[] rows = dtNews.Select("title Like '%" + strSearch + "%' ");
                databind_byRow(rows);
            }
            else if (((ImageButton)sender).ID == "imgcatsearch")
            {
                DataRow[] rows = dtNews.Select("cat = '" + ddlCat.SelectedItem.Text + "' ");
                databind_byRow(rows);
            }
        }
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            
            DataRow[] rows = dtNews.Select(string.Format("dates >= '{0}' AND dates <= '{1}'", DateFrom.ToString("dd/MM/yyyy"), DateTo.ToString("dd/MM/yyyy")));
            databind_byRow(rows);
            
        }
        protected void databind_byRow(DataRow[] rows)
        {
            DataTable dtnew = new DataTable("dtnew");
            dtnew.Columns.Add("id", typeof(string));
            dtnew.Columns.Add("serial", typeof(string));
            dtnew.Columns.Add("title", typeof(string));
            dtnew.Columns.Add("link", typeof(string));
            dtnew.Columns.Add("content", typeof(string));
            dtnew.Columns.Add("cat", typeof(string));
            dtnew.Columns.Add("dates", typeof(string));
            dtnew.Columns.Add("pub", typeof(string));

            dtnew.Columns.Add("image", typeof(string));
            dtnew.Columns.Add("imagedim", typeof(string));
            dtnew.Columns.Add("tags", typeof(string));
            int i = 1;
            foreach (DataRow rw in rows)
            {
                dtnew.Rows.Add(rw["id"], i++.ToString(), rw["title"],rw["link"], rw["content"], rw["cat"], rw["dates"], rw["pub"], rw["image"], rw["imagedim"],rw["tags"]);
            }
            grvNews.DataSource = dtnew;
            grvNews.DataBind();
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            
            grvNews.DataBind();
        }

        #endregion
    }
}