﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Web.Services;

namespace citibd.User
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bdmirrorDB.user user = Utility.get_ormuserid();
                if (user != null)
                {
                    IList<bdmirrorDB.news> news = new NewsDAO().GetAllNewsByUser(user);
                    int approved = 0, pending = 0, rejected = 0;
                    foreach (bdmirrorDB.news nz in news)
                    {
                        if (nz.published == "0") pending++;
                        else if (nz.published == "1") approved++;
                        else rejected++;
                    }
                    lblAllCount.Text = news.Count.ToString();
                    lblapprovedCount.Text = approved.ToString();
                    lblRejectedCount.Text = rejected.ToString();
                    lblPendingCount.Text = pending.ToString();
                }
            }
        }
        [WebMethod]
        public static string updatenoti()
        {
            bdmirrorDB.user usr = Utility.get_ormuserid();
            new NotificationDAO().updatenoti(usr.user_id);
            return "done";
        }
        
    }
}