﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="citibd.User.EditProfile"
    MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
    <title>Edit Profile : Citibd.com - Your way to journalism</title>
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <asp:ScriptManager ID="scrpt" runat="server">
    </asp:ScriptManager>
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
        <button class="close" data-dismiss="alert" type="button">
            ×</button>
        <i class="icon-exclamation-sign"></i><strong>Message!</strong>
        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
    </asp:Panel>
    <asp:MultiView id="muledit" runat="server">
    <asp:View ID="v1" runat="server">
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets gray">
                <div class="widget-head blue-violate">
                    <h3>
                        About You</h3>
                </div>
                <div class="widget-container">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                First Name</label>
                            <div class="controls">
                                <asp:TextBox CssClass="input-xlarge left-stripe" ID="txtfname" name="fname" placeholder="First Name"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="rfValidator3" runat="server" ControlToValidate="txtfname"
                                    CssClass="failureNotification" ErrorMessage="First Name is required." ToolTip="First Name is required."
                                    ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Last Name</label>
                            <div class="controls">
                                <asp:TextBox CssClass="input-xlarge left-stripe" ID="txtlname" name="lname" placeholder="Last Name"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtlname"
                                    CssClass="failureNotification" ErrorMessage="Last Name is required." ToolTip="Last Name is required."
                                    ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Profile Picture</label>
                            <div class="controls">
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                <em>The height and width of the image must be 80px or higher</em>
                                <asp:RegularExpressionValidator ID="regvalid1" runat="server"  ValidationExpression=".*((\.jpg)|(\.JPG)|(\.bmp)|(\.gif)|(\.png)|(\.JPEG)|(\.jpeg))"
                                    ControlToValidate="FileUpload1" CssClass="failureNotification" ErrorMessage="Only jpg,gif,bmp,png allowed."
                                    ToolTip="Only jpg,gif,bmp,png allowed." ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                National ID. Number</label>
                            <div class="controls">
                                <asp:TextBox CssClass="input-xlarge left-stripe" ID="txtnationalID" name="lname"
                                    placeholder="give us your national id here" runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Enabled="false" ControlToValidate="txtnationalID"
                                    CssClass="failureNotification" ErrorMessage="national id is required." ToolTip="national id is required."
                                    ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Short Biography</label>
                            <div class="controls">
                                <asp:TextBox Rows="5" Columns="40" CssClass="span12" TextMode="MultiLine" runat="server"
                                    ID="txtshortBio" placeholder="write your working education, working experience, personal info here"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head blue">
                    <h3>
                        Contact Details</h3>
                </div>
                <div class="widget-container">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                Phone No.</label>
                            <div class="controls">
                                <asp:TextBox CssClass="input-xlarge  left-stripe" ID="txtcontact" name="lname" placeholder="Phone No"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtcontact"
                                    CssClass="failureNotification" ErrorMessage="Contact no is required." ToolTip="Contact no is required."
                                    ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Present Address</label>
                            <div class="controls">
                                <asp:TextBox CssClass="input-xlarge  left-stripe" TextMode="MultiLine" ID="txtaddress"
                                    name="lname" placeholder="Address" runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtaddress"
                                    CssClass="failureNotification" ErrorMessage="Address is required." ToolTip="Address is required."
                                    ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <asp:UpdatePanel runat="server" ID="updtpnl">
                            <ContentTemplate>
                                <div class="control-group ">
                                    <label class="control-label">
                                        Country</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlCountry" CssClass="input-xlarge left-stripe" runat="server"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCountry"
                                            CssClass="failureNotification" ErrorMessage="Location is required." ToolTip="Location is required."
                                            ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <asp:Panel runat="server" ID="pnlothers">
                                    <div class="control-group ">
                                        <label class="control-label">
                                            Division</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddldivision" CssClass="span4" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <label class="control-label">
                                            District</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddlDistrict" CssClass="span4" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <label class="control-label">
                                            Upzilla</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddlUpzilla" CssClass="span4" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="control-group">
                            <label class="control-label">
                                Postal Code</label>
                            <div class="controls">
                                <asp:TextBox CssClass="input-xlarge" ID="txtpostalcode" name="lname" placeholder="Postal/Zip Code"
                                    runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head blue">
                    <h3>
                        Personal Info</h3>
                </div>
                <div class="widget-container">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                Date of Birth</label>
                            <div class="controls">
                                <asp:TextBox ID="txtdob" runat="server" CssClass="input-xlarge left-stripe" placeholder="Month / Day / Year"></asp:TextBox>
                                <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtdob" Format="MM/dd/yyyy" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Reference Person Name</label>
                            <div class="controls">
                                <asp:TextBox ID="txtrefPname" runat="server" CssClass="input-xlarge" placeholder="optional. any person's name who know you well"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Reference Person Address</label>
                            <div class="controls">
                                <asp:TextBox ID="txtrefPaddr" runat="server" CssClass="input-xlarge" placeholder="optional. his persent address who know you well"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Reference Person Cell No.</label>
                            <div class="controls">
                                <asp:TextBox ID="txtrefPCell" runat="server" CssClass="input-xlarge" placeholder="optional. his phone number who know you well"></asp:TextBox>
                            </div>
                        </div>
                        <div class="div-actions ">
                        <button type="button" class="btn pull-right">
                                Cancel</button>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-success pull-right" OnClick="btnsave_click"
                                ValidationGroup="SerialValidationGroup" />
                            <br />
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </asp:View>
    <asp:View ID="v2" runat="server">
     <p>Thanks, Successfully edited. Your Profile is now <b>
         <asp:Label ID="lblcomplete" runat="server" Text=""></asp:Label>%</b> complete.</p>
</asp:View>
    <asp:View ID="v3" runat="server">
    <p>Oops. Something went terribly wrong.</p>
        <asp:Label ID="lblerrmsg" runat="server" Text=""></asp:Label>
</asp:View>
    </asp:MultiView>
</asp:Content>
