﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using System.IO;
using bdmirror24.DAL.DAL_Functions.Implementations;

namespace citibd.User
{
    public partial class EditProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bdmirrorDB.user user = Utility.get_ormuserid();
                if (user != null)
                {
                    Utility.BindCountryDDL(ddlCountry);
                    pnlothers.Visible = false;
                    Utility.BindDivisionDDL(ddldivision, 0);
                    Utility.BindUpazillaPSDDL(ddlUpzilla, 0);
                    Utility.BindDistrictDDL(ddlDistrict, 0);
                    LoadAllValues(user);
                    txtdob.Text = DateTime.Now.ToShortDateString();
                    muledit.ActiveViewIndex = 0;
                }
            }
        }
        #region LoadAllValues
        protected void LoadAllValues(bdmirrorDB.user user)
        {
            
            txtfname.Text = user.first_name;
            txtlname.Text = user.last_name;
            txtnationalID.Text = user.national_id_no;
            txtcontact.Text = user.contact_no;
            if (user.profile_pic == null)
                regvalid1.Enabled = true;
            txtshortBio.Text = user.short_bio;
            txtdob.Text = user.DOB.ToShortDateString();
            txtaddress.Text = user.Address;
            txtpostalcode.Text = user.PostalCode.ToString();
            txtrefPaddr.Text = user.Reference_Person_addr;
            txtrefPname.Text = user.Reference_Person;
            txtrefPCell.Text = user.Reference_Person_contact;
            ddlCountry.SelectedValue = (user.Country != null) ?
                  user.Country.country_id.ToString() : "-1";

            ddldivision.SelectedValue = (user.Division != null) ?
                user.Division.division_id.ToString() : "-1";
            if (user.Country.country_id == 19)
            { 
                pnlothers.Visible = true;
                //ddlChanged(ddldivision, null);
            }
            ddlDistrict.SelectedValue = (user.District != null) ?
                user.District.DistrictID.ToString() : "-1";
            ddlUpzilla.SelectedValue = (user.Upzilla != null) ?
               user.Upzilla.UpazilaPSID.ToString() : "-1";
        }
        #endregion
        protected void ddlChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            if (ddl.ID == "ddlCountry" && ddl.SelectedValue == "19")
            {
                pnlothers.Visible = true;
                Utility.BindDivisionDDL(ddldivision, 0);
            }
            else if (ddl.ID == "ddldivision")
            {
                Utility.BindDistrictDDL(ddlDistrict, int.Parse(ddl.SelectedValue));
            }
            else if (ddl.ID == "ddlDistrict")
            {
                Utility.BindUpazillaPSDDL(ddlUpzilla, int.Parse(ddl.SelectedValue));
            }
            else
            {
                pnlothers.Visible = false;
            }
        }
        #region save
        protected void btnsave_click(object sender, EventArgs e)
        {
            try
            {
                bdmirrorDB.user euser = Utility.get_ormuserid();
                string oemail=euser.email;
                bdmirrorDB.images images = new ImageDAO().pickbyID(14);
                int pcomplete = euser.profile_complete;
                if (FileUpload1.HasFile)
                {
                    string filename = Path.GetFileName(FileUpload1.FileName);
                    string fileExt = Path.GetExtension(filename);
                    string path = Server.MapPath("~/Uploads") + "\\" + (filename);
                    FileUpload1.SaveAs(path);
                    System.Drawing.Image objImage = System.Drawing.Image.FromFile(path);
                    if (objImage.Height > 80 || objImage.Width > 80)
                    {
                        string wlink = WaterMark.watermark(Server.MapPath("~/Uploads/") + filename, euser.first_name, Server.MapPath("~/uploads/"), filename, false);
                        images.image_watermarked = "http://www.citibd.com/" + "Uploads/" + wlink;
                        images.caption_en = euser.first_name;
                        images.image_name = filename;
                        images.link = "http://www.citibd.com/" + "Uploads/" + (filename);
                        images.width = objImage.Width;
                        images.height = objImage.Height;
                        images.time = DateTime.Now;
                        images = new ImageDAO().Add_ret(images);
                        if (euser.profile_pic==null)
                            pcomplete += 10;
                    }
                    else
                    {
                        File.Delete(path);
                        pnlalert.Visible = true;
                        lblmsg.Text = "The height and width of the image must be 80px or higher";
                    }

                }
                euser.profile_pic = images;
                euser.first_name = txtfname.Text;
                euser.last_name = txtlname.Text;
                euser.contact_no = txtcontact.Text;
                euser.DOB = Convert.ToDateTime(txtdob.Text);
                euser.short_bio = txtshortBio.Text;
                if (txtnationalID.Text.Length == 21 || txtnationalID.Text.Length == 13 || txtnationalID.Text.Length == 17 || txtnationalID.Text.Length == 25 || txtnationalID.Text.Length == 29)
                {
                    euser.national_id_no = txtnationalID.Text;
                    if(euser.national_id_no==null)
                        pcomplete += 20;
                }
                else
                {
                    pnlalert.Visible = true;
                    lblmsg.Text = "Please enter a valid National ID no. next Time";
                }
                euser.Address = txtaddress.Text;
                if (int.Parse(txtpostalcode.Text) != 0)
                    euser.PostalCode = int.Parse(txtpostalcode.Text);
                euser.Country = (ddlCountry.SelectedValue != null) ? new CountryDAO().pickbyID(int.Parse(ddlCountry.SelectedValue)) : null;
                if (pnlothers.Visible == true)
                {
                    euser.District = (ddlDistrict.SelectedValue != null) ? new DistrictDAO().pickbyID(int.Parse(ddlDistrict.SelectedValue)) : null;
                    euser.Division = (ddldivision.SelectedValue != null) ? new DivisionDAO().pickbyID(int.Parse(ddldivision.SelectedValue)) : null;
                    euser.Upzilla = (ddlUpzilla.SelectedValue != null) ? new UpzillaDAO().pickbyID(int.Parse(ddlUpzilla.SelectedValue)) : null;
                }
                euser.Reference_Person = txtrefPname.Text;
                euser.Reference_Person_addr = txtrefPaddr.Text;
                euser.Reference_Person_contact = txtrefPCell.Text;
                euser.profile_complete = pcomplete;
                new userDAO().update(euser);
                lblcomplete.Text = pcomplete.ToString();
                muledit.ActiveViewIndex = 1;

                string emailcont = "Dear " + euser.first_name + ",\n Your profile has been updated. Your new password is:" + euser.password +
                    "\n for anyinconvenience please email to support@citibd.com";
                send_email.s_email_sub(oemail, emailcont, "Profile Edited in Citibd.com", "support");

            }
            catch (Exception ex)
            {
                //pnlalert.Visible = true;
                lblerrmsg.Text = ex.Message + ex.StackTrace;
                muledit.ActiveViewIndex = 2;

            }
        }
        #endregion
    }
}