﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.IO;
using System.Net;
using System.Drawing;
using System.Data;
namespace citibd.User
{
    public partial class ImageUpload : System.Web.UI.Page
    {
        bdmirrorDB.news nz = new bdmirrorDB.news();
        NewsDAO ndao = new NewsDAO();
        ImageDAO imgdao = new ImageDAO();
        static DataTable dtimg = new DataTable("dtimg");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                if (Request.QueryString["newsid"] != null)
                {
                    dtimg = new DataTable("dtimg");
                    if (dtimg.Columns.Count < 1)
                    {
                        dtimg.Columns.Add("id", typeof(string));
                        dtimg.Columns.Add("image_link", typeof(string));
                        dtimg.Columns.Add("image_name", typeof(string));
                    }
                    nz = ndao.pickbyID(int.Parse(Request.QueryString["newsid"].ToString()));
                    IList< bdmirrorDB.images> imgs = new ImageDAO().GetImagesbyNewsID(nz);
                    foreach (bdmirrorDB.images img in imgs)
                    {
                        if (dtimg.Select("id = "+img.image_id).Length==0)
                            dtimg.Rows.Add(new string[] { img.image_id.ToString(), img.image_watermarked, img.caption_en });
                    }

                    grdimg.DataSource = dtimg;
                    grdimg.DataBind();
                }
                else
                    Response.Redirect("~/user/postnews.aspx");
                
            }
        }
        protected void addimage_click(object sender, EventArgs e)
        {
            
            if (FileUpload1.HasFile)
            {
                Random rand = new Random();
                int rn = new Utility().get_rand(100000);
                string filename = Path.GetFileName(FileUpload1.FileName).Replace(" ","").Replace("+", "");
                string fileExt = Path.GetExtension(filename);
                string path = Server.MapPath("~/Uploads") + "\\" + (rn+filename);
                FileUpload1.SaveAs(path);
                System.Drawing.Image objImage = System.Drawing.Image.FromFile(path);
                if (objImage.Height >140 || objImage.Width > 140)
                {
                    nz = ndao.pickbyID(int.Parse(Request.QueryString["newsid"].ToString()));
                    string wlink = WaterMark.watermark(Server.MapPath("~/Uploads/") +rn+ filename, nz.writer_name, Server.MapPath("~/uploads/"), filename,true);
                    bdmirrorDB.images images = new bdmirrorDB.images();
                    images.image_watermarked = "http://www.citibd.com/" + "Uploads/" + wlink;
                    images.caption_en = nz.title_bn;
                    images.image_name =rn+ filename;
                    images.link = "http://www.citibd.com/" + "Uploads/" + (filename);
                    images.width = objImage.Width;
                    images.height = objImage.Height;
                    images.time = DateTime.Now;
                    images.news_id = nz;
                    images = imgdao.Add_ret(images);
                    dtimg.Rows.Add(new string[] { images.image_id.ToString(), images.image_watermarked, images.caption_en });
                    grdimg.DataSource = dtimg;
                    grdimg.DataBind();
                        
                }
                else
                {
                    File.Delete(path);
                    pnlalert.Visible = true;
                    lblmsg.Text = "The height and width of the image must be 140px or higher";
                }
            }
        }
        #region Methods For Grids
        protected void grvimg_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Delete"))
            {
                int intPartyID = Convert.ToInt32(e.CommandArgument.ToString());

                if (intPartyID > 0)
                {
                    DeleteImage(intPartyID);
                }
            }
        }
        protected void grvStockSerial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                Button btnDelete = (Button)e.Row.FindControl("imgDelete");
                btnDelete.CommandArgument = ((Label)e.Row.FindControl("lblnum")).Text;
            }
        }
        #endregion
        #region Methods For Delete
        private void DeleteImage(int id)
        {
            bdmirrorDB.images _tempSerial = new bdmirrorDB.images();
            //_Serial.SerialID = id;
            if (id > 0)
            {
                _tempSerial =  imgdao.pickbyID(id);
                if (imgdao.Delete(_tempSerial))
                {
                    dtimg = new DataTable("dtimg");
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
                }
            }
        }
        #endregion
    }
}