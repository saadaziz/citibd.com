﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostNews.aspx.cs" Inherits="citibd.User.PostNews" ValidateRequest="false" 
    MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
    <title>Post News : Citibd.com - Your way to journalism</title>
    <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript"
        charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"
        type="text/javascript" charset="utf-8"></script>
    <script src="js/tag-it.min.js" type="text/javascript" charset="utf-8"></script>
    
    <script type="text/javascript">
        $(function () {
            var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];

            //-------------------------------
            // Minimal
            //-------------------------------
            $('#myTags').tagit();

            //-------------------------------
            // Single field
            //-------------------------------
            $('#singleFieldTags').tagit({
                availableTags: sampleTags,
                // This will make Tag-it submit a single form value, as a comma-delimited field.
                singleField: true,
                singleFieldNode: $('#mySingleField')
            });

            // singleFieldTags2 is an INPUT element, rather than a UL as in the other 
            // examples, so it automatically defaults to singleField.
            $('#singleFieldTags2').tagit({
                availableTags: sampleTags
            });

            //-------------------------------
            // Preloading data in markup
            //-------------------------------
            $('#myULTags').tagit({
                availableTags: sampleTags, // this param is of course optional. it's for autocomplete.
                // configure the name of the input field (will be submitted with form), default: item[tags]
                itemName: 'item',
                fieldName: 'tags'
            });

            //-------------------------------
            // Tag events
            //-------------------------------
            var eventTags = $('#eventTags');

            var addEvent = function (text) {
                PageMethods.SendForm(text);
                $('#cphBody_events_container').append(text + ',');

            };

            eventTags.tagit({
                availableTags: sampleTags,

                afterTagAdded: function (evt, ui) {
                    if (!ui.duringInitialization) {
                        addEvent(eventTags.tagit('tagLabel', ui.tag));
                    }
                },

                afterTagRemoved: function (evt, ui) {
                    addEvent(eventTags.tagit('tagLabel', ui.tag));
                }

            });

            //-------------------------------
            // Read-only
            //-------------------------------
            $('#readOnlyTags').tagit({
                readOnly: true
            });



        });
    </script>
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <asp:ScriptManager ID="scrpt" runat="server">
    </asp:ScriptManager>
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
        <button class="close" data-dismiss="alert" type="button">
            ×</button>
        <i class="icon-exclamation-sign"></i><strong>Message!</strong>
        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
    </asp:Panel>
   <asp:MultiView ID="mulPost" runat="server">
   <asp:View ID="vPost" runat="server">
            <div class="row-fluid">
                <div class="span12">
                    <div class="content-widgets gray">
                        <div class="widget-head blue-violate">
                            <h3>
                                Basic Information</h3>
                        </div>
                        <div class="widget-container">
                            <div class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">
                                        Select Category</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlCat" runat="server">
                                        </asp:DropDownList>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlCat"
                                                        CssClass="failureNotification" ErrorMessage="Category  is required." ToolTip="Category is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        News Title</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txttitle" type="text" class="tags span10" Text="" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txttitle"
                                                        CssClass="failureNotification" ErrorMessage="News Title  is required." ToolTip="News Title is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">
                                        Keywords</label>
                                    <div class="controls">
                                        <form>
                                        <ul id="eventTags" class="span10">
                                        </ul>
                                        </form>
                                        <asp:Label ID="events_container" runat="server" Visible="false"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <asp:UpdatePanel runat="server" ID="updtpnl">
        <ContentTemplate>
            <div class="row-fluid">
                <div class="span12">
                    <div class="content-widgets gray">
                        <div class="widget-head blue-violate">
                            <h3>
                                Location</h3>
                        </div>
                        <div class="widget-container">
                            <div class="form-horizontal">
                                <div class="control-group ">
                                    <label class="control-label">
                                        Country</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlCountry" CssClass="span4" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCountry"
                                                        CssClass="failureNotification" ErrorMessage="Location is required." ToolTip="Location is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <asp:Panel runat="server" ID="pnlothers">
                                    <div class="control-group ">
                                        <label class="control-label">
                                            Division</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddldivision" CssClass="span4" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <label class="control-label">
                                            District</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddlDistrict" CssClass="span4" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <label class="control-label">
                                            Upzilla</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddlUpzilla" CssClass="span4" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
            <div class="row-fluid">
                <div class="span12">
                    <div class="content-widgets gray">
                        <div class="widget-head blue-violate">
                            <h3>
                                News Content</h3>
                        </div>
                        <div class="widget-container">
                            <div class="form-horizontal">
                           

                                <div class="control-group">
                                    <label class="control-label">
                                        Content</label>
                                    <div class="controls">
                                    <script type="text/javascript">
                                        //tinymce.PluginManager.load('moxiemanager', '/js/moxiemanager/plugin.min.js');

                                        tinymce.init({
                                            selector: "textarea",
                                            plugins: [
						"advlist autolink lists link image charmap print preview anchor",
						"searchreplace visualblocks code fullscreen",
						"insertdatetime media table contextmenu paste"
					],
                                            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | cut copy paste",
                                            contextmenu: "cut copy paste | link image inserttable",

                                            autosave_ask_before_unload: false,
                                            max_height: 200,
                                            min_height: 160,
                                            height: 250
                                        });
				</script>
                                        <textarea id="elm2" name="content" rows="13" cols="180" style="width: 100%" runat="server" ></textarea>
                                         
                                        <br /><em>Hint:  </em>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <div class="control-group pull-right">
        <label class="control-label">
        </label>
        <div class="controls">
            <asp:Button runat="server" ID="btnSubmit" CssClass="btn btn-success" ValidationGroup="SerialValidationGroup"
                Text="Create Post" OnClick="CreatePost_click"></asp:Button>
        </div>
    </div>
    </asp:View>
          <asp:View ID="vFailure" runat="server">
          <p>
                                Oops! There's been a problem in the server, please come back later</p>
                            <asp:Label ID="lblerr" runat="server" Text="Label"></asp:Label>
          </asp:View>
          <asp:View ID="vpriv" runat="server">
          <p>
            <b> <asp:Label ID="lbvlprivmsg" runat="server" Text="Label"></asp:Label> </p></b> 
                           <img src="../img/5-404-error-page-designs.jpg" alt="not verified" />
          </asp:View>
    </asp:MultiView>
</asp:Content>
