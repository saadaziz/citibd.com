﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Web.Services;

namespace citibd.User
{
    public partial class PostNews : System.Web.UI.Page
    {
        #region private properties
        private int newsID
        {
            get
            {
                if (ViewState["newsID"] == null)
                    ViewState["newsID"] = -1;
                return (int)ViewState["newsID"];
            }
            set
            {
                ViewState["newsID"] = value;
            }
        }
        
        NewsDAO ndao = new NewsDAO();
        KeywrdsDAO kdao = new KeywrdsDAO();
        #endregion
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bdmirrorDB.user userid = Utility.get_ormuserid();
                if (userid != null)
                {
                    if (userid.priviledge.ToArray()[0] != '1')
                    {
                        lbvlprivmsg.Text = "Your account is not yet verified to post on Citibd.com. Please wait";
                        mulPost.ActiveViewIndex = 2;
                    }
                    else if (userid.national_id_no == null)
                    {
                        lbvlprivmsg.Text = "Your national ID is not available on citibd.com. Please edit your profile and insert the national id no.";
                        mulPost.ActiveViewIndex = 2;
                    }
                    else
                    {
                        mulPost.ActiveViewIndex = 0;
                        Utility.BindCategoryDDL(ddlCat);
                        Utility.BindCountryDDL(ddlCountry);
                        foreach (ListItem li in ddlCountry.Items)
                        {
                            if (li.Value == "19")
                            {
                                li.Selected = true;
                                break;
                            }
                        }
                        pnlothers.Visible = true;
                        Utility.BindDivisionDDL(ddldivision, 0);

                        if (Request.QueryString["newsid"] != null)
                        {
                            bdmirrorDB.news nz = ndao.pickbyID(int.Parse(Request.QueryString["newsid"].ToString()));
                            newsID = Convert.ToInt32(nz.news_id);
                            LoadControlValue(nz);
                        }

                    }
                }
            }
        }
        protected void LoadControlValue(bdmirrorDB.news nz)
        {
            if (nz != null)
            {
                txttitle.Text = nz.title_bn;
                 ddlCountry.SelectedValue = (nz.country_id != null) ?
                    new CountryDAO().pickbyID(nz.country_id.country_id).country_id.ToString() : "-1";
                ddlDistrict.SelectedValue = (nz.district_id != null) ?
                    new DistrictDAO().pickbyID(nz.district_id.DistrictID).DistrictID.ToString() : "-1";
                ddlUpzilla.SelectedValue = (nz.upzillaID != null) ?
                   new UpzillaDAO().pickbyID(nz.upzillaID.UpazilaPSID).UpazilaPSID.ToString() : "-1";
                ddlCat.SelectedValue = (nz.category_id != null) ?
                    new CategoryDAO().pickbyID(nz.category_id.cat_id).cat_id.ToString() : "-1";

                elm2.Value = nz.contents.english_content;
            }
        }
        public string get_less(string cont)
        {

            int count = 0;
            cont = Utility.removehtml(cont);
            char[] arr = cont.ToCharArray();
            string cen = string.Empty;
            foreach (char VARIABLE in arr)
            {
                cen += VARIABLE;
                if (count > 320) break;
                count++;
            }
            return cen;
        }
        #region save
        protected void CreatePost_click(object sender, EventArgs e)
        {
            bool res = false;
            try
            {
                bdmirrorDB.news nz = new bdmirrorDB.news();
                if (newsID < 1)
                    nz = new bdmirrorDB.news();
                else
                    nz.news_id = newsID;
                bdmirrorDB.user userid = Utility.get_ormuserid();
                nz.country_id = new CountryDAO().pickbyID(int.Parse(ddlCountry.SelectedValue));
                if (pnlothers.Visible != false)
                {
                    if (ddlDistrict.SelectedValue != "")
                        nz.district_id = new DistrictDAO().pickbyID(int.Parse(ddlDistrict.SelectedValue));
                    if (ddlUpzilla.SelectedValue != "")
                        nz.upzillaID = new UpzillaDAO().pickbyID(int.Parse(ddlUpzilla.SelectedValue));
                }
                nz.title_bn = txttitle.Text;
                nz.content_bn = get_less(elm2.Value);
                nz.writer = userid;
                nz.writer_name = userid.first_name;
                nz.insert_time = DateTime.Now;
                nz.published = "0";
                nz.modified_time = DateTime.Now;
                nz.category_id = new CategoryDAO().pickbyID(Convert.ToInt64(int.Parse(ddlCat.SelectedValue)));
                nz.contents = save_content();
                if (nz.news_id <= 0)
                    nz = ndao.Add_ret(nz);
                else
                    ndao.update(nz);
                save_notification(nz);
                save_keywords(nz);
                Response.Redirect("~/user/ImageUpload.aspx?newsid="+nz.news_id);
            }
            catch (Exception ex)
            {
                pnlalert.Visible = true;
                mulPost.ActiveViewIndex = 1;
                lblerr.Text = ex.Message;
                lblerr.Text += ex.StackTrace;
            }
        }
        protected void save_notification(bdmirrorDB.news nz)
        {
            bdmirrorDB.notification noti = new bdmirrorDB.notification();
            noti.user_id = nz.writer;
            noti.notifications = "You have submitted a news " + nz.title_bn + ", Please wait for its review.";
            noti.insert_time = DateTime.Now;
            noti.status = "unread";
            new NotificationDAO().Add(noti);
        }
        protected void save_keywords(bdmirrorDB.news nz)
        {

            string[] keywords = events_container.Text.Split(',');
            foreach (string str in keywords)
            {
                if (str != "")
                {

                    bdmirrorDB.keywords keywordDB = new bdmirrorDB.keywords();
                    keywordDB.keyword = str;
                    keywordDB.news_id = nz;
                    //if (!keys.Contains(str))
                        kdao.Add(keywordDB);
                    //keys.Add(str);
                }
            }
        }
        protected bdmirrorDB.contents save_content()
        {
            bdmirrorDB.contents cont = new bdmirrorDB.contents();
            cont.english_content = elm2.Value;
            cont = new ContentDAO().Add_ret(cont);
            return cont;
        }
        #endregion
        protected void ddlChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            if (ddl.ID == "ddlCountry" && ddl.SelectedValue == "19")
            {
                pnlothers.Visible = true;
                Utility.BindDivisionDDL(ddldivision, 0);
            }
            else if (ddl.ID == "ddldivision")
            {
                Utility.BindDistrictDDL(ddlDistrict, int.Parse(ddl.SelectedValue));
            }
            else if (ddl.ID == "ddlDistrict")
            {
                Utility.BindUpazillaPSDDL(ddlUpzilla, int.Parse(ddl.SelectedValue));
            }
            else
            {
                pnlothers.Visible = false;
            }
        }
        [WebMethod]
        public static void SendForm(string keyword)
        {
            //events_container.te
        }
    }
}