﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="citibd.User.Registration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../css/bootstrap.css" rel="stylesheet" media="screen" />
    <title>Registration Citibd.com :: Your home to journalism</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link rel="icon" type="image/png" href="http://example.com/myicon.png">
</head>
<body>
    <div>
        <div class="span12">
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <a class="brand" href="#">CITIBD.COM সংবাদ প্রকাশনালয়</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="well">
            <form id="signup" class="form-horizontal" runat="server">
            <asp:ScriptManager ID="scrpt" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel runat="server" ID="updtpnl" >
                <ContentTemplate>
                   
                    <br />
                    <br />
                    <br />
                    <legend>নতুন অ্যাকাউন্ট সাইন আপ করুন</legend>
                    <asp:MultiView ID="mulReg" runat="server">
                        <asp:View ID="reg1" runat="server">
                            <table>
                                <tr>
                                    <td style="width: 50%;">
                                        <div class="control-group">
                                            <label class="control-label">
                                                নামের প্রথমাংশ</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-user"></i></span>
                                                    <asp:TextBox CssClass="input-xlarge" ID="txtfname" name="fname" placeholder="আপনার নামের প্রথমাংশ লিখুন"
                                                        runat="server" />
                                                    <asp:RequiredFieldValidator ID="rfValidator3" runat="server" ControlToValidate="txtfname"
                                                        CssClass="failureNotification" ErrorMessage="নামের প্রথমাংশ আবশ্যক !" ToolTip="First Name is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group ">
                                            <label class="control-label">
                                                নামের শেষাংশ</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-user"></i></span>
                                                    <asp:TextBox CssClass="input-xlarge" ID="txtlname" name="lname" placeholder="আপনার নামের শেষাংশ লিখুন"
                                                        runat="server" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlname"
                                                        CssClass="failureNotification" ErrorMessage="নামের শেষাংশ আবশ্যক !" ToolTip="Last Name is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group ">
                                            <label class="control-label">
                                                দেশ</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-globe"></i></span>
                                                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel runat="server" ID="pnlothers">
                                            <div class="control-group ">
                                                <label class="control-label">
                                                    বিভাগ</label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on"><i class="icon-globe"></i></span>
                                                        <asp:DropDownList ID="ddldivision" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group ">
                                                <label class="control-label">
                                                    জেলা</label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on"><i class="icon-globe"></i></span>
                                                        <asp:DropDownList ID="ddlDistrict" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group ">
                                                <label class="control-label">
                                                    থানা/উপজেলা</label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on"><i class="icon-globe"></i></span>
                                                        <asp:DropDownList ID="ddlUpzilla" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="control-group ">
                                            <label class="control-label">
                                                ফোন নাম্বার</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-tasks"></i></span>
                                                    <asp:TextBox CssClass="input-xlarge" ID="txtcontact" name="lname" placeholder="আপনার সঠিক ফোন নাম্বারটি লিখুন (+88-01xxx-xxxxxx)"
                                                        runat="server" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtcontact"
                                                        CssClass="failureNotification" ErrorMessage="ফোন নাম্বার আবশ্যক !" ToolTip="Contact no is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 50%;">
                                        <div class="control-group">
                                            <label class="control-label">
                                                ই-মেইল</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-envelope"></i></span>
                                                    <asp:TextBox CssClass="input-xlarge" ID="txtemail" name="email" placeholder="ইমেইল"
                                                        runat="server" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtemail"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SerialValidationGroup"
                                                        ErrorMessage="অনুগ্রহপূর্বক, সঠিক ইমেইল ব্যাবহার করুন !" ToolTip="Please insert a valid email"
                                                        runat="server"><img src="../img/Left_Arrow.png" alt="*" /></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">
                                                পাসওয়ার্ড</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-lock"></i></span>
                                                    <asp:TextBox TextMode="Password" ID="txtpassword" CssClass="input-xlarge" name="passwd"
                                                        placeholder="পাসওয়ার্ড" runat="server" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpassword"
                                                        CssClass="failureNotification" ErrorMessage="পাসওয়ার্ড আবশ্যক !" ToolTip="Password is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">
                                                পাসওয়ার্ড নিশ্চিত করুন</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-lock"></i></span>
                                                    <asp:TextBox TextMode="Password" ID="txtpassword2" CssClass="input-xlarge" name="conpasswd"
                                                        placeholder="আপনার দেয়া পাসওয়ার্ডটি এখানে পুনরায় লিখুন" runat="server" />
                                                    <asp:CompareValidator ID="compval" ControlToValidate="txtpassword" ControlToCompare="txtpassword2"
                                                        ErrorMessage="পাসওয়ার্ড মিলে নাই !" ToolTip="Password didnt match" runat="server"
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:CompareValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">
                                                লিঙ্গ</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-hand-right"></i></span>
                                                    <asp:DropDownList ID="ddlgender" runat="server" CssClass="input-xlarge">
                                                        <asp:ListItem>পুরুষ</asp:ListItem>
                                                        <asp:ListItem>মহিলা</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group ">
                                            <label class="control-label">
                                                ঠিকানা</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <asp:TextBox CssClass="input-xlarge" TextMode="MultiLine" ID="txtaddress" name="lname"
                                                        placeholder="আপনার পূর্ণাঙ্গ এবং সঠিক ঠিকানা লিখুন" runat="server" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtaddress"
                                                        CssClass="failureNotification" ErrorMessage="ঠিকানা আবশ্যক !" ToolTip="Address is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <p>
                                অ্যাকাউন্টের জন্য সাইন আপ করে আপনি আমাদের <a href="#" target="_blank">শর্তাবলী এবং নীতিমালা</a>
                                এর সাথে সম্মত হলেন
                            </p>
                            <p>
                             <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
                        <button class="close" data-dismiss="alert" type="button">
                            ×</button>
                        <i class="icon-exclamation-sign"></i><strong>ভুল হয়েছে !</strong>
                                <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>

                    </asp:Panel>
                            </p>
                            <div class="control-group pull-right">
                                <label class="control-label">
                                </label>
                                <div class="controls">
                                    <asp:Button runat="server" ID="btnSubmit" CssClass="btn btn-success" ValidationGroup="SerialValidationGroup"
                                        Text="রেজিস্ট্রেশান / সাইন আপ" OnClick="btnreg_click"></asp:Button>
                                </div>
                            </div>
                        </asp:View>
                        <asp:View ID="reg2" runat="server">
                            <p>
                                CONGRATULATIONS!! রেজিস্ট্রেশান/সাইন আপ সফল হয়েছে ! CITIBD.COM এর নাগরিক সাংবাদিকতার
                                ভুবনে আপনাকে স্বাগতম । এই পেজটি মাত্র
                                <pre>5 seconds...</pre>
                                এর মধ্যেই আপনাকে প্রকাশনালয় এ পৌঁছে দিবে । অনুগ্রহপূর্বক, অপেখখা করুন ।
                            </p>
                        </asp:View>
                        <asp:View ID="reg3" runat="server">
                            <p>
                                ওহহো ! আপনার ইন্টারনেট স্পীডে অথবা আমাদের সার্ভারে কিছু একটা সমস্যা হয়েছে । অনুগ্রহপূর্বক,
                                আবার চেষ্টা করুন । ধন্যবাদ ।</p>
                            <asp:Label ID="lblerr" runat="server" Text="Label"></asp:Label>
                        </asp:View>
                    </asp:MultiView>
                </ContentTemplate>
            </asp:UpdatePanel>
            </form>
        </div>
    </div>
    <!-- Javascript placed at the end of the file to make the  page load faster -->
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap-button.js"></script>
    <script language="javascript" type="text/javascript">

        function onSuccess() {
            setTimeout(okay, 5000);
        }
        function okay() {
            window.location = "http://www.citibd.com";
        }
        function onFailure() {
            setTimeout(fail, 5000);
        }
        function fail() {
            window.location = "http://www.citibd.com/user/registration.aspx";
        }
    </script>
</body>
</html>
