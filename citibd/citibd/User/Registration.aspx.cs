﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
namespace citibd.User
{
    public partial class Registration : System.Web.UI.Page
    {
        userDAO udao = new userDAO();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Utility.BindCountryDDL(ddlCountry);
                foreach (ListItem li in ddlCountry.Items)
                {
                    if (li.Value == "19")
                    {
                        li.Selected = true;
                        break;
                    }
                }
                //pnlothers.Visible = false;
                Utility.BindDivisionDDL(ddldivision, 0);  
                mulReg.ActiveViewIndex = 0;
            }
        }
        protected void ddlChanged(object sender, EventArgs e)
        { 
            DropDownList ddl = (DropDownList) sender;
            if (ddl.ID == "ddlCountry" && ddl.SelectedValue=="19")
            {
                pnlothers.Visible = true;
                Utility.BindDivisionDDL(ddldivision,0);                
            }
            else if (ddl.ID == "ddldivision")
            {
                Utility.BindDistrictDDL(ddlDistrict,int.Parse( ddl.SelectedValue));
            }
            else if (ddl.ID == "ddlDistrict")
            {
                Utility.BindUpazillaPSDDL(ddlUpzilla, int.Parse(ddl.SelectedValue));
            }
            else
            {
                pnlothers.Visible = false;
            }
        }
        #region registration
      
        protected void btnreg_click(object sender, EventArgs e)
        {
            try
            {
                if (udao.getuserbyemail(txtemail.Text)==null)
                {
                    if (txtpassword.Text.Length > 6)
                    {
                        bdmirrorDB.user user = new bdmirrorDB.user();
                        user.first_name = txtfname.Text;
                        user.last_name = txtlname.Text;
                        user.password = txtpassword.Text;
                        user.Address = txtaddress.Text;
                        user.email = txtemail.Text;
                        user.gender = (ddlgender.SelectedIndex==0)?"Male":"Female";
                        user.contact_no = txtcontact.Text;
                        user.priviledge = "10000";
                        user.profile_complete = 40;
                        user.joined_date = DateTime.Now;
                        user.Country = (ddlCountry.SelectedValue != null) ? new CountryDAO().pickbyID(int.Parse(ddlCountry.SelectedValue)) : null;
                        user.profile_pic = new ImageDAO().pickbyID(14);
                        if (pnlothers.Visible == true)
                        {
                            user.District = (ddlDistrict.SelectedValue != "") ? new DistrictDAO().pickbyID(int.Parse(ddlDistrict.SelectedValue)) : null;
                            user.Division = (ddldivision.SelectedValue != "") ? new DivisionDAO().pickbyID(int.Parse(ddldivision.SelectedValue)) : null;
                            user.Upzilla = (ddlUpzilla.SelectedValue != "") ? new UpzillaDAO().pickbyID(int.Parse(ddlUpzilla.SelectedValue)) : null;
                        }
                        udao.Add(user);
                        //Session.Add("usercitibd", (bdmirrorDB.user)user);
                        //HttpCookie cookie = new HttpCookie("UserInfo_citibd");
                        //cookie["UserName_citibd"] = txtemail.Text;

                        //cookie.Expires = DateTime.Now.AddHours(1);

                        //// query check1 = new query();
                        //// cookie["UserId"] = check1.GetUserID(uname, pass);
                        ////cookie.Expires = DateTime.Now.AddDays(1);
                        //Response.Cookies.Add(cookie);
                        //string useremail = user.email.ToString();
                        //Session.Add("UserName_citibd", (string)useremail);
                        
                        string cmd = @"Dear " + user.first_name + ", \nWelcome to our Citibd Community.\n" + " We are pleased to have you as our honorable member. \n" + "Please, read carefully and keep the below information your personal.\n  " +
                            "\n\tFull Name: " + user.first_name +" "+ user.last_name+
                            "Username :" + user.email +
                             "\t\n Password(which you can change later):" +
                             user.password +
                             "\t\n Joining Date: " +
                             user.joined_date.ToLongDateString()+
                             "\n\nN.B.: You can change your password later from the settings page after login. \n" +
                             "For any technical support, write your inquiry: support@citibd.com " +
                             "\nFor Loggin in use  email as Username." +
                             "\nBe with us, we are for changing the world." +
                            "\nThanks & Regards," +
                            "\n\t\t\t\t\t\t Citibd.com Management. ";
                        Tuple<bool, string> tup = send_email.s_email_reg(user.email, cmd);
                        //if (!(bool)tup.Item1)
                        //{
                        //    pnlalert.Visible = true;
                        //    lblmsg.Text = tup.Item2.ToString();
                        //}
                        //else
                        //{
                            ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                            mulReg.ActiveViewIndex = 1;
                        //}
                    }
                    else
                    {
                        pnlalert.Visible = true;
                        lblmsg.Text = "Password length must be 7 characters or more.";
                        //mulReg.ActiveViewIndex = 2;
                        //ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onFailure();", true);

                    }
                }
                else
                    {
                        pnlalert.Visible = true;
                        lblmsg.Text = "Email already exists.";
                        //mulReg.ActiveViewIndex = 2;
                        //ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onFailure();", true);

                    }
            }
            catch (Exception ex)
            {
                //ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                lblerr.Text = ex.Message;
                lblerr.Text += ex.StackTrace;
                mulReg.ActiveViewIndex = 2;
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onFailure();", true);

            }
        }
        #endregion
    }
}