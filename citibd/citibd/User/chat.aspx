﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="chat.aspx.cs" Inherits="citibd.User.chat" 
MasterPageFile="~/User/Admin_master.Master"%>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
    <title>Chatting : Citibd.com - Your way to journalism</title>
     <script src='https://cdn.firebase.com/v0/firebase.js'></script>
    <%--<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js'></script>--%>
    <style type="text/css">
    #messagesDiv {
    background-color: white;
    border: 8px solid #424547;
    height: 230px;
    margin-bottom: 5px;
    overflow: auto;
    padding: 10px;
    text-align: left;
    width: 70%;
}
    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="cphBody" ID="Content1">
<asp:ScriptManager runat="server" ID="scrpt"></asp:ScriptManager>
<asp:UpdatePanel runat="server" ID="updtpnl">
<ContentTemplate>
 <div id='messagesDiv'></div>
    <asp:Label runat="server" ID="nameInput" />
    <input type='text' id='messageInput' placeholder='Message'>
    <script type="text/javascript">
        var myDataRef = new Firebase('https://citibd.firebaseio.com/');
        $('#messageInput').keypress(function (e) {
            if (e.keyCode == 13) {
                var name = $('#cphBody_nameInput').val();
                var text = $('#messageInput').val();
                myDataRef.push({ name: name, text: text });
                $('#messageInput').val('');
            }
        });
        myDataRef.on('child_added', function (snapshot) {
            var message = snapshot.val();
            displayChatMessage(message.name, message.text);
        });
        function displayChatMessage(name, text) {
            $('<div/>').text(text).prepend($('<em/>').text(name + ': ')).appendTo($('#messagesDiv'));
            $('#messagesDiv')[0].scrollTop = $('#messagesDiv')[0].scrollHeight;
        };
    </script>
    </ContentTemplate></asp:UpdatePanel>
</asp:Content>

