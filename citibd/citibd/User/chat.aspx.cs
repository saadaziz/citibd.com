﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;

namespace citibd.User
{
    public partial class chat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bdmirrorDB.user user = Utility.get_ormuserid();
                if (user != null)
                    nameInput.Text = user.first_name;
            }

        }
    }
}