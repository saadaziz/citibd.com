﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="bdmirror24.User.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Login Citibd.com :: Your home to journalism</title>
      
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Dashboard Login to Citibd.com - Your home to journalism">
<meta name="author" content="">

<link rel="icon" 
      type="image/png" 
      href="http://example.com/myicon.png">
<!-- styles -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<!--[if IE 7]>
            <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
        <![endif]-->
<link href="css/styles.css" rel="stylesheet">
<link href="css/theme-wooden.css" rel="stylesheet">

<!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="css/ie/ie7.css" />
        <![endif]-->
<!--[if IE 8]>
            <link rel="stylesheet" type="text/css" href="css/ie/ie8.css" />
        <![endif]-->
<!--[if IE 9]>
            <link rel="stylesheet" type="text/css" href="css/ie/ie9.css" />
        <![endif]-->
<link href="css/aristo-ui.css" rel="stylesheet">
<link href="css/elfinder.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
<!--fav and touch icons -->
<link rel="shortcut icon" href="ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
<!--============j avascript===========-->
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>
</head>
<body>
 
   <form id="form1" runat="server">
   
   <div class="layout">
	<!-- Navbar================================================== -->
	<div class="navbar navbar-inverse top-nav">
		<div class="navbar-inner">
			<div class="container">
				<span class="home-link"><a href="../default.aspx" class="icon-home"></a></span><a class="brand" href=""><img src="http://www.citibd.com/Uploads/citibd.jpg" width="103" height="50" alt="সিটিবিডি.কম"></a>
				<div class="btn-toolbar pull-right notification-nav">
					<div class="btn-group">
						<div class="dropdown">
							<a class="btn btn-notification"><i class="icon-reply"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
            <button class="close" data-dismiss="alert" type="button">
                ×</button>
            <i class="icon-exclamation-sign"></i><strong>ভুল হয়েছে !</strong>
            <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
        </asp:Panel>
	<div class="container">
		<div class="form-signin">
			<h3 class="form-signin-heading">Please JOIN US</h3>
			<div class="controls input-icon">
				<i class=" icon-user-md"></i>
				<asp:TextBox runat="server" CssClass="input-block-level" ID="txtemail" placeholder="ই-মেইল"/>
                   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtemail"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SerialValidationGroup"
                                                        ErrorMessage="অনুগ্রহপূর্বক, সঠিক ী-মেইল এড্রেস ব্যাবহার করুন" ToolTip="Please insert a valid email"
                                                        runat="server"><img src="../img/Left_Arrow.png" alt="*" /></asp:RegularExpressionValidator>
			</div>
			<div class="controls input-icon">
				<i class=" icon-key"></i><asp:TextBox TextMode="Password" CssClass="input-block-level" placeholder="পাসওয়ার্ড" runat="server" ID="txtpassword"/>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpassword"
                                                        CssClass="failureNotification" ErrorMessage="পাসওয়ার্ড দিতে হবে !" ToolTip="পাসওয়ার্ড দিতে হবে !"
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
			</div>
			<label class="checkbox">
                <asp:CheckBox ID="CheckBox1" runat="server" Text="আমাকে লগইন রাখুন" />  </label>
			            <asp:Button ID="Button1" runat="server" CssClass="btn btn-inverse btn-block" Text="লগ ইন" ValidationGroup="SerialValidationGroup" OnClick="login_click" />

			<h4>পাসওয়ার্ড ভুলে গিয়েছেন ?</h4>
			<p>
				সেক্ষেত্রে, এখানে <a href="#">ক্লিক করুন</a>
			</p>
			<h5>আপনার কি অ্যাকাউন্ট নেই ? তাহলে</h5>
			<button class="btn btn-success btn-block" type="submit">রেজিস্ট্রেশান করুন</button>
		</div>
	</div>
</div>
   </form>
</body>
</html>
