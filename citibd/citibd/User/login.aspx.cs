﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;

namespace bdmirror24.User
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HttpCookie cookie = Request.Cookies["UserInfo_citibd"];
                if (cookie != null)
                /*if cookie exists, user is logged in, wants to logout, so kill cookie */
                {
                    //objckusername.Expires = DateTime.Now.AddDays(-1);
                    //Response.Cookies.Add(objckusername);

                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(cookie);

                    Session.RemoveAll();
                    Session.Abandon();
                }
            }

        }
        protected void login_click(object sender, EventArgs e)
        {
            bdmirrorDB.user user = new userDAO().getuserbyemail(txtemail.Text);
            if (user != null)
            {
                if (user.password.Equals(txtpassword.Text))
                {
                    //Response.Redirect("~/User/home/");
                    Session.Add("obcitibd", (bdmirrorDB.user)user);

                    //HttpCookie cookie = new HttpCookie("UserInfo_freelancer");
                    HttpCookie cookie = new HttpCookie("UserInfo_citibd");
                    cookie["UserName_citibd"] = txtemail.Text;

                    cookie.Expires = (CheckBox1.Checked) ? DateTime.Now.AddDays(5) : DateTime.Now.AddHours(5);
                    Response.Cookies.Add(cookie);
                    string useremail = user.email.ToString();
                    Session.Add("UserName_citibd", (string)useremail);
                    Response.Redirect("~/user/Dashboard.aspx");
                }
                else
                {
                    pnlalert.Visible = true;
                    lblmsg.Text = "Username or password mismatch";
                    //Response.Redirect("~/user/login.aspx");
                }

            }
            else
            {
                pnlalert.Visible = true;
                lblmsg.Text = "Email doesnt exist, please register.";
                //Response.Redirect("~/user/login.aspx");
            }
        }
    }
}