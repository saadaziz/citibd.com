﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="users.aspx.cs" Inherits="citibd.User.users" MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
  <title>User Account Management : Citibd.com - Your way to journalism</title>
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <asp:ScriptManager ID="scrpt" runat="server">
    </asp:ScriptManager>
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
        <button class="close" data-dismiss="alert" type="button">
            ×</button>
        <i class="icon-exclamation-sign"></i><strong>Message!</strong>
        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
    </asp:Panel>
        <div class="row-fluid">
        <div class="span12 switch-board gray">
            <div class="span4">
                Search by Name :
                <asp:TextBox ID="txtSearch" runat="server" Width="50%"></asp:TextBox>&nbsp;
                <asp:ImageButton ID="imgtextsearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                    ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch2_Click" />
            </div>
           
            <div class="span4 pull-right">
                Date From
                <asp:TextBox ID="txtFromDate" runat="server" Width="80px"></asp:TextBox>
                <cc1:CalendarExtender ID="ceFromDate" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate" />
                &nbsp;To
                <asp:TextBox ID="txtToDate" runat="server" Width="80px"></asp:TextBox>
                <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate" Format="dd/MM/yyyy" />
                <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/img/Search.png"
                    ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/img/Refresh.png"
                    ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
            </div>
        </div>
        <div class="span12 switch-board ">
        <%--postnews postvideos advertise publish adduser--%>
            <asp:Panel ID="pnlaccept" runat="server" Visible="true">
                <asp:Label ID="lblids" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Button ID="Buttonpostn" runat="server" Text="Post news" CssClass="btn btn-success"
                    OnClick="btnpublish_click" />
                <asp:Button ID="Buttonpostv" runat="server" Text="Post Videos" CssClass="btn btn-danger" OnClick="btnpublish_click" />
                <asp:Button ID="btnAdvertise" runat="server" Text="Advertise" CssClass="btn btn-warning" OnClick="btnpublish_click" />
                <asp:Button ID="btnpublish" runat="server" Text="Publish" CssClass="btn btn-inverse"
                    OnClick="btnpublish_click" />
                <asp:Button ID="btnuser" runat="server" Text="User Management" CssClass="btn btn-info"
                    OnClick="btnpublish_click" />
            </asp:Panel>
        </div>
    </div>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:GridView ID="grvNews" DataKeyNames="id" runat="server" Width="100%" AutoGenerateColumns="False"
                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvNews_PageIndexChanging"
                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvNews_RowDataBound"  AllowSorting="true"
                    EmptyDataText="No Records Found" CssClass="dark-table table table-dark table-striped table-sortable table-bordered"
                    AllowPaging="true" PageSize="30">
                    <Columns>
                     <asp:TemplateField  HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="1%">
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" runat="server" />
                            </ItemTemplate>
                            <HeaderTemplate>  
                                <asp:CheckBox ID="chkMainheadr"  
                                OnCheckedChanged="chkMainheadr_CheckedChanged"  
                                AutoPostBack="true" runat="server" />  
                            </HeaderTemplate>  
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="1%">
                            <ItemTemplate>
                                <asp:Label ID="lbluserid" Text='<%# Eval("id") %>' Visible="false" runat="server" HorizontalAlign="Left" />
                                <asp:Label ID="lblid" Text='<%# Eval("serial") %>' runat="server" HorizontalAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblentitle" Text='<%# (Eval("name")) %>' runat="server" HorizontalAlign="Left" NavigateUrl='<%# (Eval("link")) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="email" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lblemail" Text='<%# (Eval("email")) %>' runat="server" HorizontalAlign="Left"  />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="password" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lblpass" Text='<%# (Eval("password")) %>' runat="server" HorizontalAlign="Left"  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="contact" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lblcontact" Text='<%# (Eval("contact")) %>' runat="server" HorizontalAlign="Left"  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="date" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="3%">
                            <ItemTemplate>
                                <asp:Label ID="lbldate" Text='<%# (Eval("doj")) %>' runat="server" HorizontalAlign="Left"  />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Priviledges" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <div class="btn-group">
                                    <asp:Label ID="lblpostnews" CssClass="btn-mini btn-inverse" Visible="false" runat="server"
                                        Text="Post News" />
                                    <asp:Label ID="lblpostvideos" runat="server" Text="Post Video" Visible="false" CssClass="btn-mini btn-warning" />
                                    <asp:Label ID="lblpublish" runat="server" Text="Publish" Visible="false" CssClass="btn-mini btn-info" />
                                    <asp:Label ID="lbladvertise" runat="server" Text="Advertise" Visible="false" CssClass="btn-mini btn-danger" />
                                    <asp:Label ID="lbluser" runat="server" Text="Add User" Visible="false" CssClass="btn-mini btn-info" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>