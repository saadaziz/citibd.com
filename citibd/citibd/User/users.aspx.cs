﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;
using System.Data;
using System.Text.RegularExpressions;

namespace citibd.User
{
    public partial class users : System.Web.UI.Page
    {
        #region priv properties
        private string strSearch = string.Empty;
        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }
        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }
        NewsDAO ndao = new NewsDAO();
        userDAO udao = new userDAO();
        static DataTable dtNews = new DataTable("dtNews");
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bdmirrorDB.user user = Utility.get_ormuserid();
                if (user != null && user.priviledge.ToCharArray()[4] == '1')
                {
                    DateFrom = DateTime.Today;
                    txtFromDate.Text = DateFrom.ToString("dd/MM/yyyy");
                    DateTo = DateTime.Today;
                    txtToDate.Text = DateTo.ToString("dd/MM/yyyy");
                    initDT();
                    grvNews.DataSource = dtNews;
                    grvNews.DataBind();
                }
                else
                    Response.Redirect("Dashboard.aspx");
            }
        }
        protected void chkMainheadr_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkMainheadr = (CheckBox)grvNews.HeaderRow.FindControl("chkMainheadr");
            if (chkMainheadr.Checked == true)
            {
                foreach (GridViewRow gvOrdr in grvNews.Rows)
                {
                    CheckBox chkIndividual = (CheckBox)gvOrdr.FindControl("chk");
                    chkIndividual.Checked = true;
                }
            }
            else
            {
                foreach (GridViewRow gvOrdr in grvNews.Rows)
                {
                    CheckBox chkIndividual = (CheckBox)gvOrdr.FindControl("chk");
                    chkIndividual.Checked = false;
                }
            }
        }
        #region publish/latest/top
        protected void btnpublish_click(object sender, EventArgs e)
        {
            bdmirrorDB.notification noti = new bdmirrorDB.notification();
            foreach (GridViewRow rw in grvNews.Rows)
            {
                CheckBox chk = (CheckBox)rw.FindControl("chk");
                if (chk.Checked == true)
                {
                    bdmirrorDB.user user = udao.pickbyID(int.Parse(((Label)rw.FindControl("lbluserid")).Text));
                    //nz.published = "1";
                    switch (((Button)sender).ID)
                    {
                        case ("Buttonpostn"):
                            {
                                user.priviledge = (user.priviledge.ToCharArray()[0] == '0') ?
                            "1" + user.priviledge.Substring(1, 4) :
                            "0" + user.priviledge.Substring(1, 4);
                                break;
                            }
                        case ("Buttonpostv"):
                            {
                                user.priviledge = (user.priviledge.ToCharArray()[1] == '0') ?
                         user.priviledge.Substring(0, 1) + "1" + user.priviledge.Substring(2, 3) :
                         user.priviledge.Substring(0, 1) + "0" + user.priviledge.Substring(2, 3);
                                break;
                            }
                        case ("btnAdvertise"):
                            {
                                string pre = user.priviledge.Substring(0, 2);
                                string post = user.priviledge.Substring(3,2);
                                user.priviledge = (user.priviledge.ToCharArray()[2] == '0') ?pre + "1" + post : pre + "0" + post;
                                break;
                            }
                        case ("btnpublish"):
                            {
                                user.priviledge = (user.priviledge.ToCharArray()[3] == '0') ?
                         user.priviledge.Substring(0, 3) + "1" + user.priviledge.Substring(4, 1) :
                         user.priviledge.Substring(0, 3) + "0" + user.priviledge.Substring(4, 1);
                                break;
                            }
                        case ("btnuser"):
                            {
                                user.priviledge = (user.priviledge.ToCharArray()[4] == '0') ?
                    user.priviledge.Substring(0, 4) + "1"  :
                    user.priviledge.Substring(0, 4) + "0" ;
                                break;
                            }
                       // default:user.priviledge
                    }
                    

                    udao.update(user);
                    chk.Checked = false;
                    noti.user_id = user;
                    noti.notifications = "Congratulations, your priviledge level has changed by admin.";
                    noti.insert_time = DateTime.Now;
                    noti.status = "unread";
                    new NotificationDAO().Add(noti);
                }
            }
            initDT();
            grvNews.DataSource = dtNews;
            grvNews.DataBind();
        }
        #endregion
        #region init
        protected void initDT()
        {
            IList<bdmirrorDB.user> users = udao.All();
           
            dtNews = new DataTable("dtUser");
            dtNews.Columns.Add("id", typeof(string));
            dtNews.Columns.Add("serial", typeof(string));
            dtNews.Columns.Add("name", typeof(string));
            dtNews.Columns.Add("link", typeof(string));
            dtNews.Columns.Add("email", typeof(string));
            dtNews.Columns.Add("password", typeof(string));
            dtNews.Columns.Add("contact", typeof(string));
            dtNews.Columns.Add("doj", typeof(string));
            dtNews.Columns.Add("priv", typeof(string));
            int i = 0;
            foreach (bdmirrorDB.user usr in users.OrderByDescending(x => x.joined_date).ToList())
            {
                dtNews.Rows.Add(usr.user_id.ToString(), (++i).ToString(), usr.first_name + " " + usr.last_name, "~/profile.aspx?id=" + usr.user_id + "&name=" + usr.first_name,
                    usr.email, usr.password, usr.contact_no, usr.joined_date.ToString("dd/MM/yyyy"),
                    usr.priviledge);
            }
        }
        protected void initDT(IList<bdmirrorDB.user> users)
        {
            dtNews = new DataTable("dtUser");
            dtNews.Columns.Add("id", typeof(string));
            dtNews.Columns.Add("serial", typeof(string));
            dtNews.Columns.Add("name", typeof(string));
            dtNews.Columns.Add("link", typeof(string));
            dtNews.Columns.Add("email", typeof(string));
            dtNews.Columns.Add("password", typeof(string));
            dtNews.Columns.Add("contact", typeof(string));
            dtNews.Columns.Add("doj", typeof(string));
            dtNews.Columns.Add("priv", typeof(string));
            int i = 0;
            foreach (bdmirrorDB.user usr in users.OrderByDescending(x => x.joined_date).ToList())
            {
                dtNews.Rows.Add(usr.user_id.ToString(), (++i).ToString(), usr.first_name + " " + usr.last_name,"~/profile.aspx?id="+usr.user_id+"&name="+usr.first_name,
                    usr.email, usr.password, usr.contact_no, usr.joined_date.ToString("dd/MM/yyyy"),
                    usr.priviledge);
            }
        }
        #endregion
        #region Methods For Button
        protected void imgRefresh_Click(object sender, EventArgs e)
        {
            initDT();

            grvNews.DataSource = dtNews;
            grvNews.DataBind();
        }
        protected void imgSearch2_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text.Trim();
            initDT();

            if (strSearch != string.Empty && ((ImageButton)sender).ID == "imgtextsearch")
            {
                DataRow[] rows = dtNews.Select("name Like '%" + strSearch + "%' ");
                databind_byRow(rows);
            }
            
        }
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {

            DataRow[] rows = dtNews.Select(string.Format("doj >= '{0}' AND doj <= '{1}'", DateFrom.ToString("dd/MM/yyyy"), DateTo.ToString("dd/MM/yyyy")));
            databind_byRow(rows);

        }
        protected void databind_byRow(DataRow[] rows)
        {
            DataTable dtunew = new DataTable("dtunew");
            dtunew.Columns.Add("id", typeof(string));
            dtunew.Columns.Add("serial", typeof(string));
            dtunew.Columns.Add("name", typeof(string));
            dtunew.Columns.Add("link", typeof(string));
            dtunew.Columns.Add("email", typeof(string));
            dtunew.Columns.Add("password", typeof(string));
            dtunew.Columns.Add("contact", typeof(string));
            dtunew.Columns.Add("doj", typeof(string));
            dtunew.Columns.Add("priv", typeof(string));
            int i = 1;
            foreach (DataRow rw in rows)
            {
                dtunew.Rows.Add(rw["id"], i++.ToString(), rw["name"], rw["link"], rw["email"], rw["password"], rw["contact"], rw["doj"], rw["priv"]);
            }
            grvNews.DataSource = dtunew;
            grvNews.DataBind();
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {

            grvNews.DataBind();
        }

        #endregion
        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion
        #region Methods For Grid
        protected void grvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvNews.PageIndex = e.NewPageIndex;
            IList<bdmirrorDB.user> users = udao.All();
            users.Skip(grvNews.PageSize * (e.NewPageIndex)).Take(grvNews.PageSize);
            initDT(users);

            grvNews.DataSource = dtNews;
            grvNews.DataBind();
        }

        protected void grvNews_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[0].Controls.Count > 0))
            {
                char[] priv = (DataBinder.Eval(e.Row.DataItem, "priv")).ToString().ToCharArray();
                if (priv[0] == '1')
                    ((Label)e.Row.FindControl("lblpostnews")).Visible = true;
                if (priv[1] == '1')
                    ((Label)e.Row.FindControl("lblpostvideos")).Visible = true;
                if (priv[2] == '1')
                    ((Label)e.Row.FindControl("lbladvertise")).Visible = true;
                if (priv[3] == '1')
                    ((Label)e.Row.FindControl("lblpublish")).Visible = true;
                if (priv[4] == '1')
                    ((Label)e.Row.FindControl("lbluser")).Visible = true;
            }
        }
        #endregion
    }
}