﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.Manager;
using bdmirror24.DAL.DAL_Functions.Implementations;

namespace citibd
{
    public class Utility
    {
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static string getcaticon(Int64 catid)
        {

            switch (catid)
            {
                case 1: return "icon-flag icon-2x";
                case 2: return "icon-random icon-2x";
                case 3: return "icon-globe icon-2x";
                case 4: return "icon-baseball icon-2x";
                case 5: return "icon-group icon-2x";
                case 6: return "icon-usd icon-2x";
                case 7: return "icon-film icon-2x";
                case 8: return "icon-wifi-alt icon-2x";
                case 9: return "icon-plane icon-2x";
                case 10: return "icon-book-open icon-2x";
                case 11: return "icon-hospital icon-2x";
                //case 12: return "icon-music";
                //case 13: return "icon-music";
                default:
                    return "icon-music";
            }
                 
        }
        public static bdmirrorDB.user get_ormuserid()
        {
            //return userid;
            bdmirrorDB.user res = new bdmirrorDB.user();
            res = ((bdmirrorDB.user)HttpContext.Current.Session["obcitibd"]);
            if (res != null)
                return res;
            else
                //return new userDAO().getuserbyname(HttpContext.Current.Session["UserName_citibd"].ToString());
                //return new userDAO().pickbyID(7);
                return null;

        }
        public static string retDate(DateTime dt)
        {
            return dt.ToString("MMMM dd, yyyy");
        }
        public int get_rand(int bet)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(0, bet);
            }
        
        }
        public static void BindCategoryDDL(DropDownList ddl)
        {
            IList<bdmirrorDB.categories> cats;


            cats = new CategoryDAO().All();
                //division = new List<ADMDivision>();
          
            if (cats != null)
            {
                if (ddl.Items.Count > 2)
                    ddl.Items.Clear();
                ddl.DataSource = cats;
                ddl.DataTextField = "name_bn";
                ddl.DataValueField = "cat_id";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select Category -->", ""));
            }
        }
        public static string removehtml(string cont1)
        {
            // System.Text.RegularExpressions.Regex.Replace(cont1, "<.*?>", string.Empty);
            return System.Text.RegularExpressions.Regex.Replace(cont1, "<.*?>", "").TrimStart().TrimEnd()
                .Replace("&nbsp;", "").Replace("&lsquo;","").Replace("&rsquo;",""); 

        }
        public static void BindDivisionDDL(DropDownList ddl, int countryID)
        {
            IList<bdmirrorDB.admdivision> division;

            if (countryID > 0)
            {
                division = new DivisionManager().GetAllDivisionByCountry(countryID);
            }
            else
            {
                division = new DivisionManager().GetAllDivision();
                //division = new List<ADMDivision>();
            }

            if (division != null)
            {
                if (ddl.Items.Count > 2)
                    ddl.Items.Clear();
                ddl.DataSource = division;
                ddl.DataTextField = "divName";
                ddl.DataValueField = "division_id";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select Division -->", "-1"));
            }
        }

        public static void BindDistrictDDL(DropDownList ddl, int divisionID)
        {
            IList<bdmirrorDB.admdistrict> district;

            if (divisionID > 0)
            {
                district = new DistrictManager().GetAllDistrictByDivision(divisionID);
            }
            else
            {
                district = new DistrictManager().GetAllDistrict();
                //district = new List<ADMDistrict>();
            }

            if (district != null)
            {
                ddl.Items.Clear();
                ddl.DataSource = district;
                ddl.DataTextField = "DistrictName";
                ddl.DataValueField = "DistrictID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select District -->", "-1"));
            }
        }
        public static void BindCountryDDL(DropDownList ddl)
        {
            IList<bdmirrorDB.admcountry> country = new CountryManager().GetAllCountry();

            if (country != null)
            {
                ddl.Items.Clear();
                ddl.DataSource = country;
                ddl.DataTextField = "country_name";
                ddl.DataValueField = "country_id";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select Country -->", ""));
            }
        }
        public static void BindUpazillaPSDDL(DropDownList ddl, int districtID)
        {
            IList<bdmirrorDB.admupazilaps> upazilaPS;

            if (districtID > 0)
            {
                upazilaPS = new UpzillaManager().GetAllUpzillaByDistrict(districtID);
            }
            else
            {
                upazilaPS = new UpzillaManager().GetAllUpzilla();
                //upazilaPS = new List<ADMUpazilaPS>();
            }

            if (upazilaPS != null)
            {
                ddl.Items.Clear();
                ddl.DataSource = upazilaPS;
                ddl.DataTextField = "UpazilaPSName";
                ddl.DataValueField = "UpazilaPSID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("   <-- Select Upazila -->", "-1"));
            }
        }
    }
}