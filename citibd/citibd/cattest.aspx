﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cattest.aspx.cs" Inherits="citibd.cattest" MasterPageFile="~/main.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
 <div class="n_content clearfix">
        <div class="row-fluid">
            <div class="viewport">
                <div class="overview">
                    <div class="metro-slider">
                        <div class="metro-row">
                            <div class="metro-item half">
                                <a class="metro-image" href="post.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/8.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-music icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item-ch full">
                                <div class="metro-row2">
                                    <div class="metro-item metro-itemq box">
                                        <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                            <img src="photos/metroSlider/7.png" width="100%" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                        <div class="metro-description metro-hidden">
                                            <div class="metro-icon">
                                                <span><i class="icon-medkit icon-2x"></i></span>
                                            </div>
                                            <div class="metro-misc clearfix">
                                                <div class="metro-date n_color">
                                                    18 February 2013</div>
                                                <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, consectetur.</a>
                                                <p class="metro-text">
                                                    Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                                    sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo,
                                                    eget mollis ipsum fermentum eu. Donec posuere justo vel leo vehicula volutpat. Praesent
                                                    leo urna, ultrices in rutrum consectetur, condimentum vel mauris. Fusce quam metus,
                                                    consequat quis lacinia ut, posuere sit amet enim. Suspendisse dignissim iaculis
                                                    tempus. Pellentesque pharetra faucibus neque, ac imperdiet mi vehicula vel. Suspendisse
                                                    aliquam varius nibh, auctor iaculis velit fermentum ut.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="metro-item metro-itemq box">
                                        <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                            <img src="photos/metroSlider/2.png" width="100%" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                        <div class="metro-description metro-hidden">
                                            <div class="metro-icon">
                                                <span><i class="icon-medkit icon-2x"></i></span>
                                            </div>
                                            <div class="metro-misc clearfix">
                                                <div class="metro-date n_color">
                                                    18 February 2013</div>
                                                <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, consectetur.</a>
                                                <p class="metro-text">
                                                    Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                                    sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo,
                                                    eget mollis ipsum fermentum eu. Donec posuere justo vel leo vehicula volutpat. Praesent
                                                    leo urna, ultrices in rutrum consectetur, condimentum vel mauris. Fusce quam metus,
                                                    consequat quis lacinia ut, posuere sit amet enim. Suspendisse dignissim iaculis
                                                    tempus. Pellentesque pharetra faucibus neque, ac imperdiet mi vehicula vel. Suspendisse
                                                    aliquam varius nibh, auctor iaculis velit fermentum ut.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="metro-row2">
                                    <div class="metro-item metro-itemq box">
                                        <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                            <img src="photos/metroSlider/6.png" width="100%" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                        <div class="metro-description metro-hidden">
                                            <div class="metro-icon">
                                                <span><i class="icon-medkit icon-2x"></i></span>
                                            </div>
                                            <div class="metro-misc clearfix">
                                                <div class="metro-date n_color">
                                                    18 February 2013</div>
                                                <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, consectetur.</a>
                                                <p class="metro-text">
                                                    Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                                    sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo,
                                                    eget mollis ipsum fermentum eu. Donec posuere justo vel leo vehicula volutpat. Praesent
                                                    leo urna, ultrices in rutrum consectetur, condimentum vel mauris. Fusce quam metus,
                                                    consequat quis lacinia ut, posuere sit amet enim. Suspendisse dignissim iaculis
                                                    tempus. Pellentesque pharetra faucibus neque, ac imperdiet mi vehicula vel. Suspendisse
                                                    aliquam varius nibh, auctor iaculis velit fermentum ut.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="metro-item metro-itemq box">
                                        <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                            <img src="photos/metroSlider/3.png" width="100%" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                        <div class="metro-description metro-hidden">
                                            <div class="metro-icon">
                                                <span><i class="icon-medkit icon-2x"></i></span>
                                            </div>
                                            <div class="metro-misc clearfix">
                                                <div class="metro-date n_color">
                                                    18 February 2013</div>
                                                <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, consectetur.</a>
                                                <p class="metro-text">
                                                    Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                                    sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo,
                                                    eget mollis ipsum fermentum eu. Donec posuere justo vel leo vehicula volutpat. Praesent
                                                    leo urna, ultrices in rutrum consectetur, condimentum vel mauris. Fusce quam metus,
                                                    consequat quis lacinia ut, posuere sit amet enim. Suspendisse dignissim iaculis
                                                    tempus. Pellentesque pharetra faucibus neque, ac imperdiet mi vehicula vel. Suspendisse
                                                    aliquam varius nibh, auctor iaculis velit fermentum ut.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item half">
                                <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/2.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-fire icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="metro-row">
                            <div class="metro-item quarter">
                                <a class="metro-image" href="post.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/5.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-group icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item quarter">
                                <a class="metro-image" href="post.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/4.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-group icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item full">
                                <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.">
                                    <img src="photos/metroSlider/3.png" width="100%" alt="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor."></a>
                                <div class="metro-description">
                                    <div class="metro-icon">
                                        <span><i class="icon-magic icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, consectetur
                                            adipisicing elit, sed do eiusmod tempor.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo,
                                            eget mollis ipsum fermentum eu. Donec posuere justo vel leo vehicula volutpat. Praesent
                                            leo urna, ultrices in rutrum consectetur, condimentum vel mauris. Fusce quam metus,
                                            consequat quis lacinia ut, posuere sit amet enim. Suspendisse dignissim iaculis
                                            tempus. Pellentesque pharetra faucibus neque, ac imperdiet mi vehicula vel. Suspendisse
                                            aliquam varius nibh, auctor iaculis velit fermentum ut.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item half">
                                <a class="metro-image" href="post.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/6.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-umbrella icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                          <div class="metro-row">
                            <div class="metro-item half">
                                <a class="metro-image" href="post.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/8.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-music icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="metro-item half">
                                <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/2.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-fire icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item half">
                                <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/2.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-fire icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item half">
                                <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/2.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-fire icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                          <div class="metro-row">
                            <div class="metro-item half">
                                <a class="metro-image" href="post.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/8.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-music icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="metro-item quarter">
                                <a class="metro-image" href="post.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/5.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-group icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item quarter">
                                <a class="metro-image" href="post.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/4.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-group icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item half">
                                <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/2.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-fire icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="metro-item half">
                                <a class="metro-image" href="post_2.html" title="Lorem ipsum dolor sit amet, consectetur.">
                                    <img src="photos/metroSlider/2.png" alt="Lorem ipsum dolor sit amet, consectetur."></a>
                                <div class="metro-description metro-hidden">
                                    <div class="metro-icon">
                                        <span><i class="icon-fire icon-2x"></i></span>
                                    </div>
                                    <div class="metro-misc clearfix">
                                        <div class="metro-date n_color">
                                            20 January 2013</div>
                                        <a class="metro-title" href="post_2.html" title="">Lorem ipsum dolor sit amet, cosectetur.</a>
                                        <p class="metro-text">
                                            Ut pellentesque rutrum fermentum. Sed sit amet libero a enim dapibus pharetra quis
                                            sit amet dui. Ut consequat condimentum nunc et dignissim. Duis iaculis iaculis justo.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            
</asp:Content>