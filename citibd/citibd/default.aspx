﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="citibd._default"
    MasterPageFile="~/main.Master" %>

<asp:Content ID="contHead" runat="server" ContentPlaceHolderID="head">
<title>CitiBD.com .::. Your home to civil journalism</title>

  <script type="text/javascript">
      $(document).ready(function () {

          function lastPostFunc() {
              //  alert("asda");
              $('#divPostsLoader').html('<img src="img/bigLoader.gif">');
              // var data = { category: $("#content_hdnval").val() };
              //send a query to server side to present new content
              $.ajax({
                  type: "POST",
                  url: "default.aspx/getimages",
                  data: "{}",
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function (data) {

                      if (data != "") {
                          $('#pnlgallery').append(data.d);
                      }
                      $('#divPostsLoader').empty();
                  },
                  error: function () {
                      alert('Fail Test Get Dynamic');
                  }

              })
          };

          function getjatio(incoming, pnljatio) {

              $('.postloading').html('<img src="img/bigLoader.gif">');
              var data = incoming;
              //send a query to server side to present new content
              $.ajax({
                  type: "POST",
                  url: "default.aspx/getnews",
                  data: JSON.stringify(data),
                  contentType: "application/json;charset=utf-8",
                  dataType: "json",
                  success: function (data) {
                      if (data != "") {
                          $(pnljatio).append(data.d);
                      }
                      $('.postloading').empty();
                  },
                  error: function () {
                      //alert('Fail Test Get Dynamic');
                  }

              });
          };
          function getmorejatio(incoming, pnljatio) {
             
              $('.postloading').html('<img src="img/bigLoader.gif">');
              var data = incoming;
              //send a query to server side to present new content
              $.ajax({
                  type: "POST",
                  url: "default.aspx/getmorenews",
                  data: JSON.stringify(data),
                  contentType: "application/json;charset=utf-8",
                  dataType: "json",
                  success: function (data) {
                      if (data != "") {
                          $(pnljatio).append(data.d);
                      }
                      $('.postloading').empty();
                  },
                  error: function () {
                      alert('Fail Test Get Dynamic');
                  }

              });
          };
          //$(window).scroll(function () {
          //if ($(window).scrollTop() == $(tilesfirst).height()) {
          //alert($(window).scrollTop());
          var pnljatio = $('#pnljatio');
          getjatio({ catid: 1 }, pnljatio);
          pnljatio = $('#pnlrajniti');
          getjatio({ catid: 2 }, pnljatio);
          pnljatio = $('#pnlinternational');
          getjatio({ catid: 3 }, pnljatio);
          pnljatio = $('#pnlsports')
          getjatio({ catid: 4 }, pnljatio);
          pnljatio = $('#pnltrade');
          getjatio({ catid: 6 }, pnljatio);
          lastPostFunc();
          pnljatio = $('#pnlentertainment');
          getjatio({ catid: 7 }, pnljatio);
          pnljatio = $('#pnltravel');
          getjatio({ catid: 9 }, pnljatio);
          //          $('window').scroll(function () {
          //              //if ($('#tilesfirst').height() == $('#tilesfirst').scrollTop()) {
          //              alert($('#tilesfirst').height());
          //              var pnljatio = $('#pnlentertainment')
          //              getjatio({ catid: 7 }, pnljatio);
          //              pnljatio = $('#pnltravel')
          //              getjatio({ catid: 9 }, pnljatio);

          //              // }
          //          });

          //          function isScrollBottom() {
          //              var elementHeight = $('#tilesfirst').height();
          //              var scrollPosition = $('#tilesfirst').height() + $('#tilesfirst').scrollTop();
          //              return (elementHeight == scrollPosition);
          //          };
          //          var no = 2000;
          //          $(window).scroll(function () {
          //              if ($(window).scrollTop() == $no) {
          //                  alert($(window).scrollTop());
          //                  lastPostFunc();
          //                  //metro_slider();
          //              }
          //          });
          $("#pnljationext").click(function () {
              var pnljatio1 = $('#pnljatio');
              getmorejatio({ catid: 1 }, pnljatio1);
          });
          $("#pnlrajnitinext").click(function () {
              var pnljatio2 = $('#pnlrajniti');
              getmorejatio({ catid: 2 }, pnljatio2);
          });
          $("#pnlinternationalnext").click(function () {
              var pnljatio3 = $('#pnlinternational');
              getmorejatio({ catid: 3 }, pnljatio3);
          });
          $("#pnlsportsnext").click(function () {
              var pnljatio4 = $('#pnlsports');
              getmorejatio({ catid: 4 }, pnljatio4);
          });
          $("#pnltravelnext").click(function () {
              var pnljatio5 = $('#pnltravel');
              getmorejatio({ catid: 9 }, pnljatio5);
          }); 
          $("#pnlentertainmentnext").click(function () {
              var pnljatio6 = $('#pnlentertainment');
              getmorejatio({ catid: 7 }, pnljatio6);
          });
          $("#pnltradenext").click(function () {
              var pnljatio6 = $('#pnltrade');
              getmorejatio({ catid: 6 }, pnljatio6);
          });
      });
      
  </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">

    <div class="n_content clearfix">
        <div id="tilesfirst" class="row-fluid">
            <div class="span12">
                <div class="viewport">
                    <div class="overview">
                        <div class="metro-slider" id="pnlslider" style="width: 100%">
                            <asp:Repeater ID="rptrow" runat="server">
                                <ItemTemplate>
                                    <asp:Panel ID="pnlrow" runat="server" class="metro-row" Style="width: 100%">
                                        <%# Eval("text") %>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">

		<div class="span8" id="sliders">
			<div class="row-fluid carousel_block">
				<div class="span12">
					<div class="n_news_cat_list clearfix">
   
						<h4 class="pull-left n_news_cat_list_title">জাতীয়<small> খবরাখবর </small></h4>
						<div class="met_carousel_control clearfix">
							<a href="#"><i class="icon-angle-left"></i></a>
							<a href="#" id="pnljationext"><i class="icon-angle-right" ></i></a>
						</div>
						<div class="n_splitter"><span class="n_bgcolor"></span></div>
						<div class="met_carousel_wrap">
							<div id="pnljatio" class="met_carousel clearfix ">
						     <div class="postloading">
                             </div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <!-- Carousel with News Items Ends -->
            <div class="row-fluid carousel_block">
				<div class="span12">
					<div class="n_news_cat_list clearfix">
   
						<h4 class="pull-left n_news_cat_list_title">রাজনীতি<small> 'র সর্বশেষ খবর </small></h4>
						<div class="met_carousel_control clearfix">
							<a href="#"><i class="icon-angle-left"></i></a>
							<a href="#" id="pnlrajnitinext"><i class="icon-angle-right"></i></a>
						</div>
						<div class="n_splitter"><span class="n_bgcolor"></span></div>
						<div class="met_carousel_wrap">
							<div id="pnlrajniti" class="met_carousel clearfix">
						     <div class="postloading">
                             </div>
							</div>
						</div>
					</div>
				</div>
			</div>
            
            <div class="row-fluid carousel_block">
				<div class="span12">
					<div class="n_news_cat_list clearfix">
   
						<h4 class="pull-left n_news_cat_list_title">আন্তর্জাতিক<small> বিভিন্ন সংবাদ </small></h4>
						<div class="met_carousel_control clearfix">
							<a href="#"><i class="icon-angle-left"></i></a>
							<a href="#" id="pnlinternationalnext" ><i class="icon-angle-right"></i></a>
						</div>
						<div class="n_splitter"><span class="n_bgcolor"></span></div>
						<div class="met_carousel_wrap">
							<div id="pnlinternational" class="met_carousel clearfix">
						     <div class="postloading">
                             </div>
							</div>
						</div>
					</div>
				</div>
			</div>
            
            <div class="row-fluid carousel_block">
				<div class="span12">
					<div class="n_news_cat_list clearfix">
   
						<h4 class="pull-left n_news_cat_list_title">খেলাধুলা<small> 'র সর্বশেষ সংবাদ</small></h4>
						<div class="met_carousel_control clearfix">
							<a href="#"><i class="icon-angle-left"></i></a>
							<a href="#" id="pnlsportsnext"><i class="icon-angle-right"></i></a>
						</div>
						<div class="n_splitter"><span class="n_bgcolor"></span></div>
						<div class="met_carousel_wrap">
							<div id="pnlsports" class="met_carousel clearfix">
						     <div class="postloading">
                             </div>
							</div>
						</div>
					</div>
				</div>
			</div>
             <div class="row-fluid carousel_block">
				<div class="span12">
					<div class="n_news_cat_list clearfix">
   
						<h4 class="pull-left n_news_cat_list_title">বাণিজ্য<small> 'র সর্বশেষ সংবাদ</small></h4>
						<div class="met_carousel_control clearfix">
							<a href="#"><i class="icon-angle-left"></i></a>
							<a href="#" id="pnltradenext"><i class="icon-angle-right"></i></a>
						</div>
						<div class="n_splitter"><span class="n_bgcolor"></span></div>
						<div class="met_carousel_wrap">
							<div id="pnltrade" class="met_carousel clearfix">
						     <div class="postloading">
                             </div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row-fluid carousel_block">
				<div class="span12">
					<div class="n_news_cat_list clearfix">
   
						<h4 class="pull-left n_news_cat_list_title">বিনোদন<small> 'র সংবাদ</small></h4>
						<div class="met_carousel_control clearfix">
							<a href="#"><i class="icon-angle-left"></i></a>
							<a href="#" id="pnlentertainmentnext"><i class="icon-angle-right"></i></a>
						</div>
						<div class="n_splitter"><span class="n_bgcolor"></span></div>
						<div class="met_carousel_wrap">
							<div id="pnlentertainment" class="met_carousel clearfix">
						     <div class="postloading">
                             </div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row-fluid carousel_block">
				<div class="span12">
					<div class="n_news_cat_list clearfix">
   
						<h4 class="pull-left n_news_cat_list_title">ভ্রমণ<small> এর কাহিনী </small></h4>
						<div class="met_carousel_control clearfix">
							<a href="#"><i class="icon-angle-left"></i></a>
							<a href="#" id="pnltravelnext"><i class="icon-angle-right"></i></a>
						</div>
						<div class="n_splitter"><span class="n_bgcolor"></span></div>
						<div class="met_carousel_wrap">
							<div id="pnltravel" class="met_carousel clearfix">
						     <div class="postloading">
                             </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12"><a href="#"><img class="n_met_ad" src="photos/ADS/1.gif" alt=""/></a></div>
			</div>
            <%--photogallery--%>
            
			<div class="row-fluid">
				<div class="span12">
					<h4 class="pull-left n_news_cat_list_title">ছবির হাট<small>  photos by you :)</small></h4>
					<div class="met_carousel_control clearfix">
						<a href="#"><i class="icon-angle-left"></i></a>
						<a href="#"><i class="icon-angle-right"></i></a>
					</div>
					<div class="n_splitter"><span class="n_bgcolor"></span></div>
					<div class="n_gallery_carousel_wrap">
						<div class="n_gallery_carousel" id="pnlgallery">
							
                             <div id="divPostsLoader">
                             </div>
						</div>
					</div>
				</div>
			</div><!-- Carousel with News Items Ends -->
		</div><!-- Big Content Area -->
         <%--spotlight--%>
       <div class="span4" id="sptlight">
      <div class="row-fluid n_text_align_center" >
				<div class="span12 text-center centering-saad" >
					<h4 class="n_news_cat_list_title">বিশেষ প্রতিবেদন</h4>
					<a href="#">
                        <asp:Image ID="imgspotlight" runat="server" ImageAlign="AbsMiddle" /></a>
					<a href="#" class="n_title_link"><h5 class="n_little_title">
                        <asp:HyperLink ID="hplspottitle" runat="server" Text=""></asp:HyperLink></h5></a>
					<span class="n_little_date"><asp:HyperLink ID="hplspotDate" runat="server" Text=""></asp:HyperLink></span>
					<p class="n_short_descr n_without_margin"><asp:Label ID="lblspotContent" runat="server" Text=""></asp:Label></p>
				</div>
			</div>
            <div class="n_splitter_2"><span class="n_bgcolor"></span></div>
            </div>
		<div class="span2">

			<div class="row-fluid">
				<div class="span12">
					<h4 class="n_news_cat_list_title">সর্বশেষ সংবাদ</h4>
                    <asp:Repeater ID="rptlatest" runat="server">
                    <ItemTemplate>
                    <div class="n_latest_post_container clearfix latest-post text-center">

						<a href='<%# Eval("newslink") %>' class="n_latest_post_picture"><img src='<%# Eval("imagelink") %>' height="80px" width="80px" alt='<%# Eval("newstitle") %>' /></a>
						<div class="n_latest_post_details">
							<a href='<%# Eval("newslink") %>' class="n_title_link"><h5 class="n_little_title"><%# Eval("newstitle") %></h5></a>
							<span class="n_little_date"><%# Eval("newsdate") %></span>
						</div>
					</div>
					<div class="n_splitter_2"><span class="n_bgcolor"></span></div>
                    </ItemTemplate>
                    </asp:Repeater>

				</div>
			</div><!-- Latest News Ends -->

			<div class="row-fluid n_hide">
				<div class="span12">
					<a href="#" target="_blank"><img src="http://www.citibd.com/img/footerad.png" alt="Advertise Here 2"/></a>
				</div>
			</div><!-- AD Block Ends -->

		</div><!-- Mid Side Bar Area -->

		<div class="span2">
        <div class="row-fluid n_small_block n_text_align_center">
				<div class="span12">
					<a href="#" target="_blank"><img src="http://www.citibd.com/img/footerad.png" alt="Advertise Here 1"/></a>
				</div>
			</div>

			<div class="row-fluid n_small_block n_text_align_center">
				<div class="span12">
					<h4 class="n_news_cat_list_title">সর্বাধিক পঠিত সংবাদ</h4>
					
				</div>
			</div>

			 <asp:Repeater ID="rptmostread" runat="server">
                    <ItemTemplate>
                      <div class="row-fluid n_small_block n_text_align_center n_hide">
                <div class="span12">
                    <div class="pull-left text-center">
                        <a href='<%# Eval("newslink") %>'><img src='<%# Eval("imagelink") %>' width="170px"  height="170px" alt='<%# Eval("newstitle") %>'/></a>
                        <a href='<%# Eval("newslink") %>' class="n_title_link"><h5 class="n_little_title"><%# Eval("newstitle") %></h5></a>
                        <span class="n_little_date"><%# Eval("newsdate") %></span>
                        <p class="n_short_descr">'<%# Eval("newscontent") %>'</p>
                    </div>

                </div>
            </div>

					<div class="n_splitter_2"><span class="n_bgcolor"></span></div>
                    </ItemTemplate>
                    </asp:Repeater>
		

		</div><!-- Right Side Bar Area -->

	</div>

	<div class="n_splitter"><span class="n_bgcolor"></span></div>

	<div class="row-fluid">
		<div class="span12">
        <p>PARTNERS</p>
            <a href="http://www.bdmirror24.com/" target="_blank"><img src="http://www.citibd.com/img/footerlogo1.png" alt="Advertisement"/></a>
            <a href="http://www.bdmirror24.com/" target="_blank"><img src="http://www.citibd.com/img/footerlogo2.png" alt=""/></a>
            <a href="http://www.bdmirror24.com/" target="_blank"><img src="http://www.citibd.com/img/footerlogo3.png" alt=""/></a>
            <a href="http://www.bdmirror24.com/" target="_blank"><img src="http://www.citibd.com/img/footerlogo4.png" alt=""/></a>
            <a href="http://www.bdmirror24.com/" target="_blank"><img src="http://www.citibd.com/img/footerlogo5.png" alt=""/></a>
            </div>
	</div>

</div><!-- Middle Content Ends -->

</asp:Content>