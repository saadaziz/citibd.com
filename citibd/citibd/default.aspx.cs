﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Data;
using bdmirror24.DAL.mappings;
using System.Web.Services;
namespace citibd
{
    public partial class _default : System.Web.UI.Page
    {
        #region private properties
        getImages getimg = new getImages();
        NewsDAO ndao = new NewsDAO();
        ImageDAO imgdao = new ImageDAO();
        static int jatio = 0;
        static int catids = 0;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable dtnz = getimg.loadrows();
                rptrow.DataSource = dtnz;
                rptrow.DataBind();
                set_spotlight();
                loadlatest();
                loadpopular();
            }
        }
        #region load views
        protected void set_spotlight()
        {
            bdmirrorDB.news getspotlight = ndao.getspotlight();
            bdmirrorDB.images img = imgdao.GetImagebyNewsID(getspotlight);
            hplspottitle.Text = getspotlight.title_bn;
            hplspottitle.NavigateUrl ="NewsDetails.aspx?newsid="+getspotlight.news_id+"&title="+Server.UrlEncode( getspotlight.title_bn);
            hplspotDate.Text = Utility.retDate(getspotlight.insert_time);
            hplspotDate.NavigateUrl ="Search.aspx?date="+ Utility.retDate(getspotlight.insert_time);
            lblspotContent.Text = getspotlight.content_bn;
            imgspotlight.ImageUrl = img.image_watermarked;
            imgspotlight.AlternateText = getspotlight.title_bn;
        }
        protected void loadlatest()
        {
            DataTable dtlatest = new DataTable("dtlatest");
            dtlatest.Columns.Add("newslink", typeof(string));
            dtlatest.Columns.Add("imagelink", typeof(string));
            dtlatest.Columns.Add("newstitle", typeof(string));
            dtlatest.Columns.Add("newsdate", typeof(string));
            IList<bdmirrorDB.news> latestnews = ndao.GetLatestNews();
            foreach (bdmirrorDB.news nz in latestnews)
            {
                bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
                if (img.news_id == null)
                    img = imgdao.pickbyID(233);
                dtlatest.Rows.Add("/NewsDetails.aspx?newsid=" + nz.news_id + "&title=" + Server.UrlEncode(nz.title_bn), img.image_watermarked, nz.title_bn, Utility.retDate(nz.insert_time));
            }
            rptlatest.DataSource = dtlatest;
            rptlatest.DataBind();
        }
        protected void loadpopular()
        {
            DataTable dtlatest = new DataTable("dtpopular");
            dtlatest.Columns.Add("newslink", typeof(string));
            dtlatest.Columns.Add("imagelink", typeof(string));
            dtlatest.Columns.Add("newstitle", typeof(string));
            dtlatest.Columns.Add("newsdate", typeof(string));
            dtlatest.Columns.Add("newscontent", typeof(string));
            IList<bdmirrorDB.news> latestnews = ndao.getMostread();
            foreach (bdmirrorDB.news nz in latestnews)
            {

                bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
                if (img.news_id == null)
                    img = imgdao.pickbyID(233);
                dtlatest.Rows.Add("/NewsDetails.aspx?newsid=" + nz.news_id + "&title=" +Server.UrlEncode( nz.title_bn), img.image_watermarked, nz.title_bn, Utility.retDate(nz.insert_time),nz.content_bn);
            }
            rptmostread.DataSource = dtlatest;
            rptmostread.DataBind();
        }
        #endregion
        #region webmethod
        [WebMethod]
        public static string getnews(string catid)
        {
            bdmirrorDB.categories cat = new CategoryDAO().pickbyID(int.Parse(catid));
            IList<bdmirrorDB.news> newz = new NewsDAO().getAllNewsbyCatName(cat).Take(4).ToList();
            string html = "";
            ImageDAO imgdao = new ImageDAO();
            foreach (bdmirrorDB.news nz in newz)
            {
                bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
                if (img.news_id == null)
                    img = imgdao.pickbyID(233);
                html += @"<div class=""metro-item half"" style=""width:100%"">
									<a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="+(nz.title_bn) + @">
                                            <img src=" + img.image_watermarked + @" width=""100%"" ></a>

										<div class=""metro-description metro-hidden"">
											<div class=""metro-icon""><span><i class="+Utility.getcaticon(int.Parse(catid))+@"></i></span></div>
											<div class=""metro-misc clearfix"">
												<div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
												   <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
											</div>
										</div>
									</div>";
            }
            return html;
        }
        [WebMethod]
        
        public static string getmorenews(string catid)
        {
            if (int.Parse(catid) != catids)
            {
                catids = int.Parse(catid);
                jatio = 0;
            }
            jatio += 4;
            bdmirrorDB.categories cat = new CategoryDAO().pickbyID(int.Parse(catid));
            IList<bdmirrorDB.news> newz = new NewsDAO().getAllNewsbyCatName(cat).Skip(jatio).Take(4).ToList();
            string html = "";
            ImageDAO imgdao = new ImageDAO();
            foreach (bdmirrorDB.news nz in newz)
            {
                bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
                if (img.news_id == null)
                    img = imgdao.pickbyID(233);
                html += @"<div class=""metro-item half"" style=""width:100%"">
									<a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + (nz.title_bn) + @">
                                            <img src=" + img.image_watermarked + @" width=""100%"" ></a>

										<div class=""metro-description metro-hidden"">
											<div class=""metro-icon""><span><i class=" + Utility.getcaticon(int.Parse(catid)) + @"></i></span></div>
											<div class=""metro-misc clearfix"">
												<div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
												   <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
											</div>
										</div>
									</div>";
            }
            return html;
        }
        [WebMethod]
        public static string getimages()
        {
            ImageDAO imgdao = new ImageDAO();
            string html="";
            IList<bdmirrorDB.images> images = imgdao.getNewsImage();
            List<string> divs = new List<string>();
            List<long> ids = new List<long>();
            foreach(bdmirrorDB.images img in images)
            {
                
                bdmirrorDB.news inz = img.news_id;
                if (ids.Where(x => x == inz.news_id).ToList().Count != 0) continue;
                ids.Add(inz.news_id);
                divs.Add(@"<div><a href=NewsDetails.aspx?newsid=" + inz.news_id + @" title=" + (inz.title_bn) + @"><img src=" + img.image_watermarked + @" alt=" + img.caption_en + @" height=""250px"" width=""250px"" /></a>
									<a href=NewsDetails.aspx?newsid=" + inz.news_id + @" title=" + (inz.title_bn) + @"><span><i class=""icon-picture icon-2x""></i></span></a>
								</div>");
            }
            int i = 1;
            while(true)
            {
                html += @"<div class=""n_gallery_carousel_col"">" + divs[i] + divs[i + 2] + divs[i + 3] + divs[i + 4] + divs[i + 5] + divs[i + 6] + "</div>";
                i += 7;
                if (i > 60)
                    break;
            }
            return html;
        }
        #endregion
    }
}