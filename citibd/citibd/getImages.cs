﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Data;

namespace citibd
{
    public class getImages
    {
        #region private properties
        IList<bdmirrorDB.images> imagesall = new List<bdmirrorDB.images>();
        static Utility uit = new Utility();
        IList<bdmirrorDB.news> alreadyinserted = new List<bdmirrorDB.news>();
        IList<bdmirrorDB.images> alreadyinsertedimg = new List<bdmirrorDB.images>();        
        IList<bdmirrorDB.news> newz = new List<bdmirrorDB.news>();
        Dictionary<bdmirrorDB.news, bdmirrorDB.images> dictimages = new Dictionary<bdmirrorDB.news, bdmirrorDB.images>();
        ImageDAO imgdao = new ImageDAO();
        NewsDAO ndao = new NewsDAO();
        #endregion
        #region loadrows
        public DataTable loadrows(bdmirrorDB.categories cat, int count)
        {
            DataTable dtnews = new DataTable("dtnz");
            newz = ndao.getAllNewsbyCatName(cat);
            int cnt = newz.Count;
            newz = (count<20)? newz.Take((count + 1) * cnt).Skip(count * cnt).ToList():newz;
            
            var concreteList = new List<bdmirrorDB.news>(newz);
            concreteList.Reverse();
            newz = concreteList;
            foreach (bdmirrorDB.news nz in newz)
            {
                bdmirrorDB.images images = imgdao.GetImagebyNewsID(nz);
                if(images.news_id==null)
                    images = imgdao.pickbyID(233);
                imagesall.Add(images);
                dictimages.Add(nz, images);
            }
            dtnews.Columns.Add("text", typeof(string));

            for (int i = 0; i < 4; i++)
            {
                int tot = 0;
                string html = "";
                while (true)
                {
                    int rand = uit.get_rand(4);
                    if (tot < 2 && rand == 2)
                    {
                        int randf = uit.get_rand(4);
                        if (randf == 2)
                            html += get_boxs();
                        else
                            html += get_full();
                        tot += 2;
                    }
                    else
                    {
                        int randh = uit.get_rand(4);
                        if (randh == 2)
                            html += get_quarters();
                        else
                            html += get_half();
                        tot += 1;
                    }
                    if (tot == 4) break;
                }
                dtnews.Rows.Add(html);
            }
            return dtnews;
        }
        public DataTable loadrows()
        {
            //imagesall = new List<bdmirrorDB.images>();
            DataTable dtnews = new DataTable("dtnz");
            newz = ndao.GetLatestNews("top_slider");
            foreach (bdmirrorDB.news nz in newz)
            {
                bdmirrorDB.images images = imgdao.GetImagebyNewsID(nz);
               // foreach (bdmirrorDB.images img in images)

                imagesall.Add(images);
                dictimages.Add(nz, images);
            }
            dtnews.Columns.Add("text", typeof(string));

            for (int i = 0; i < 3; i++)
            {
                int tot = 0;
                string html = "";
                while (true)
                {
                    int rand = uit.get_rand(3);
                    if (tot < 2 && rand == 2)
                    {
                        int randf = uit.get_rand(6);
                        if (randf == 2)
                            html += get_boxs();
                        else
                            html += get_full();
                        tot += 2;
                    }
                    else
                    {
                        int randh = uit.get_rand(5);
                        if (randh == 2)
                            html += get_quarters();
                        else
                            html += get_half();
                        tot += 1;
                    }
                    if (tot == 4) break;
                }
                dtnews.Rows.Add(html);
            }
            return dtnews;
        }
        public DataTable loadrows(IList<bdmirrorDB.news> newwslist)
        {
            //imagesall = new List<bdmirrorDB.images>();
            DataTable dtnews = new DataTable("dtnz");
            newz = newwslist;
            foreach (bdmirrorDB.news nz in newz)
            {
                bdmirrorDB.images images = imgdao.GetImagebyNewsID(nz);
                if (images.news_id != null)
                    imagesall.Add(images);
                else
                    images = imgdao.pickbyID(233);
                dictimages.Add(nz, images);
            }
            dtnews.Columns.Add("text", typeof(string));

            for (int i = 0; i < 3; i++)
            {
                int tot = 0;
                string html = "";
                while (true)
                {
                    int rand = uit.get_rand(4);
                    if (tot < 2 && rand == 2)
                    {
                        int randf = uit.get_rand(4);
                        if (randf == 2)
                            html += get_boxs();
                        else
                            html += get_full();
                        tot += 2;
                    }
                    else
                    {
                        int randh = uit.get_rand(4);
                        if (randh == 2)
                            html += get_quarters();
                        else
                            html += get_half();
                        tot += 1;
                    }
                    if (tot == 4) break;
                }
                dtnews.Rows.Add(html);
            }
            return dtnews;
        }
        public string loadrowslate(bdmirrorDB.categories cat, int count)
        {
            string res = "";
            IList<bdmirrorDB.images> oldnews = imagesall.ToList();
            newz = ndao.getAllNewsbyCatName(cat).Take((count + 1) * 6).Skip(count * 6).ToList();

            foreach (bdmirrorDB.news nz in newz)
            {
                IList<bdmirrorDB.images> images = imgdao.GetImagesbyNewsID(nz);
                foreach (bdmirrorDB.images img in images)
                    imagesall.Add(img);
            }
            for (int i = 0; i < 1; i++)
            {
                int tot = 0;
                string html = "";
                while (true)
                {
                    int rand = uit.get_rand(4);
                    if (tot < 2 && rand == 2)
                    {
                        int randf = uit.get_rand(3);
                        if (randf == 2)
                            html += get_boxs();
                        else
                            html += get_full();
                        tot += 2;
                    }
                    else
                    {
                        int randh = uit.get_rand(7);
                        if (randh == 2)
                            html += get_quarters();
                        else
                            html += get_half();
                        tot += 1;
                    }
                    if (tot == 4) break;
                }
                res += (html);
            }
            return res;
        }
        #endregion

        #region htmls
        public  Tuple<bdmirrorDB.news, bdmirrorDB.images> get_news(int no)
        {
            bdmirrorDB.news nz = new bdmirrorDB.news();
            bdmirrorDB.images img = new bdmirrorDB.images();
            ImageDAO imgdao = new ImageDAO();
            //NewsDAO ndao = ndao;
            List<bdmirrorDB.news> inhere = new List<bdmirrorDB.news>();

            try
            {
                if (no == 1)//full
                {
                    img = imagesall.Where(x => x.width > 600).FirstOrDefault();
                }
                else if (no == 3)//quarter
                {
                    img = imagesall.Where(x => x.width < 450).FirstOrDefault();
                }
                else if (no == 2)//half
                    img = imagesall.Where(x => x.width > 250).FirstOrDefault();
                else
                    img = imagesall.Where(x => x.height < 300).FirstOrDefault();

                if (img == null)
                    img = imagesall.ElementAt(uit.get_rand(imagesall.Count));
                if (img.image_id!=233)
                    nz = img.news_id;
                else
                    nz =(bdmirrorDB.news) dictimages.ElementAt(uit.get_rand(dictimages.Count)).Key;
                imagesall.Remove(img);
                alreadyinserted.Add(nz);
                alreadyinsertedimg.Add(img);
                return new Tuple<bdmirrorDB.news, bdmirrorDB.images>(nz, img);
            }
            catch (Exception ex)
            {
                
                img = (imagesall.Count != 0) ? imagesall.ElementAt(uit.get_rand(imagesall.Count)) :
                    (bdmirrorDB.images) dictimages.ElementAt(uit.get_rand(dictimages.Count)).Value;

                if (img.image_id != 233)
                    nz = img.news_id;
                else
                    nz = (bdmirrorDB.news)dictimages.ElementAt(uit.get_rand(dictimages.Count)).Key;
                alreadyinserted.Add(nz);
                return new Tuple<bdmirrorDB.news, bdmirrorDB.images>(nz, img);
            }

        }
        public string get_half()
        {
            Tuple<bdmirrorDB.news, bdmirrorDB.images> tup = get_news(2);
            bdmirrorDB.news nz = tup.Item1;

            bdmirrorDB.images img = tup.Item2;

            if (nz.news_id != null)
                return @" <div class=""metro-item half"">
                                <a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                            <img src=" + img.image_watermarked + @" width=""100%"" ></a>
                                        <div class=""metro-description metro-hidden"">
                                            <div class=""metro-icon"">
                                                <span><i class=" + Utility.getcaticon(nz.category_id.cat_id) + @"></i></span>
                                            </div>
                                            <div class=""metro-misc clearfix"">
                                                <div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
                                                <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
                                    </div>
                                </div>
                            </div>";
            else
                return "";

        }
        public string get_full()
        {
            Tuple<bdmirrorDB.news, bdmirrorDB.images> tup = get_news(1);
            bdmirrorDB.news nz = tup.Item1;

            bdmirrorDB.images img = tup.Item2;

            return @" <div class=""metro-item full"">
                                <a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                    <img src=" + img.image_watermarked + @"   width=""100%"" alt=" + nz.title_bn + @"></a>
                                <div class=""metro-description"">
                                    <div class=""metro-icon"">
                                        <span><i class="+ Utility.getcaticon(nz.category_id.cat_id) + @"></i></span>
                                    </div>
                                    <div class=""metro-misc clearfix"">
                                        <div class=""metro-date n_color"">
                                            " + nz.insert_time.ToShortDateString() + @"</div>
                                        <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" >" + nz.title_bn + @"</a>
                                        <p class=""metro-text"">
                                           " + nz.content_bn + @"
                                        </p>
                                    </div>
                                </div>
                            </div>";

        }
        public string get_quarters()
        {
            string res = "";
            for (int i = 0; i < 2; i++)
            {
                Tuple<bdmirrorDB.news, bdmirrorDB.images> tup = get_news(3);
                bdmirrorDB.news nz = tup.Item1;

                bdmirrorDB.images img = tup.Item2;
                if(nz.news_id !=null)
                res += @"<div class=""metro-item quarter"">
                                 <a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                            <img src=" + img.image_watermarked + @" width=""100%"" alt=" + nz.title_bn + @"></a>
                                        <div class=""metro-description metro-hidden"">
                                            <div class=""metro-icon"">
                                                <span><i class=" + Utility.getcaticon(nz.category_id.cat_id) + @" ></i></span>
                                            </div>
                                            <div class=""metro-misc clearfix"">
                                                <div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
                                                <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
                                            </div>
                                        </div>
                            </div>";
            }

            return res;
        }
        public string get_boxs()
        {
            string res = @"<div class=""metro-item-ch full"">
                                <div class=""metro-row2"">";
            for (int i = 0; i < 4; i++)
            {
                Tuple<bdmirrorDB.news, bdmirrorDB.images> tup = get_news(0);
                bdmirrorDB.news nz = tup.Item1;

                bdmirrorDB.images img = tup.Item2;
                res += @"               <div class=""metro-item metro-itemq box"">
                                        <a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                           <img src=" + img.image_watermarked + @" width=""100%"" alt=" + nz.title_bn + @"></a>
                                        <div class=""metro-description metro-hidden"">
                                            <div class=""metro-icon"">
                                                <span><i class=" + Utility.getcaticon(nz.category_id.cat_id) + @"></i></span>
                                            </div>
                                            <div class=""metro-misc clearfix"">
                                                <div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
                                                <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
                                            </div>
                                        </div>
                                    </div>";
            }
            res += @"</div>
                            </div>";
            return res;
        }
        #endregion
    }
}