﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
using System.Web.Script.Services;

namespace citibd
{
    /// <summary>
    /// Summary description for home
    /// </summary>
    [WebService(Namespace = "http://www.citibd.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class home : System.Web.Services.WebService
    {

        [WebMethod]
        public string getnews(string catid)
        {
            bdmirrorDB.categories cat = new CategoryDAO().pickbyID(int.Parse(catid));
            IList<bdmirrorDB.news> newz = new NewsDAO().getAllNewsbyCatName(cat).Take(7).ToList();
            string html="";
            ImageDAO imgdao = new ImageDAO();
            foreach(bdmirrorDB.news nz in newz)
            {
                bdmirrorDB.images img = imgdao.GetImagebyNewsID(nz);
                if(img==null)
                    img = imgdao.pickbyID(233);
                html+=@"<div class=""metro-item half"" style=""width:100%"">
									<a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                            <img src=" + img.image_watermarked + @" width=""100%"" ></a>

										<div class=""metro-description metro-hidden"">
											<div class=""metro-icon""><span><i class=""icon-music icon-2x""></i></span></div>
											<div class=""metro-misc clearfix"">
												<div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
												   <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
											</div>
										</div>
									</div>";
            }
            return html;
        }
    }
}
