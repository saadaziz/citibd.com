﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using bdmirror24.DAL.mappings;
using bdmirror24.DAL.DAL_Functions.Implementations;
namespace citibd
{
    public partial class main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable dtcat = new DataTable("dtcat");
                dtcat.Columns.Add("cat", typeof(string));
                dtcat.Columns.Add("catlink", typeof(string));
                IList<bdmirrorDB.categories> cats = new CategoryDAO().All();
                foreach (bdmirrorDB.categories cat in cats)
                    dtcat.Rows.Add(cat.name_bn,"Category.aspx?category="+cat.name_bn+"&id="+cat.cat_id);
                rptcat.DataSource = dtcat;
                rptcat.DataBind();
                rptrespon.DataSource = dtcat;
                rptrespon.DataBind();

                dtcat = new DataTable("dtdiv");
                dtcat.Columns.Add("cat", typeof(string));
                dtcat.Columns.Add("catlink", typeof(string));
                IList<bdmirrorDB.admdivision> divs = new DivisionDAO().All();
                foreach (bdmirrorDB.admdivision cat in divs)
                    dtcat.Rows.Add(cat.divName, "search.aspx?division=" + cat.divName + "&id=" + cat.division_id);
                rptdiv.DataSource = dtcat;
                rptdiv.DataBind();
                rptdiv1.DataSource = dtcat;
                rptdiv1.DataBind();
                if (Request.QueryString["search"] != null)
                    Response.Redirect("Search.aspx?Searchtag=" + Request.QueryString["search"].ToString());

                //DataTable dtlatest1 = new DataTable("dtlatest1");
                //dtlatest1.Columns.Add("newstitle", typeof(string));
                //dtlatest1.Columns.Add("newslink", typeof(string));
                //IList<bdmirrorDB.news> latestnews = new NewsDAO().GetLatestNews("latest");
                //foreach (bdmirrorDB.news nz in latestnews)
                //    dtlatest1.Rows.Add(nz.title_bn,"Newsdetails.aspx?id="+nz.news_id+"&title="+Server.UrlEncode( nz.title_bn));
                //rptlatest1.DataSource = dtlatest1;
                //rptlatest1.DataBind();
                if (Utility.get_ormuserid() == null)
                {
                    hplreg.Visible = true;
                    hpllogin.Visible = true;
                }
                else
                {
                    hplreg.Visible = false;
                    hpllogin.Visible = false;
                }
            }
        }
    }
}