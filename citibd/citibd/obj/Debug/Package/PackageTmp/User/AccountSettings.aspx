﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountSettings.aspx.cs"
    Inherits="citibd.User.AccountSettings" MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
    <title>Account Settings : Citibd.com - Your way to journalism</title>
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <asp:ScriptManager ID="scrpt" runat="server">
    </asp:ScriptManager>
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
        <button class="close" data-dismiss="alert" type="button">
            ×</button>
        <i class="icon-exclamation-sign"></i><strong>Message!</strong>
        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
    </asp:Panel>
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">
                    Account Settings</h3>
                <ul class="top-right-toolbar">
                    <li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" title="Profile">
                        <i class="icon-user"></i></a></li>
                    <li><a href="#" class="green" title="Post News"><i class=" icon-upload-alt"></i></a>
                    </li>
                    <li><a href="#" class="bondi-blue" title="Settings"><i class="icon-cogs"></i></a>
                    </li>
                </ul>
            </div>
            <ul class="breadcrumb">
                <li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right">
                </i></span></li>
                <li><a href="#">My Profile</a><span class="divider"><i class="icon-angle-right"></i></span></li>
                <li class="active">Account Settings</li>
            </ul>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head blue">
                    <h3>
                        Account Settings</h3>
                </div>
                <div class="widget-container">
                    <div class="div-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                Email Address</label>
                            <div class="controls">
                                <input type="text" placeholder="Text Input" class="span12">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Password</label>
                            <div class="controls">
                                <input type="text" placeholder="Text Input" class="span12 left-stripe">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Confirm Password</label>
                            <div class="controls">
                                <input type="password" placeholder="Password" class="span12">
                            </div>
                        </div>
                        <div class="div-actions">
                            <button type="submit" class="btn btn-success">
                                Save changes</button>
                            <button type="button" class="btn">
                                Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
