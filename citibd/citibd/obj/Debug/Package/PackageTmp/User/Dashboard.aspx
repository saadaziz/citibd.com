﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="citibd.User.Dashboard"
    MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
    <title>Dashboard :: Citibd.com - Your home to journalism</title>
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">
            &times;</button>
        <i class="icon-exclamation-sign"></i><strong>Beta!</strong> Its the beta version running.....
    </div>
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">
                    Dashboard</h3>
                <ul class="top-right-toolbar">
                    <li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" title="Profile">
                        <i class="icon-user"></i></a></li>
                    <li><a href="#" class="green" title="Post News"><i class=" icon-upload-alt"></i></a>
                    </li>
                    <li><a href="#" class="bondi-blue" title="Settings"><i class="icon-cogs"></i></a>
                    </li>
                </ul>
            </div>
            <ul class="breadcrumb">
                <li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right">
                </i></span></li>
                <li><a href="Dashboard.aspx">Dashboard</a><span class="divider"><i class="icon-angle-right"></i></span></li>
            </ul>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="switch-board gray">
                <ul class="clearfix switch-item">
                    <li><a href="#" class="brown"><i class="icon-user"></i><span>Post
                        News</span></a></li>
                    <li><span class="notify-tip">
                        <asp:Label ID="lblAllCount" runat="server" Text="20"></asp:Label></span>
                        <a href="#" class="light-blue"><i class="icon-cogs"></i><span>All News</span></a></li>
                    <li><span class="notify-tip">
                        <asp:Label ID="lblapprovedCount" runat="server" Text="20"></asp:Label></span><a href="#" class=" blue-violate"><i class="icon-lightbulb"></i><span>Approved</span></a></li>
                    <li><span class="notify-tip">
                        <asp:Label ID="lblPendingCount" runat="server" Text="20"></asp:Label></span><a href="#" class="dark-yellow"><i class="icon-bar-chart"></i><span>Pending</span></a></li>
                    <li><span class="notify-tip">
                        <asp:Label ID="lblRejectedCount" runat="server" Text="20"></asp:Label></span><a href="#" class="green"><i class="icon-shopping-cart"></i><span>Rejected</span></a></li>
                    <li><span class="notify-tip">
                        <asp:Label ID="lblNoticeCount" runat="server" Text="20"></asp:Label></span><a href="#" class=" bondi-blue"><i class="icon-time"></i><span>Notice Board</span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="span6">
        <div class="content-widgets white">
            <div class="widget-head light-blue">
                <h3>
                    <i class="icon-comments-alt"></i>Chat</h3>
            </div>
            <div class="widget-container">
                <div class="tab-widget tabbable tabs-left chat-widget">
                   <%-- <ul class="nav nav-tabs" id="chat-tab">
                        <li class="active"><a href="#user"><span class="user-online"></span><i class="icon-user">
                        </i>Online User </a></li>
                        <li><a href="#user1"><span class="user-offline"></span><i class="icon-user"></i>Offline
                            User </a></li>
                        <li><a href="#user2"><span class="user-offline"></span><i class="icon-user"></i>Offline
                            User </a></li>
                        <li><a href="#user3"><span class="user-offline"></span><i class="icon-user"></i>Offline
                            User </a></li>
                        <li><a href="#user4"><span class="user-offline"></span><i class="icon-user"></i>Offline
                            User </a></li>
                        <li><a href="#user5"><span class="user-offline"></span><i class="icon-user"></i>Offline
                            User </a></li>
                        <li><a href="#user6"><span class="user-online"></span><i class="icon-user"></i>Online
                            User </a></li>
                        <li><a href="#user7"><span class="user-online"></span><i class="icon-user"></i>Online
                            User </a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="user">
                            <div class="conversation">
                                <a href="#" class="pull-left media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Jhon Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation">
                                <a href="#" class="pull-left media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Jhon Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation">
                                <a href="#" class="pull-left media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Jhon Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation right-align">
                                <a href="#" class="pull-right media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Marfy Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation">
                                <a href="#" class="pull-left media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Jhon Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation right-align">
                                <a href="#" class="pull-right media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Marfy Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation right-align">
                                <a href="#" class="pull-right media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Marfy Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="user1">
                            User Offline
                        </div>
                        <div class="tab-pane" id="user2">
                            User Offline
                        </div>
                        <div class="tab-pane" id="user3">
                            User Offline
                        </div>
                        <div class="tab-pane" id="user4">
                            User Offline
                        </div>
                        <div class="tab-pane" id="user5">
                            User Offline
                        </div>
                        <div class="tab-pane" id="user6">
                            <div class="conversation">
                                <a href="#" class="pull-left media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Jhon Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation">
                                <a href="#" class="pull-left media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Jhon Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation">
                                <a href="#" class="pull-left media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Jhon Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation right-align">
                                <a href="#" class="pull-right media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Marfy Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="user7">
                            <div class="conversation">
                                <a href="#" class="pull-left media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Jhon Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                            <div class="conversation right-align">
                                <a href="#" class="pull-right media-thumb">
                                    <img src="images/item-pic.png" width="34" height="34" alt="user"></a>
                                <div class="conversation-body ">
                                    <h4 class="conversation-heading">
                                        Marfy Says:</h4>
                                    <p>
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin
                                        commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce
                                        condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chat-input">
                        <textarea class="chat-inputbox span4" name="input"></textarea>
                        <button class="btn btn-primary btn-large pull-right" type="button">
                            <i class="icon-ok"></i>Send</button>
                    </div>--%>
                    <i>Coming Soon....</i>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
