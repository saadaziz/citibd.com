﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="citibd.User.EditProfile"
    MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
    <title>Edit Profile : Citibd.com - Your way to journalism</title>
</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
    <asp:ScriptManager ID="scrpt" runat="server">
    </asp:ScriptManager>
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
        <button class="close" data-dismiss="alert" type="button">
            ×</button>
        <i class="icon-exclamation-sign"></i><strong>Message!</strong>
        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
    </asp:Panel>
    <div class="row-fluid ">
        <div class="span12">
            <div class="primary-head">
                <h3 class="page-header">
                    Account Settings</h3>
                <ul class="top-right-toolbar">
                    <li><a data-toggle="dropdown" class="dropdown-toggle blue-violate" href="#" title="Profile">
                        <i class="icon-user"></i></a></li>
                    <li><a href="#" class="green" title="Post News"><i class=" icon-upload-alt"></i></a>
                    </li>
                    <li><a href="#" class="bondi-blue" title="Settings"><i class="icon-cogs"></i></a>
                    </li>
                </ul>
            </div>
            <ul class="breadcrumb">
                <li><a href="#" class="icon-home"></a><span class="divider "><i class="icon-angle-right">
                </i></span></li>
                <li><a href="#">My Profile</a><span class="divider"><i class="icon-angle-right"></i></span></li>
                <li class="active">Account Settings</li>
            </ul>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head blue">
                    <h3>
                        About You</h3>
                </div>
                <div class="widget-container">
                    <div class="div-horizontal">
  <div class="control-group">
                                    <label class="control-label">
                                        First Name</label>
                                    <div class="controls">
                                <input type="text" placeholder="First name" class="span12">
                                        
                                        
                                    </div>
                                </div>                        <div class="control-group">
                            <label class="control-label">
                                Last Name</label>
                            <div class="controls">
                                <input type="text" placeholder="last name" class="span12 left-stripe">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Profile Picture</label>
                            <div class="controls">
                                <input name="" type="file" class="file-unidiv">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                National ID. Number</label>
                            <div class="controls">
                                <input type="text" placeholder="give us your national identity number here" class="span12 left-stripe">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Short Biography</label>
                            <div class="controls">
                                <textarea rows="4" placeholder="write your working education, working experience, personal info here"
                                    class="span12"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head blue">
                    <h3>
                        Contact Details</h3>
                </div>
                <div class="widget-container">
                    <div class="div-horizontal">
                        <fieldset class="default">
                            <div class="control-group">
                                <label class="control-label">
                                    Facebook Url</label>
                                <div class="controls">
                                    <input type="text" placeholder="optional. example: http://www.facebook.com/your-profile-id"
                                        class="span12">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Linkedin Url</label>
                                <div class="controls">
                                    <input type="password" placeholder="optional. example: http://www.linkedin.com/your-profile-id"
                                        class="span12">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Phone No.</label>
                                <div class="controls">
                                    <input type="text" placeholder="example: +88-01XXX-XXXXXX" class="span12">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Present Address</label>
                                <div class="controls">
                                    <textarea rows="2" placeholder="write your present address here" class="span12"></textarea>
                                </div>
                            </div>
                            <p>
                                Country Dropdown<p>
                                    <p>
                                        Division Dropdown<p>
                                            <p>
                                                District Dropdown<p>
                                                    <p>
                                                        Upazilla/Thana Dropdown<p>
                                                            <div class="control-group">
                                                                <label class="control-label">
                                                                    Postal Code</label>
                                                                <div class="controls">
                                                                    <input type="text" placeholder="Postal code number" class="span12">
                                                                </div>
                                                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content-widgets light-gray">
                <div class="widget-head blue">
                    <h3>
                        Personal Info</h3>
                </div>
                <div class="widget-container">
                    <div class="div-horizontal">
                        <fieldset class="default">
                            <div class="control-group">
                                <label class="control-label">
                                    Date of Birth</label>
                                <div class="controls">
                                    <input type="text" placeholder="Day / Month / 2013" class="span12">
                                </div>
                            </div>
                            <p>
                                Gender<p>
                                    <div class="control-group">
                                        <label class="control-label">
                                            Reference Person Name</label>
                                        <div class="controls">
                                            <input type="text" placeholder="optional. write any person's name who know you well"
                                                class="span12">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            Reference Person Address</label>
                                        <div class="controls">
                                            <input type="text" placeholder="optional. write reference person's persent address who know you well"
                                                class="span12">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">
                                            Reference Person Cell No.</label>
                                        <div class="controls">
                                            <input type="text" placeholder="optional. write reference person's phone number who know you well"
                                                class="span12">
                                        </div>
                                    </div>
                                    <div class="div-actions">
                                        <button type="submit" class="btn btn-success">
                                            Save changes</button>
                                        <button type="button" class="btn">
                                            Cancel</button>
                                    </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
