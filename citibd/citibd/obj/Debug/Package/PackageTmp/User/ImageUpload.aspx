﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageUpload.aspx.cs" Inherits="citibd.User.ImageUpload" MasterPageFile="~/User/Admin_master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content runat="server" ContentPlaceHolderID="head" ID="idhead">
  <title>Upload Image : Citibd.com - Your way to journalism</title>

</asp:Content>
<asp:Content ID="idcnt" ContentPlaceHolderID="cphBody" runat="server">
<asp:MultiView ID="mulPost" runat="server" ActiveViewIndex="0">
   <asp:View ID="vImage" runat="server">
    <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" >
        <button class="close" data-dismiss="alert" type="button">
            ×</button>
        <i class="icon-exclamation-sign"></i><strong>Message!</strong>
        <asp:Label ID="lblmsg" runat="server" Text="Your news is inserted,Please upload images and wait for review."></asp:Label>
    </asp:Panel>
     <div class="row-fluid">
                <div class="span12">
                    <div class="content-widgets gray">
                        <div class="widget-head blue-violate">
                            <h3>
                                Upload Images</h3>
                        </div>
                        <div class="widget-container">
                            <div class="form-horizontal">
                              <div class="fileupload fileupload-new" data-provides="fileupload">
										
                                  <asp:FileUpload ID="FileUpload1" runat="server" />
                                            <asp:Button CssClass="btn" ID="Button1" runat="server" Text="Upload" OnClick="addimage_click" />
                                            <em>  The height and width of the image must be 140px or higher</em>
                                            <br />
                                            <br />
                                            <asp:GridView ID="grdimg" runat="server" CssClass="table table-striped table-bordered"
                                   Width="100%" AutoGenerateColumns="False" ShowHeaderWhenEmpty="false" OnRowCommand="grvimg_RowCommand"
                                   OnRowDataBound="grvStockSerial_RowDataBound">
                                  <Columns>
                                  
                                   <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:Label ID="lblnum" runat="server" Text='<%# Eval("id") %>'  />
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Image ID="imgUp" runat="server" ImageUrl='<%# Eval("image_link") %>' Height="140" Width="140" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Image Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="8%">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("image_name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="2%">
                            <ItemTemplate>
                               <asp:Button ID="imgDelete" runat="server" CommandName="Delete" OnClientClick='return confirm("Are you sure you want to Delete?");'  Text="Delete" />
                                      
                            </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                                  </asp:GridView>
										</div>
                            </div>
                        </div>
                    </div>
                </div>
   </asp:View>
   </asp:MultiView>
</asp:Content>