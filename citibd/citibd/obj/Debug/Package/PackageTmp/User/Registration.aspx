﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="citibd.User.Registration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/bootstrap.css" rel="stylesheet" media="screen" />
    <title>Registration Citibd.com :: Your home to journalism</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    
<link rel="icon" 
      type="image/png" 
      href="http://example.com/myicon.png">
</head>
<body>
    <div>
        <div class="span12">
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <a class="brand" href="#">CitiBD.com User Panel</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <asp:Panel CssClass="alert alert-error" runat="server" ID="pnlalert" Visible="false">
            <button class="close" data-dismiss="alert" type="button">
                ×</button>
            <i class="icon-exclamation-sign"></i><strong>Error!</strong>
            <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
        </asp:Panel>
        <br />
        <br />
        <br />
        <div class="well">
            <form id="signup" class="form-horizontal" runat="server">
            <asp:ScriptManager ID="scrpt" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel runat="server" ID="updtpnl">
                <ContentTemplate>
                    <legend>Sign Up</legend>
                    <asp:MultiView ID="mulReg" runat="server">
                        <asp:View ID="reg1" runat="server">
                            <table>
                                <tr>
                                    <td style="width: 50%;">
                                        <div class="control-group">
                                            <label class="control-label">
                                                First Name</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-user"></i></span>
                                                    <asp:TextBox CssClass="input-xlarge" ID="txtfname" name="fname" placeholder="First Name"
                                                        runat="server" />
                                                    <asp:RequiredFieldValidator ID="rfValidator3" runat="server" ControlToValidate="txtfname"
                                                        CssClass="failureNotification" ErrorMessage="First Name is required." ToolTip="First Name is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group ">
                                            <label class="control-label">
                                                Last Name</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-user"></i></span>
                                                    <asp:TextBox CssClass="input-xlarge" ID="txtlname" name="lname" placeholder="Last Name"
                                                        runat="server" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlname"
                                                        CssClass="failureNotification" ErrorMessage="Last Name is required." ToolTip="Last Name is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group ">
                                            <label class="control-label">
                                                Country</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-globe"></i></span>
                                                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel runat="server" ID="pnlothers">
                                            <div class="control-group ">
                                                <label class="control-label">
                                                    Division</label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on"><i class="icon-globe"></i></span>
                                                        <asp:DropDownList ID="ddldivision" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group ">
                                                <label class="control-label">
                                                    District</label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on"><i class="icon-globe"></i></span>
                                                        <asp:DropDownList ID="ddlDistrict" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group ">
                                                <label class="control-label">
                                                    Upzilla</label>
                                                <div class="controls">
                                                    <div class="input-prepend">
                                                        <span class="add-on"><i class="icon-globe"></i></span>
                                                        <asp:DropDownList ID="ddlUpzilla" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="control-group ">
                                            <label class="control-label">
                                                Contact No</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-tasks"></i></span>
                                                    <asp:TextBox CssClass="input-xlarge" ID="txtcontact" name="lname" placeholder="Contact No"
                                                        runat="server" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtcontact"
                                                        CssClass="failureNotification" ErrorMessage="Contact no is required." ToolTip="Contact no is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 50%;">
                                        <div class="control-group">
                                            <label class="control-label">
                                                Email</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-envelope"></i></span>
                                                    <asp:TextBox CssClass="input-xlarge" ID="txtemail" name="email" placeholder="Email"
                                                        runat="server" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtemail"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SerialValidationGroup"
                                                        ErrorMessage="Please insert a valid email" ToolTip="Please insert a valid email"
                                                        runat="server"><img src="../img/Left_Arrow.png" alt="*" /></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">
                                                Password</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-lock"></i></span>
                                                    <asp:TextBox TextMode="Password" ID="txtpassword" CssClass="input-xlarge" name="passwd"
                                                        placeholder="Password" runat="server" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpassword"
                                                        CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">
                                                Confirm Password</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-lock"></i></span>
                                                    <asp:TextBox TextMode="Password" ID="txtpassword2" CssClass="input-xlarge" name="conpasswd"
                                                        placeholder="Re-enter Password" runat="server" />
                                                    <asp:CompareValidator ID="compval" ControlToValidate="txtpassword" ControlToCompare="txtpassword2"
                                                        ErrorMessage="Password didnt match" ToolTip="Password didnt match" runat="server"
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:CompareValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">
                                                Gender</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <span class="add-on"><i class="icon-hand-right"></i></span>
                                                    <asp:DropDownList ID="ddlgender" runat="server" CssClass="input-xlarge">
                                                        <asp:ListItem>Male</asp:ListItem>
                                                        <asp:ListItem>Female</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group ">
                                            <label class="control-label">
                                                Address</label>
                                            <div class="controls">
                                                <div class="input-prepend">
                                                    <asp:TextBox CssClass="input-xlarge" TextMode="MultiLine" ID="txtaddress" name="lname"
                                                        placeholder="Address" runat="server" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtaddress"
                                                        CssClass="failureNotification" ErrorMessage="Address is required." ToolTip="Address is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../img/Left_Arrow.png" 
                                                    alt="*" /></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <p>
                                By registering you will oblidge the <a href="#" target="_blank">terms and conditions</a>
                                of Citibd.com
                            </p>
                            <div class="control-group pull-right">
                                <label class="control-label">
                                </label>
                                <div class="controls">
                                    <asp:Button runat="server" ID="btnSubmit" CssClass="btn btn-success" ValidationGroup="SerialValidationGroup"
                                        Text="Create My Account" OnClick="btnreg_click"></asp:Button>
                                </div>
                            </div>
                        </asp:View>
                        <asp:View ID="reg2" runat="server">
                            <p>
                                Successfully registered. You will be redirected to the Dashboard of Citibd.com in
                                <pre>5 seconds...</pre>
                            </p>
                        </asp:View>
                        <asp:View ID="reg3" runat="server">
                            <p>
                                Oops! There's been a problem in the server, please come back later</p>
                            <asp:Label ID="lblerr" runat="server" Text="Label"></asp:Label>
                        </asp:View>
                    </asp:MultiView>
                </ContentTemplate>
            </asp:UpdatePanel>
            </form>
        </div>
    </div>
    <!-- Javascript placed at the end of the file to make the  page load faster -->
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap-button.js"></script>
    <script language="javascript" type="text/javascript">

        function onSuccess() {
            setTimeout(okay, 5000);
        }
        function okay() {
            window.location = "http://www.google.com";
        }
    </script>
</body>
</html>
