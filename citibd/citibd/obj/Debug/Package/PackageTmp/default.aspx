﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="citibd._default"
    MasterPageFile="~/main.Master" %>

<asp:Content ID="contHead" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="contBody" runat="server" ContentPlaceHolderID="cphbody">
    <div class="container">
        <ul class="thumbnails">
            <li class="span3 tile tile-double tile-orange"><a href="#">
                <asp:Image ID="Image1" CssClass="tile tile-double" runat="server" ImageUrl="~/img/Dipu-Moni-New-York.jpg"
                    Height="140" Width="280" />
            </a></li>
            <li class="span3 tile">
                <div class="linked" href="#">
                    <div class="more">
                        <a>Read More </a>
                    </div>
                    <asp:Image ID="Image2" CssClass="imgg" runat="server" ImageUrl="~/img/Tulips.jpg"
                        Height="140" Width="140" />
                </div>
            </li>
            <li class="span3 tile tile-orange"><a href="#">
                <h3 class="tile-text">
                    3!</h3>
            </a></li>
            <li class="span3 tile tile-double tile-teal"><a href="#">
                <h3 class="tile-text">
                    I'm the 4!</h3>
            </a></li>
            <li class="span3 tile tile-double-hor tile-lime"><a href="#">
                <h3 class="tile-text">
                    I'm the 5!</h3>
            </a></li>
            <li class="span3 tile tile-green"><a href="#">
                <h3 class="tile-text">
                    7!</h3>
            </a></li>
            <li class="span3 tile tile-teal"><a href="#">
                <h3 class="tile-text">
                    8!</h3>
            </a></li>
            <li class="span3 tile tile-double tile-blue"><a href="#">
                <h3 class="tile-text">
                    9!</h3>
            </a></li>
            <li class="span3 tile tile-double tile-red"><a href="#">
                <h3 class="tile-text">
                    10!</h3>
            </a></li>
        </ul>
    </div>
</asp:Content>
