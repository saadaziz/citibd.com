﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.IO;

namespace citibd
{
    /// <summary>
    /// Summary description for showimg
    /// </summary>
    public class showimg : IHttpHandler
    {
        long seq = 0;
        byte[] empPic = null;
        public void ProcessRequest(HttpContext context)
        {
            string picid;
            if (context.Request.QueryString["path"] != null)
                picid = (context.Request.QueryString["path"].ToString());
            else
                throw new ArgumentException("No parameter specified");
            // Convert Byte[] to Bitmap
            Bitmap newBmp = ConvertToBitmap(ShowAlbumImage(picid));
            if (newBmp != null)
            {
                newBmp.Save(context.Response.OutputStream, ImageFormat.Jpeg);
                newBmp.Dispose();
            }
 
        }
        protected Bitmap ConvertToBitmap(byte[] bmp)
        {
            if (bmp != null)
            {
                TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                Bitmap b = (Bitmap)tc.ConvertFrom(bmp);
                return b;
            }
            return null;
        }
        public byte[] ShowAlbumImage(string fullFilePath)
        {
            //bdmirrorDB.images img = new ImageDAO().GetImagebyNewsID(picid);
            //string fullFilePath = Server.MapPath();
            FileStream fs = File.OpenRead(fullFilePath);
            try
            {
                byte[] bytes = new byte[fs.Length];
                fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                fs.Close();
                return bytes;
            }
            finally
            {
                fs.Close();
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}