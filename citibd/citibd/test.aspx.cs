﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using HtmlAgilityPack;
using System.Text;
namespace citibd
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string result = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.dsebd.org/complaintCell.php");
            request.Method = "GET";

            using (var stream = request.GetResponse().GetResponseStream())
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                result = reader.ReadToEnd();
            }

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.Load(new StringReader(result));

            if (doc.DocumentNode != null)
            {
               string htmls =doc.DocumentNode.SelectSingleNode("//marquee").ParentNode.InnerHtml;
               
                pnlshare.Controls.Add(new LiteralControl(htmls));
            }
        }
    }
}