﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test_ashx.aspx.cs" Inherits="citibd.test_ashx"
    MasterPageFile="~/main.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function RetrievePicture(imgCtrl, picid) {
            alert("asadasda");
            imgCtrl.onload = null;
            imgCtrl.src = 'showimg.ashx?path=' + picid;

        }
       
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            function lastPostFunc() {
                $('#divPostsLoader').html('<img src="img/bigLoader.gif">');

                //send a query to server side to present new content
                $.ajax({
                    type: "POST",
                    url: "test_ashx.aspx/Foo",
                    data: "{}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (data != "") {
                            $('.metro-slider:last').after(data.d);
                        }
                        $('#divPostsLoader').empty();
                    }

                })
            };

            //When scroll down, the scroller is at the bottom with the function below and fire the lastPostFunc function
            $(window).scroll(function () {
                if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                    alert($(window).scrollTop());
                    lastPostFunc();
                }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <div class="n_content clearfix">
        <div class="viewport">
            <div class="overview">
                <div class="metro-slider">
                    <asp:Repeater ID="rptrow" runat="server">
                        <ItemTemplate>
                            <asp:Panel ID="pnlrow" runat="server" class="metro-row">
                                <%# Eval("text") %>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                    
                </div>
            </div>
            </div>
        </div>
         <div id="divPostsLoader">
        </div>
</asp:Content>
