﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Text;
using System.Data;
using bdmirror24.DAL.DAL_Functions.Implementations;
using bdmirror24.DAL.mappings;

namespace citibd
{
    public partial class test_ashx : System.Web.UI.Page
    {
        #region private properties
        static Utility uit = new Utility();

        private int catid
        {
            get
            {
                if (ViewState["catid"] == null)
                    ViewState["catid"] = -1;
                return (int)ViewState["catid"];
            }
            set
            {
                ViewState["catid"] = value;
            }
        }
        static IList<bdmirrorDB.news> newz = new List<bdmirrorDB.news>();
        static IList<bdmirrorDB.news> alreadyinserted = new List<bdmirrorDB.news>();
        ImageDAO imgdao = new ImageDAO();
        #endregion
         #region loadrows
        protected void loadrows(bdmirrorDB.categories cat,int count)
        {
            DataTable dtnews = new DataTable("dtnz");
            newz = new NewsDAO().getAllNewsbyCatName(cat).Take((count+1)*18).Skip(count*18).ToList();
            dtnews.Columns.Add("text", typeof(string));

            for (int i = 0; i < 3; i++)
            {
                int tot = 0;
                string html = "";
                while (true)
                {
                    int rand = uit.get_rand(4);
                    if (tot < 2 && rand == 2)
                    {
                        int randf = uit.get_rand(3);
                        if (randf == 2)
                            html += get_boxs();
                        else
                            html += get_full();
                        tot += 2;
                    }
                    else
                    {
                        int randh = uit.get_rand(5);
                        if (randh == 2)
                            html += get_quarters();
                        else
                            html += get_half();
                        tot += 1;
                    }
                    if (tot == 4) break;
                }
                dtnews.Rows.Add(html);
            }
            rptrow.DataSource = dtnews;
            rptrow.DataBind();
        }
        protected string loadrowslate(bdmirrorDB.categories cat, int count)
        {
            string res = "";
            IList<bdmirrorDB.news> oldnews = newz.ToList();
            newz = new NewsDAO().getAllNewsbyCatName(cat).Take((count + 1) * 6).Skip(count * 6).ToList();
            foreach (bdmirrorDB.news newzz in oldnews)
            {
                if (alreadyinserted.Where(x => x.news_id == newzz.news_id).ToList().Count == 0)
                    newz.Add(newzz);
            }
            for (int i = 0; i < 1; i++)
            {
                int tot = 0;
                string html = "";
                while (true)
                {
                    int rand = uit.get_rand(4);
                    if (tot < 2 && rand == 2)
                    {
                        int randf = uit.get_rand(3);
                        if (randf == 2)
                            html += get_boxs();
                        else
                            html += get_full();
                        tot += 2;
                    }
                    else
                    {
                        int randh = uit.get_rand(7);
                        if (randh == 2)
                            html += get_quarters();
                        else
                            html += get_half();
                        tot += 1;
                    }
                    if (tot == 4) break;
                }
                res+=(html);
            }
            return res;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bdmirrorDB.categories cat = new CategoryDAO().pickbyID(7);
                catid = 7;
                //if (Request.QueryString["id"] != null)
                //{
                //    cat = new CategoryDAO().pickbyID(int.Parse(Request.QueryString["id"].ToString()));
                //    catid = Convert.ToInt32(cat.cat_id);
                //}
                //else
                //    Response.Redirect("default.aspx");
                loadrows(cat, 0);
                Page.Title = cat.name_bn + " .::. Your home to civil journalism.";
                Page.MetaDescription = cat.name_bn + " news from dhaka,rajshahi,barishal,chittagong,rangpur,sylhet,khulna." + cat.name_bn + " Civil journalism of bangladesh";
                Page.MetaKeywords = "civil journalism bangladesh,civil journalism dhaka,photo journalism dhaka," + cat.name_bn;
            }
        }
        #region htmls
        protected static Tuple<bdmirrorDB.news, bdmirrorDB.images> get_news(int no)
        {
            bdmirrorDB.news nz = new bdmirrorDB.news();
            bdmirrorDB.images img = new bdmirrorDB.images();
            ImageDAO imgdao = new ImageDAO();
            List<bdmirrorDB.news> inhere = new List<bdmirrorDB.news>();
            while (true)
            {
                nz = newz.ElementAt(uit.get_rand(newz.Count));
               
                //if (inhere.Where(x => x.news_id == nz.news_id).ToList().Count != 0)
                //    continue;
                if (no == 1)//full
                {
                    img = imgdao.GetImagebyNewsID(nz);
                    if (img.width < 500)
                    {
                        //inhere.Add(nz);
                        continue;
                    }
                }
                //else if (no == 3)//quarter
                //{
                //    img = new ImageDAO().GetImagebyNewsID(nz);
                //    if (img.width<img.height )
                //    {
                //        inhere.Add(nz);
                //        continue;
                //    }
                //}
                //else if (no == 2)//half
                //{
                //    img = new ImageDAO().GetImagebyNewsID(nz);
                //    if (img.width <= 300)
                //        continue;
                //}
                else
                    img = imgdao.GetImagebyNewsID(nz);
                if (alreadyinserted.Where(x => x.news_id == nz.news_id).ToList().Count == 0)
                    break;

            }
            alreadyinserted.Add(nz);
            return new Tuple<bdmirrorDB.news, bdmirrorDB.images>(nz, img);
        }
        protected static string get_half()
        {
            Tuple<bdmirrorDB.news, bdmirrorDB.images> tup = get_news(2);
            bdmirrorDB.news nz = tup.Item1;

            bdmirrorDB.images img = tup.Item2;


            return @" <div class=""metro-item half"">
                                <a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                            <img src=" + img.image_watermarked + @" width=""100%"" ></a>
                                        <div class=""metro-description metro-hidden"">
                                            <div class=""metro-icon"">
                                                <span><i class=""icon-medkit icon-2x""></i></span>
                                            </div>
                                            <div class=""metro-misc clearfix"">
                                                <div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
                                                <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
                                    </div>
                                </div>
                            </div>";

        }
        protected static string get_full()
        {
            Tuple<bdmirrorDB.news, bdmirrorDB.images> tup = get_news(1);
            bdmirrorDB.news nz = tup.Item1;

            bdmirrorDB.images img = tup.Item2;

            return @" <div class=""metro-item full"">
                                <a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                    <img src=" + img.image_watermarked + @"   width=""100%"" alt=" + nz.title_bn + @"></a>
                                <div class=""metro-description"">
                                    <div class=""metro-icon"">
                                        <span><i class=""icon-magic icon-2x""></i></span>
                                    </div>
                                    <div class=""metro-misc clearfix"">
                                        <div class=""metro-date n_color"">
                                            " + nz.insert_time.ToShortDateString() + @"</div>
                                        <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" >" + nz.title_bn + @"</a>
                                        <p class=""metro-text"">
                                           " + nz.content_bn + @"
                                        </p>
                                    </div>
                                </div>
                            </div>";

        }
        protected static string get_quarters()
        {
            string res = "";
            for (int i = 0; i < 2; i++)
            {
                Tuple<bdmirrorDB.news, bdmirrorDB.images> tup = get_news(3);
                bdmirrorDB.news nz = tup.Item1;

                bdmirrorDB.images img = tup.Item2;
                res += @"<div class=""metro-item quarter"">
                                 <a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                            <img src=" + img.image_watermarked + @" width=""100%"" alt=" + nz.title_bn + @"></a>
                                        <div class=""metro-description metro-hidden"">
                                            <div class=""metro-icon"">
                                                <span><i class=""icon-medkit icon-2x""></i></span>
                                            </div>
                                            <div class=""metro-misc clearfix"">
                                                <div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
                                                <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
                                            </div>
                                        </div>
                            </div>";
            }

            return res;
        }
        protected static string get_boxs()
        {
            string res = @"<div class=""metro-item-ch full"">
                                <div class=""metro-row2"">";
            for (int i = 0; i < 4; i++)
            {
                Tuple<bdmirrorDB.news, bdmirrorDB.images> tup = get_news(0);
                bdmirrorDB.news nz = tup.Item1;

                bdmirrorDB.images img = tup.Item2;
                res += @"               <div class=""metro-item metro-itemq box"">
                                        <a class=""metro-image"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title=" + nz.title_bn + @">
                                           <img src=" + img.image_watermarked + @" width=""100%"" alt=" + nz.title_bn + @"></a>
                                        <div class=""metro-description metro-hidden"">
                                            <div class=""metro-icon"">
                                                <span><i class=""icon-medkit icon-2x""></i></span>
                                            </div>
                                            <div class=""metro-misc clearfix"">
                                                <div class=""metro-date n_color"">
                                                     " + nz.insert_time.ToShortDateString() + @"</div>
                                                <a class=""metro-title"" href=NewsDetails.aspx?newsid=" + nz.news_id + @" title="""">" + nz.title_bn + @" </a>
                                                <p class=""metro-text"">
                                                    " + nz.content_bn + @"
                                                </p>
                                            </div>
                                        </div>
                                    </div>";
            }
            res += @"</div>
                            </div>";
            return res;
        }
        #endregion
        [WebMethod]
        public static string Foo()
        {
            //using (DataSet ds = new DataSet())
            //{
            //    ds.ReadXml(HttpContext.Current.Server.MapPath("App_Data/books.xml"));
            //    DataView dv = ds.Tables[0].DefaultView;

            //    foreach (DataRowView myDataRow in dv)
            //    {
            //        getPostsText.AppendFormat("<p>author: {0}</br>", myDataRow["author"]);
            //        getPostsText.AppendFormat("genre: {0}</br>", myDataRow["genre"]);
            //        getPostsText.AppendFormat("price: {0}</br>", myDataRow["price"]);
            //        getPostsText.AppendFormat("publish date: {0}</br>", myDataRow["publish_date"]);
            //        getPostsText.AppendFormat("description: {0}</br></p>", myDataRow["description"]);
            //    }
            //    getPostsText.AppendFormat("<div style='height:15px;'></div>");

            //}
            bdmirrorDB.categories cat = new CategoryDAO().pickbyID(7);

            return @"<div class=""metro-slider""><div class=""metro-row"">" + new test_ashx().loadrowslate(cat, 1) + "</div></div>";
            
        }
    }
}